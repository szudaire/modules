Process:
	Controller
States:
	65
Transitions:
	Controller = Q0,
	Q0	= (initial_config -> Q14),
	Q1	= ({no_person, yes_person} -> Q60),
	Q2	= (yes_next -> Q29
		  |no_next -> Q60),
	Q3	= (sort_locations -> Q24),
	Q4	= (yes_safe_next -> Q22
		  |no_safe_next -> Q47),
	Q5	= (go_next -> Q3),
	Q6	= (battery_charged -> Q25),
	Q7	= (no_next -> Q1
		  |yes_person -> Q2
		  |yes_next -> Q12
		  |no_person -> Q51),
	Q8	= (capture -> Q38),
	Q9	= (sort_locations -> Q26),
	Q10	= (sort_locations -> Q30),
	Q11	= (battery_charged -> Q28),
	Q12	= (yes_person -> Q29
		  |no_person -> Q55
		  |is_safe_next -> Q57),
	Q13	= (landed -> Q11),
	Q14	= (takeoff -> Q35),
	Q15	= (sort_locations -> Q37),
	Q16	= (has_next -> Q32),
	Q17	= (battery_charged -> Q33),
	Q18	= (takeoff -> Q39),
	Q19	= (landed -> Q36),
	Q20	= (reset_iterator -> Q16),
	Q21	= (no_person -> Q3
		  |yes_person -> Q27
		  |sort_locations -> Q40),
	Q22	= (go_next -> Q27),
	Q23	= (yes_safe_next -> Q5
		  |no_safe_next -> Q50),
	Q24	= (arrived -> Q48),
	Q25	= (initial_config -> Q53),
	Q26	= (takeoff_ended -> Q50),
	Q27	= (sort_locations -> Q52),
	Q28	= (initial_config -> Q54),
	Q29	= (is_safe_next -> Q4),
	Q30	= (takeoff_ended -> Q34),
	Q31	= (land -> Q13),
	Q32	= (capture -> Q42
		  |yes_next -> Q55
		  |no_next -> Q60),
	Q33	= (initial_config -> Q61),
	Q34	= (has_next -> Q32),
	Q35	= (sort_locations -> Q56),
	Q36	= (battery_charged -> Q64),
	Q37	= (takeoff_ended -> Q58),
	Q38	= ({no_person, yes_person} -> Q49),
	Q39	= (sort_locations -> Q62),
	Q40	= (no_person -> Q24
		  |arrived -> Q46
		  |yes_person -> Q52),
	Q41	= (landed -> Q6),
	Q42	= (no_next -> Q1
		  |yes_person -> Q2
		  |yes_next -> Q12
		  |no_person -> Q51),
	Q43	= (landed -> Q17),
	Q44	= (no_person -> Q5
		  |go_next -> Q21
		  |yes_person -> Q22),
	Q45	= (has_next -> Q32),
	Q46	= (yes_person -> Q49
		  |no_person -> Q58),
	Q47	= (land -> Q41),
	Q48	= (capture -> Q46),
	Q49	= (land -> Q43),
	Q50	= (remove_next -> Q34),
	Q51	= (yes_next -> Q55
		  |no_next -> Q60),
	Q52	= (arrived -> Q8),
	Q53	= (takeoff -> Q9),
	Q54	= (takeoff -> Q10),
	Q55	= (is_safe_next -> Q23),
	Q56	= (takeoff_ended -> Q16),
	Q57	= (yes_person -> Q4
		  |no_person -> Q23
		  |yes_safe_next -> Q44
		  |no_safe_next -> Q59),
	Q58	= (remove_next -> Q45),
	Q59	= (yes_person -> Q47
		  |no_person -> Q50
		  |remove_next -> Q63),
	Q60	= (land -> Q19),
	Q61	= (takeoff -> Q15),
	Q62	= (takeoff_ended -> Q20),
	Q63	= (has_next -> Q7
		  |yes_person -> Q31
		  |no_person -> Q34),
	Q64	= (initial_config -> Q18).
