from MAVProxy.modules.planner_module import Module
from MAVProxy.modules import planner_util_message as mesg
from MAVProxy.modules.lib.ANUGA import lat_long_UTM_conversion as llutm
import numpy as np
import time
from multiprocessing import Value
from random import random

class Battery(Module):
    def init(self):
        self._set_suscription_packets(["GLOBAL_POSITION_INT"])

        self._orig_budget = 1200 #seconds
        self._sharedmem.set_budget(self._orig_budget) #seconds
        
        self._sharedmem.set_battery_warning(0)
        self._sharedmem.set_total_dist(0)
        self._sharedmem.batt_thresh.value = 0.8
        self.controllables = ['charge']

        self._time_set = False
        return
    
    def charge(self):
        self._add_to_message_queue(mesg.PRINT,"Charging with "+str(self._sharedmem.get_total_dist())+" out of "+str(self._sharedmem.get_budget()))
        self._sharedmem.set_total_dist(0)
        self._sharedmem.set_battery_warning(0)
        return 'charged'
	
    def mavlink_packet(self,m):
        mtype = m.get_type()
        if mtype == "GLOBAL_POSITION_INT":
            vx = m.vx*0.01
            vy = m.vy*0.01
            vel = np.sqrt(vx**2 + vy**2)

            if vel < 10: #Don't start counting until takeoff
                return

            if not self._time_set:
                self._time = time.time()
                self._time_set = True
            else:
                if time.time() - self._time < 0.1:
                    return

                dt = time.time() - self._time
                self._time = time.time()
                aux = self._sharedmem.get_total_dist() #Now reinterpreted as time
                
                aux += dt
                self._sharedmem.set_total_dist(aux)

                if self._sharedmem.get_total_dist() > self._sharedmem.batt_thresh.value * self._sharedmem.get_budget():
                    if not self._sharedmem.get_battery_warning():
                        self._sharedmem.set_battery_warning(1)
                        self._add_to_event_queue('low_bat')

class Delivery(Module):
    def init(self):
        self.controllables = ['collect','drop']
        self._process_type = 1
        self.prob_c1 = 0.8
        self._sharedmem.prob_c1.value = self.prob_c1

        self._visit_array = []
        self._visit_time = Value('d',0.0)

        self.estimate = True
        self._cant_c1 = 0
        self._cant_c2 = 0
        
    def _started(self):
        self._visit_time.value = time.time()
        self.drop(first=True)

    def collect(self):
        return
    
    def drop(self,first=False):
        if first == False:
            t2 = time.time()
            t1 = self._visit_time.value
            self._visit_array.append(t2-t1)

            self._visit_time.value = t2

            if np.sum(self._visit_array) > 3600: #time limit
                ftest = open("/root/.cache/mavproxy/logs/time_log.txt",'w')
                ftest.write("plan changes: "+str(self._sharedmem.num_change_choices.value)+"\n")
                ftest.write("rounds: "+str(len(self._visit_array))+"\n")
                ftest.write("mean: "+str(np.mean(self._visit_array))+"\n")
                ftest.write("min: "+str(np.min(self._visit_array))+"\n")
                ftest.write("max: "+str(np.max(self._visit_array))+"\n")
                ftest.write(str(self._visit_array)+"\n")
                ftest.close()

                self._add_to_mavproxy_command_queue("rtl") #Generate RTL to quit mission and save info
                #self._add_to_event_queue('stop_simu') #Provoke automata error and RTL engage

        if random() < self.prob_c1:
            self._cant_c1 += 1
            self._add_to_event_queue('next[8][3]')
        else:
            self._cant_c2 += 1
            self._add_to_event_queue('next[8][7]')
            
        if self.estimate and self._cant_c1 + self._cant_c2 > 0:
            self._sharedmem.prob_c1.value = self._cant_c1 * 1.0 / (self._cant_c1 + self._cant_c2)
            self._add_to_message_queue(mesg.PRINT,"Estimated prob_c1: "+str(self._sharedmem.prob_c1.value))
        
    
