from planner_module import Module
from multiprocessing import Array,Value
from ctypes import c_bool
import numpy as np
import time
#from MAVProxy.modules import planner_util_message as mesg

class Iterator(Module):
	def init(self):
		self._process_type = 0 #1 has independent processes, 0 no independent processes

		self.t1 = 0
		
	def add_locations(self,pos_locations):
		self._size_locations = len(pos_locations)
		
		self._locations = np.zeros((self._size_locations,2),np.int32) #location
		self._locations[:,0] = np.array(range(self._size_locations),np.int32)
		
		self._location_list = Array('i',range(self._size_locations))
		self._array_ind = Value('i',0)
		self._array_skip = Value('i',0)
		self._local_ind = 0
		self._local_skip = 0
		self._sorting_status = Value(c_bool,False) #True sorting, False not sorting
		self._local_list = np.array(range(self._size_locations))
		return

        def question(self):
                return "answer"
        
        def _switch_in_location_list(self,ind1,ind2):
                aux = self._location_list[ind1]
                self._location_list[ind1] = self._location_list[ind2]
                self._location_list[ind2] = aux
	
	def has_next(self):
                '''
                if (self.t1 == 0):
                        self._counter = 0
                        self.t1 = time.time()

                self._counter += 1

                if (self._counter == 10000):
                        t2 = time.time()
                        print(t2-self.t1)
                        return 'no_next'
                return 'yes_next'
                '''
                if (self.t1 == 0):
                        self.t1 = time.time()                
                
                array_ind = self._local_ind
                array_skip = self._local_skip
		if(array_ind < self._size_locations):
                        aux = self._local_list[array_ind]
                        self._next_location = aux
			self._sharedmem.set_next_location(aux)
			return 'yes_next'
		elif(array_skip < self._size_locations):
                        # Loop the elements
                        self._local_ind = self._local_skip
                        array_ind = self._local_ind
			self._sharedmem.set_next_location(self._local_list[array_ind])
                        return ['reached_end','yes_next']
                else:
                        t2 = time.time()
                        print(t2-self.t1)
			return 'no_next'

        def _set_iterating(self):
                if (not self._sharedmem.get_iteration_status()):
                        # Stop sorting if its sorting
                        self._sharedmem.set_iteration_status(True)
                                
                        # Sleep until sorting is finished, to avoid rewriting
                        while (self._get_sorting_status()):
                                time.sleep(0.01)

                        aux = self._local_ind
                        self._local_list[aux:] = self._location_list[aux:]

                self._put_next_location_first()
	
	def skip_next(self):
                self._set_iterating()
		self._add_to_array_ind()

	def reset_skipped(self):
                return

        def _put_next_location_first(self):
                next_loc = self._next_location
                array_ind = self._local_ind
                if (self._local_list[array_ind] == next_loc):
                        return

                aux_list = self._local_list
                ind = np.flatnonzero(aux_list[array_ind:] == next_loc)[0]
                aux_list[array_ind+1:array_ind+ind+1] = aux_list[array_ind:array_ind+ind]
                aux_list[array_ind] = next_loc
                
	def remove_next(self):
                #return
                self._set_iterating()
                
                #self._switch_in_location_list(self._get_array_ind(),self._get_array_skip())
                aux_list = self._local_list
                ind1 = self._local_ind
                ind2 = self._local_skip
                aux = aux_list[ind1]
                aux_list[ind1] = aux_list[ind2]
                aux_list[ind2] = aux
                
                self._local_ind += 1
                self._local_skip += 1
		return
	
	def reset_iterator(self):
		#Consider again the whole array
		#Array will not be in order
		self._set_array_ind(0)
		self._set_array_skip(0)
	
	def sort_locations(self):
		#Add to command queue to be processed in parallel
                self._location_list[:] = self._local_list
                self._set_array_ind(self._local_ind)
                self._set_array_skip(self._local_skip)
		self._add_to_command_queue("sort_locations")
	
	def _sort_locations(self):
		self._set_sorting_status(True)
		start_sorting = time.time()

		#Check if there is no current location
		loc_current = self._sharedmem.get_next_location()
		if(loc_current == -1):
			loc_current_pos = self._sharedmem.get_position()
		else:
			loc_current_pos = self._sharedmem.get_discretizer().get_position(loc_current)
		
		# Assume we won't remove the element
		array_ind = self._get_array_ind()
			
		indexes = np.array(self._location_list[array_ind:],np.int)
		
		#Set trajectory parameters
		aux_positions = self._sharedmem.get_discretizer().get_position(indexes)
		self._sharedmem.set_trajectory_parameters(loc_current_pos,grid=aux_positions,auxwp=False,overwp=True,check_ang=True,pond=True)

                #Order locations with straight distance first (fast)
		aux_locations = np.linalg.norm(aux_positions-loc_current_pos,axis=1)
		aux_sorted_ind = aux_locations.argsort()
		indexes = indexes[aux_sorted_ind]
		aux_positions = aux_positions[aux_sorted_ind]
		aux_locations = aux_locations[aux_sorted_ind]
		
                t1 = time.time()
		delta_time = t1 - start_sorting
		#self._add_to_print_queue("Sorting by straight paths finished, elapsed time: " + str(delta_time))
		
		index = 0
		while (index < len(indexes)):
			pos_elem = aux_positions[index]
			
			distance = self._sharedmem.calculate_trajectory(pos_elem)
			aux_locations[index] = distance
			
			if (self._sharedmem.get_iteration_status()):
				break
			
			index += 1
			#When there's only one processor, sleep to give time to other processes
			#time.sleep(0.01)
                
		delta_time = time.time() - t1
                t1 = time.time()
		#self._add_to_print_queue("Calculating trajectory paths finished, elapsed time: " + str(delta_time))
		self._add_to_print_queue("Calculated " + str(index) + " elements of " + str(len(aux_locations)) + " to be calculated")
		
                #Order and write to location list ordered elements (very fast)
		self._location_list[array_ind:] = indexes[aux_locations.argsort()]
		delta_time = time.time() - t1
                t1 = time.time()
		#self._add_to_print_queue("Sorting calculated elements finished, elapsed time: " + str(delta_time))
		
		#self._add_to_print_queue(str(self._location_list[array_ind:]))
		self._set_sorting_status(False)
		delta_time = time.time() - start_sorting
		self._add_to_print_queue("Sorting process finished, elapsed time: " + str(delta_time))
	
	def _set_sorting_status(self,status):
		self._sorting_status.value = status
		
	def _get_sorting_status(self):
		return self._sorting_status.value
	
	def _get_array_ind(self):
		return self._array_ind.value
	
	def _set_array_ind(self,value):
		self._array_ind.value = value
	
	def _add_to_array_ind(self):
		self._array_ind.value += 1

	def _get_array_skip(self):
		return self._array_skip.value
	
	def _set_array_skip(self,value):
		self._array_skip.value = value
	
	def _add_to_array_skip(self):
		self._array_skip.value += 1
