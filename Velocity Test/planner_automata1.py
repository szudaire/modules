#from threading import Thread
from multiprocessing import Process
from multiprocessing import Queue
import numpy as np
from os.path import expanduser
#from MAVProxy.modules import planner_util_message as mesg
#from MAVProxy.modules.planner_colors import pcolors

class Automata(Process):
	def __init__(self,module_list,queue_print,queue_message):
		super(Automata,self).__init__()
		
		self._queue_print = queue_print
		self._queue_message = queue_message
		
		self._load_automata()
		self._map_controllable_to_module(module_list)
		self._queue_event = Queue()	
		self._index = 0
		self._not_exit = 1
	
	def run(self):
		while (self._not_exit):
			#Stops when the only thing left to do is wait for the environment
			self._generate_controllables()
			if (not self._not_exit):
                                break
			
			#Wait for the environment
			event = self._queue_event.get()
			self._process_event(event)
	
	def _exit_routine(self):
		while (self._queue_event.qsize() > 0):
			self._queue_event.get()
	
	def get_event_queue(self):
		return self._queue_event
	
	def add_to_event_queue(self,event):
		self._queue_event.put(event)
		
	def _map_controllable_to_module(self,module_list):
		self._map = {}
		for controllable in self._controllable_events:
			for module in module_list:
				if (hasattr(module,controllable)):
					self._map[controllable] = getattr(module,controllable)
					break
		return
	
	def _generate_controllables(self):
		controllable_found = 1
		while (controllable_found):
			#Process all the events from the environment before searching for controllables
                        
                        while(self._queue_event.qsize() > 0):
                                event = self._queue_event.get()
                                self._process_event(event)
                        
			#Search for controllables
			controllable_found = 0

			#for elem in self._states[self._index][0]:
			if (len(self._states[self._index][0]) > 0):
                                elem = self._states[self._index][0][0]
                                self._index = elem[1]
				events = self._call_controllable(elem[0])
				controllable_found = 1
				if (events != None):
                                        for ev in events:
                                                self._process_event(ev)
				#break
		return
	
	def _add_to_print_queue(self,event):
		self._queue_print.put(pcolors.color(event,'yellow'))
		
	def _add_to_message_queue(self,code,text):
		self._queue_message.put(mesg.message_to_str(code,text))
	
	def _process_event(self,event):
		#self._add_to_print_queue(event)
                #print(event)
		if (event == 'exit'):
			self._not_exit = 0
			self._exit_routine()
			return

		for elem in self._states[self._index][1]:                        
			if(elem[0] == event):
				self._index = elem[1]
				return True #Accepted
		
		#self._add_to_print_queue("EVENT NOT ACCEPTED")
		#self._call_controllable('rtl')
		#self._add_to_message_queue(mesg.ERROR,"")
		self._not_exit = 0
		self._exit_routine()
		return False #Not Accepted
		
	def _call_controllable(self,controllable):
		return self._map[controllable]()
	
	def _load_automata(self):
                '''
		home = expanduser("~")
		filename = home + '/.cache/mavproxy/automata_data.txt'
		
		self._controllable_events = ['takeoff', 'go_next', 'land', 'abort_go',
						'skip_next', 'has_next', 'reset_iterator', 'sort_locations', 'remove_next',
						'has_Person_next','has_Person_current',
                                                'has_Island_next','has_Island_current',
                                                'has_Nemo_next','has_Nemo_current',
                                                'has_SRoom_next',
                                                'reset_skipped']
						
		self._controllable_events.append('rtl')
		
		file = open(filename,'r')
		lines = file.readlines()
		file.close()
		states_size = int(lines[3].strip())
		self._states = [[]]*states_size
		
		analysing_state = False
		num_state = 0
		for i in range(6,len(lines)):
			line = lines[i].strip()
			
			if(analysing_state == False):
				if (line[0] == 'Q'):
					analysing_state = True
					aux_vec = []
			if(analysing_state == True):
					aux_str = ''
					many_events = False
					action_set = False
					for char in line:
						if (char == '='):
							num_state = int(aux_str[1:].strip())
							continue
						
						if (char in ['(','{','|']):
							aux_str = ''
							continue
						
						if (char == '}'):
							many_events = True
							action = aux_str
							action_set = True
							continue
						
						if (many_events == False and char == '-'):
							action = aux_str
							action_set = True
							continue
						
						if (action_set == True):
							if (char == 'Q'):
								aux_str = ''
								continue
						
						if (char == ')'):
							num_next_state = int(aux_str.strip())
							analysing_state = False
							break
					
						aux_str = aux_str + char
					
					if(analysing_state == True):
						num_next_state = int(aux_str.strip())
						
					if (many_events == True):
						aux_lista = np.array([])
						aux_str = ''
						for j in range(len(action)):
							if (action[j] == ','):
								aux_lista = np.append(aux_lista,aux_str)
								aux_str = ''
								continue
							aux_str += action[j]
							if (j == len(action)-1):
								aux_lista = np.append(aux_lista,aux_str)
						action = aux_lista
					else:
						action = [action]
					
					for accion in action:
						accion = accion.strip()
						if (accion in self._controllable_events):
							aux_var = 'C'
						else:
							aux_var = 'A'
						aux_vec.append([accion,aux_var,num_next_state])
					
					if(analysing_state == False):
						self._states[num_state] = aux_vec
			
		self._states = np.array(self._states)
		'''
                
		self._controllable_events = ['has_next','question','remove_next']
		
		self._states = [
                        [[['has_next',1]],[]], #0
			[[],[['yes_next',2]]], #1
			[[['question',3]],[]], #2
			[[],[['answer',4]]], #3
			[[['remove_next',0]],[]] #4
                                ]
		
		
		return
		
		
