#from MAVProxy.modules.lib.ANUGA import lat_long_UTM_conversion as llutm
from ctypes import c_bool
from multiprocessing import Value
import numpy as np
#from MAVProxy.modules import planner_util_discretizer
#from MAVProxy.modules import planner_util_trajectory

class SharedMem():
	def __init__(self):
		#Position
		self._pos_lat = Value('d',0.0) #Y axis
		self._pos_lon = Value('d',0.0) #X axis
		self._altitude = Value('d',0.0)
		self._relative_altitude = Value('d',0.0)
		#Direction
		self._dir_lat = Value('d',0.0)
		self._dir_lon = Value('d',0.0)
		#Roll
		self._roll = Value('d',0.0)
		#Battery
		self._battery_per = Value('d',0.0)
		self._battery_vol = Value('d',0.0)
		#Iterator
		self._next_loc = Value('i',-1)
		self._current_loc = Value('i',-1)
		#Iteration_status is set to false with "land" and "go_next"
		self._iteration_status = Value(c_bool,False) #True iterating, False not iterating

                #Grid configuration
		self._flight_height = Value('d',100.0) #meters
		self._grid_param = Value('d',50.0) #meters
		
		#Discretizer
		#self._discretizer = planner_util_discretizer.Discretizer(self)
		
		#Plane trajectory
		self._control_mode = Value('i',0)
		# Parallel to wind: 0
		# Minimal: 1
		self._wind_dir_lat = Value('d',np.sin(np.pi/2))
		self._wind_dir_lon = Value('d',np.cos(np.pi/2))
		self._turn_radius = Value('d',0.0)
		
		#Arrived direction, added for better trajectory control
		self._arrived_dir_lat = Value('d',0.0)
		self._arrived_dir_lon = Value('d',0.0)
		self._arrived_dir_flag = Value(c_bool,False)

		#Auxiliary variables for meters to latlon
		self._dirx = np.array([1.0,0.0])
		self._diry = np.array([0.0,1.0])

		#Auxiliary variables for sharedmem
		#self._tj = planner_util_trajectory.Trajectory()
		self.scale_x = Value('d',0.0)
		self.scale_y = Value('d',0.0)

        def set_battery(self,per,vol):
                self._battery_per.value = per
                self._battery_vol.value = vol

        def get_battery(self):
                return self._battery_per.value,self._battery_vol.value
        
        def set_flight_height(self,value):
                self._flight_height.value = value

        def get_flight_height(self):
                return self._flight_height.value
        
        def set_grid_param(self,value):
                self._grid_param.value = value

        def get_grid_param(self):
                return self._grid_param.value
	
        def set_scale(self):
                retx,rety = self.meters2latlon(50.0)
                self.scale_x.value = rety/retx
                self.scale_y.value = 1.0

        def get_scale(self):
                return np.array([self.scale_x.value,self.scale_y.value])
	
	def set_arrived_dir_flag(self,value):
		self._arrived_dir_flag.value = value
	
	def get_arrived_dir_flag(self):
		return self._arrived_dir_flag.value
	
	def get_turn_radius(self):
		return self._turn_radius.value
	
	def set_turn_radius(self,radius):
                radius = self.meters2latlon(radius)[1]
		self._turn_radius.value = radius
		
	def get_wind_dir(self):
		return np.array([self._wind_dir_lon.value,self._wind_dir_lat.value])
	
	def set_wind_dir(self,wind_dir):
		self._wind_dir_lon.value = wind_dir[0]
		self._wind_dir_lat.value = wind_dir[1]
	
	def set_arrived_dir(self,dir):
		self._arrived_dir_lon.value = dir[0]
		self._arrived_dir_lat.value = dir[1]
		
	def get_arrived_dir(self):
		return np.array([self._arrived_dir_lon.value,self._arrived_dir_lat.value])
		
	def get_control_mode(self):
		return self._control_mode.value
	
	def set_control_mode(self,mode):
		self._control_mode.value = mode

	def get_roll(self):
                return self._roll.value

        def set_roll(self,value):
                self._roll.value = value
	
	def get_discretizer(self):
		return self._discretizer
	
	def get_altitude(self):
		return self._altitude.value
		
	def set_altitude(self,altitude):
		self._altitude.value = altitude
	
	def get_relative_altitude(self):
		return self._relative_altitude.value
		
	def set_relative_altitude(self,altitude):
		self._relative_altitude.value = altitude
		
	def get_position(self):
		return np.array([self._pos_lon.value,self._pos_lat.value])
	
	def set_position(self,position):
		self._pos_lat.value = position[1]
		self._pos_lon.value = position[0]
		
	def get_direction(self):
		return np.array([self._dir_lon.value,self._dir_lat.value])
	
	def set_direction(self,direction):
		self._dir_lat.value = direction[1]
		self._dir_lon.value = direction[0]
		
	def get_next_location(self):
		return self._next_loc.value
	
	def set_next_location(self,next_loc):
		self._next_loc.value = next_loc
		
	def get_current_location(self):
		return self._current_loc.value
	
	def set_current_location(self,current_loc):
		self._current_loc.value = current_loc
	
	def set_iteration_status(self,status):
		self._iteration_status.value = status
	
	def get_iteration_status(self):
		return self._iteration_status.value

	def meters2latlon(self,distance):
		position = self.get_position()
		(ZoneNumber, UTMEasting, UTMNorthing) = llutm.LLtoUTM(position[1],position[0])
		wp_utm_x = np.array([UTMEasting,UTMNorthing])
		wp_utm_y = np.array([UTMEasting,UTMNorthing])
		wp_utm_x += self._dirx*distance
		wp_utm_y += self._diry*distance
		(Lat_x,Long_x) = llutm.UTMtoLL(wp_utm_x[1],wp_utm_x[0],ZoneNumber,isSouthernHemisphere=True)
		(Lat_y,Long_y) = llutm.UTMtoLL(wp_utm_y[1],wp_utm_y[0],ZoneNumber,isSouthernHemisphere=True)
		wp_ll_x = np.array([Long_x,Lat_x])
                wp_ll_y = np.array([Long_y,Lat_y])
		
		dist_ll_x= np.linalg.norm(wp_ll_x-position,axis=0)
		dist_ll_y= np.linalg.norm(wp_ll_y-position,axis=0)
		return np.array([dist_ll_x,dist_ll_y])
	
	def set_trajectory_parameters(self,loc_current_pos,grid=None,auxwp=False,overwp=False,check_ang=False,pond=False):
		if (self.get_arrived_dir_flag() == False):
			self.set_arrived_dir_flag(True)
			arrived_dir = self.get_direction()
			self.set_arrived_dir(arrived_dir)
		else:
			arrived_dir = self.get_arrived_dir()

                self._auxwp = auxwp
                self._pond = pond
		turn_radius = self.get_turn_radius()
		scale = self.get_scale()
		self._tj.set_parameters(loc_current_pos,arrived_dir,turn_radius,scale,auxwp,overwp,check_ang)

		#Overwaypoint distance
		if (overwp):
                        overwp_dist = self.meters2latlon(10)
                        self._tj.set_overwp_dist(overwp_dist)

		#Grid to calculate ponderation
		if (pond):
                        self._tj.set_grid(grid)
		
	def calculate_trajectory(self,loc_next_pos,update_dir=False):
		if(self.get_control_mode() == 0):
			wind_dir = self.get_wind_dir()
			if (self._pond):
                                wp_list,distance,dir = self._tj.calcular_trayectoria_paralela_direccion_ponderada(loc_next_pos,wind_dir)
                        else:
                                wp_list,distance,dir = self._tj.calcular_trayectoria_paralela_direccion(loc_next_pos,wind_dir)
		else:
			wp_list,distance,dir = self._tj.calcular_trayectoria_minima(loc_next_pos)
		
		if(update_dir):
			self.set_arrived_dir(dir)
		
		if(self._auxwp == False):
			return distance
		
		return wp_list,distance,dir
