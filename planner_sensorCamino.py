import cv2 as cv
import numpy as np
import multiprocessing
import time

class SensorCamino(multiprocessing.Process):
	def __init__(self,planner):
		super(SensorCamino,self).__init__()
		self.planner = planner
		self.camara = self.planner.camara
		self.iterador = self.planner.iterador
		
		self.queue = multiprocessing.Queue()
		self.queue_packet = multiprocessing.Queue()
		self.queue_destinos = multiprocessing.Queue()
		self.queue_captura = multiprocessing.Queue()
		self.queue_captura_ind = multiprocessing.Queue()
		
		return
	
	def run(self):
		while (True):
			metodo = self.queue.get()
			if (metodo == 'exit'):
				while (self.queue_captura.qsize() > 0):
					self.queue_captura.get()
				while (self.queue_captura_ind.qsize() > 0):
					self.queue_captura_ind.get()
				while (self.queue_destinos.qsize() > 0):
					self.queue_destinos.get()
				while (self.queue_packet.qsize() > 0):
					self.queue_packet.get()
				while (self.queue.qsize() > 0):
					self.queue.get()
				cv.destroyWindow('caminos')
				break
			getattr(self,metodo)()
	
	def idle_task(self):
		return
	
	def agregar_destinos(self):
		cv.namedWindow('caminos', cv.CV_WINDOW_AUTOSIZE)
		cv.moveWindow('caminos',950,0)
		cv.startWindowThread()
		
		self.pos_destinos = self.queue_destinos.get()
		self.dest_cant = len(self.pos_destinos)
		
		self.imagenes_no_procesadas = []
		self.imagenes_ind = np.array([],np.int32)
		
		image_size = self.planner.avion.metros2latlon(self.planner.camara.field_of_view*100.0)
		self.dist_proc = 3*image_size
		self.distancia_camino = 0.72*image_size
		
		self.destinos = np.zeros((self.dest_cant,3),np.int32) #destino,hayCaminoActual,hayCaminoProx
		self.destinos[:,0] = np.array(range(self.dest_cant),np.int32)
	
	def registrar_automata(self,automata):
		self.automata = automata
		return
		
	def mavlink_packet(self):
		m = self.queue_packet.get()
		return
		
	def hayCaminoProx(self):
		dest_actual = self.iterador.dest_actual.value
		#self.planner.queue_print.put(str(dest_actual))
		dest_prox = self.iterador.dest_prox.value
		
		if(self.destinos[dest_prox,2] == 1):
			self.automata.queue_ambiente.put('siCaminoProx')
		else:
			self.automata.queue_ambiente.put('noCaminoProx')
		return
	
	def hayCaminoActual(self):	
		dest_actual = self.iterador.dest_actual.value
		self.procesar_capturas(dest_actual)
		
		self.planner.queue_print.put(str(dest_actual))
		
		if(self.destinos[dest_actual,1] == 1):
			self.automata.queue_ambiente.put('siCaminoActual')
		else:
			self.automata.queue_ambiente.put('noCaminoActual')
		return
	
	def procesar_capturas(self,destino):
		while (self.queue_captura.qsize() > 0):
			self.imagenes_no_procesadas.append(self.queue_captura.get())
			self.imagenes_ind = np.append(self.imagenes_ind,self.queue_captura_ind.get())
		
		if (len(self.imagenes_no_procesadas) == 0):
			return
		
		indice = np.flatnonzero(self.imagenes_ind == self.planner.iterador.dest_actual.value)
		
		for ind in indice:
			# La imagen ya estaba en la lista de capturadas
			self.procesar_imagen(ind)
			return
		
		#Si la imagen no estaba en la lista, buscar elementos cercanos para procesar
		cercanos = self.get_cercanos(destino)
		indices = []
		
		for i in range(len(self.imagenes_ind)):
			if (self.imagenes_ind[i] in cercanos):
				indices.append(i)
		
		indices = np.array(indices)
		indices.sort()
		
		if (len(indices) > 0):
			indices = np.flip(indices,0)
			for ind in indices:
				self.procesar_imagen(ind)
		return
		
	def marcar_costados(self,dest,ind):
		costados = self.iterador.getCostados(dest)
		
		for elem in costados:
			self.destinos[self.iterador.getDestino(elem),ind] = 1
		return
	
	def procesar_imagen(self,ind):
		t1 = time.time()
		
		dest_actual = self.imagenes_ind[ind]
		image_orig = self.imagenes_no_procesadas.pop(ind)
		self.imagenes_ind = np.delete(self.imagenes_ind,ind,axis=0)		
		image = cv.cvtColor(image_orig,cv.COLOR_RGB2GRAY)
		image = cv.equalizeHist(image)
		
		height = len(image)
		width = len(image[0])
		
		#Filtrado
		image_filt = image
		limit_thresh = 215
		np.place(image_filt, image_filt > limit_thresh,255)
		np.place(image_filt, image_filt <= limit_thresh,0)
		
		#Obtener contornos
		contours,hierarchy = cv.findContours(image_filt.copy(), cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
		
		#Generar contornos validos
		valid_contours = []
		for contour in contours:
			#Revisar como se eligen contornos validos
			if ((1 in contour) or (width-2 in contour[:,0,0]) or (height-2 in contour[:,0,1])):
				if(cv.arcLength(contour,True) > 100):
					valid_contours.append(contour)
		tol_pol = 5
		#Procesamiento y seleccionamiento de los poligonos
		poligon_vec = []
		for contour in valid_contours:
			poligon = cv.approxPolyDP(contour,tol_pol,True)
			#poligon = self.aproximar_contorno(contour,20)
			if (poligon is not None):
				if(len(poligon) < 0.1*cv.arcLength(poligon,True)):
					poligon_vec.append(poligon)
		
		valid_poligon = []
		for poligon in poligon_vec:
			indices = np.flatnonzero((poligon[:,0,0] < tol_pol+1) | (poligon[:,0,0] > width-2-tol_pol) | (poligon[:,0,1] < 1+tol_pol) | (poligon[:,0,1] > height-2-tol_pol))
			if (len(indices) == 1):
				continue
			cant_ind = 0
			for i in range(-1,len(indices)-1):
				if(cant_ind > 1):
					break
				dist = np.linalg.norm(poligon[indices[i+1],0] - poligon[indices[i],0],axis=0)
				if (dist > 70):
					if (np.absolute(poligon[indices[i],0,0] - poligon[indices[i+1],0,0]) > tol_pol and np.absolute(poligon[indices[i],0,1] - poligon[indices[i+1],0,1]) > tol_pol):
						cant_ind += 1
					else:
						if (indices[i+1] - indices[i] > 1):
							if (i==-1):
								if (indices[0] > 0 or indices[-1] < len(indices)-1):
									cant_ind += 1
							else:
								cant_ind += 1
			
			if(cant_ind > 1): #Checkeo de vertices pasado
				valid_poligon.append(poligon)
		
		#Marcar elemento actual
		if(len(valid_poligon) > 0):
			self.destinos[dest_actual,1] = 1
		
		#Marcar elementos cercanos si corresponde
		for poligon in valid_poligon:
			cercanos = self.get_cercanos(dest_actual)
			image_size = self.planner.avion.metros2latlon(self.planner.camara.field_of_view*100)
			if(len(cercanos) > 0):
				indices = np.flatnonzero((poligon[:,0,0] < tol_pol+1) | (poligon[:,0,0] > width-2-tol_pol) | (poligon[:,0,1] < tol_pol+1) | (poligon[:,0,1] > height-2-tol_pol))
				
				puntos = self.pos_destinos[dest_actual] + np.array([image_size/2,-image_size/2])
				#Acordarse de cambiar el tema de altura
				constante_conv = self.planner.avion.metros2latlon(1.0*(self.planner.camara.field_of_view*100.0)/self.camara.width)
				puntos = puntos + constante_conv*np.array([-poligon[indices,0,1],poligon[indices,0,0]],np.float_).T
				
				for punto in puntos:
					#distancia = np.linalg.norm(self.pos_destinos[cercanos] - punto,axis=1)
					#ind_dist = cercanos[np.flatnonzero(distancia < self.distancia_camino)]
					pos_cercanos = self.pos_destinos[cercanos]
					ind_dist = cercanos[np.flatnonzero((np.absolute(pos_cercanos[:,0] - punto[0]) < image_size/2) & (np.absolute(pos_cercanos[:,1] - punto[1]) < image_size/2))]
					
					#self.planner.queue_print.put(str(ind_dist))
					self.destinos[ind_dist,2] = 1
		
		self.planner.queue_print.put('Tiempo de procesamiento: ' + str(time.time() - t1))
		
		image_poligon = image_orig.copy()
		cv.drawContours(image_poligon,valid_poligon,-1,(0,0,255))
		cv.imshow('caminos',image_poligon)
		return
		
	def get_cercanos(self,destino):
		self.iterador.queue_get_cercanos_destino.put(destino)
		self.iterador.queue_get_cercanos_dist_proc.put(self.dist_proc)
		self.iterador.queue.put('get_cercanos')
		return self.iterador.queue_cercanos.get()
	

