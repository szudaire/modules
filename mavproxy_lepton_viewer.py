#!/usr/bin/env python
'''
Example Module
Peter barker, September 2016

This module simply serves as a starting point for your own MAVProxy module.

1. copy this module sidewise (e.g. "cp mavproxy_example.py mavproxy_coolfeature.py"
2. replace all instances of "example" with whatever your module should be called
(e.g. "coolfeature")

3. trim (or comment) out any functionality you do not need
'''

import os
import os.path
import numpy as np
import time

from MAVProxy.modules.lib.mp_module import MPModule
#from MAVProxy.modules.lib import mp_util
#from MAVProxy.modules.lib import mp_settings
from MAVProxy.modules.lib.textconsole import SimpleConsole
from MAVProxy.modules import planner_util_message as mesg

class lepton_viewer(MPModule):
	def __init__(self, mpstate):
		"""Initialise module"""
		super(lepton_viewer, self).__init__(mpstate, "lepton_viewer", "")
		#self.add_command('start', self.cmd_start, "start mission")
		self.add_command('ping', self.cmd_ping, "Ping onboard computer")
		self.add_command('capture', self.cmd_capture, "Capture photo")
		self.add_command('leppos', self.cmd_position, "Get position information")
	
		self.mpstate.functions.process_stdin("set moddebug 3", immediate=True)
		#self.mpstate.functions.process_stdin("module load graph", immediate=True)
		#self.mpstate.functions.process_stdin("set requireexit True", immediate=True)
		self._txt_console = SimpleConsole()
		
		self._init_time = time.time()
		self._config_time = 5
		self._configured = False
		
        def cmd_ping(self,args):
                self._ping_time = time.time()
                mesg.send_message(self,mesg.PING,"")

        def cmd_capture(self,args):
                mesg.send_message(self,mesg.CAPTURE,"")

        def cmd_position(self,args):
                mesg.send_message(self,mesg.POSITION,"")
	
	def _print_ln(self,print_str):
		self._txt_console.writeln(print_str)

	def _process_command(self,command):
		self.mpstate.functions.process_stdin(command, immediate=True)
	
	def idle_task(self):
                if (not self._configured):
                        if (time.time() > self._init_time + self._config_time):
                                self.mpstate.functions.process_stdin("map follow 0", immediate=True)
                                self._configured = True
                
	def mavlink_packet(self, m):
		mtype = m.get_type()
		
		if (mtype == "STATUSTEXT"):
			if (m.text[0] == '_'):
				code = mesg.code(m.text)
				if (code == mesg.PRINT):
					self._print_ln(mesg.message(m.text))
                                elif (code == mesg.PING):
                                        delta_time = time.time() - self._ping_time
                                        self._print_ln("Received PING from onboard computer. Elapsed time: " + str(delta_time))

		return

def init(mpstate):
	'''initialise module'''
	return lepton_viewer(mpstate)
