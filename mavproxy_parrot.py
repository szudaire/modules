#!/usr/bin/env python
'''
Example Module
Peter barker, September 2016

This module simply serves as a starting point for your own MAVProxy module.

1. copy this module sidewise (e.g. "cp mavproxy_example.py mavproxy_coolfeature.py"
2. replace all instances of "example" with whatever your module should be called
(e.g. "coolfeature")

3. trim (or comment) out any functionality you do not need
'''

import os
import os.path
import sys
from pymavlink import mavutil
import errno
import time

from MAVProxy.modules.lib import mp_module
from MAVProxy.modules.lib import mp_util
from MAVProxy.modules.lib import mp_settings

#From arproxy.py
EMERGENCY_CODE = 100
FLAT_TRIM_CODE = 101  # perform flat trim - ONLY DO THIS WHILE LANDED
CALIBRATE_MAGNETOMETER_CODE = 102  # calibrate magnetometer ONLY DO THIS IN THE AIR

class parrot(mp_module.MPModule):
    def __init__(self, mpstate):
        """Initialise module"""
        super(parrot, self).__init__(mpstate, "parrot", "")

        self.add_command('stop', self.cmd_emergency, "emergency call to parrot")
        self.add_command('trim', self.cmd_trim, "perform flat trim of parrot")
        self.add_command('cal', self.cmd_cal, "perform magnetometer calibration of parrot")
        self.add_command('TKOF', self.cmd_takeoff, "automatic takeoff of quadcopter")

        self._simulation = True
        self._wait_for_guided = False
        self._wait_takeoff = False
        self._wait_for_ack = False
        self._wait_time = 2

    def cmd_takeoff(self,args):
        if (self._simulation):
            self.mpstate.functions.process_stdin("mode guided", immediate=True)
            self._wait_for_guided = True
    
    def cmd_emergency(self, args):
        self.master.mav.command_long_send(
                self.settings.target_system,  # target_system
                mavutil.mavlink.MAV_COMP_ID_SYSTEM_CONTROL, # target_component
                EMERGENCY_CODE, # command
                0, # confirmation
                0, # param1
                0, # param2
                0, # param3
                0, # param4
                0, # param5
                0, # param6
                0) # param7
        
    def cmd_trim(self, args):
        self.master.mav.command_long_send(
                self.settings.target_system,  # target_system
                mavutil.mavlink.MAV_COMP_ID_SYSTEM_CONTROL, # target_component
                FLAT_TRIM_CODE, # command
                0, # confirmation
                0, # param1
                0, # param2
                0, # param3
                0, # param4
                0, # param5
                0, # param6
                0) # param7
        
    def cmd_cal(self, args):
        self.master.mav.command_long_send(
                self.settings.target_system,  # target_system
                mavutil.mavlink.MAV_COMP_ID_SYSTEM_CONTROL, # target_component
                CALIBRATE_MAGNETOMETER_CODE, # command
                0, # confirmation
                0, # param1
                0, # param2
                0, # param3
                0, # param4
                0, # param5
                0, # param6
                0) # param7

    def idle_task(self):
        if (self._wait_for_guided):
            if (self.mpstate.status.flightmode == 'GUIDED'):
                self._wait_for_ack = True
                self.mpstate.functions.process_stdin("arm throttle", immediate=True)
                self._wait_for_guided = False
        if (self._wait_takeoff):
            if (time.time() > self._takeoff_time + self._wait_time):
                self.mpstate.functions.process_stdin("auto", immediate=True)
                self._wait_takeoff = False
                self.mpstate.functions.process_stdin("param set SIM_SPEEDUP 1", immediate=True)
        pass

    def mavlink_packet(self, m):
        mtype = m.get_type()
        if (mtype == "COMMAND_ACK"):
            if m.command == 400:
                if (self._wait_for_ack):
                    self._wait_for_ack = False
                    self.mpstate.functions.process_stdin("takeoff 1", immediate=True)
                    self.mpstate.functions.process_stdin("param set SIM_SPEEDUP 1", immediate=True)
                    self._wait_takeoff = True
                    self._takeoff_time = time.time()
                
        pass

def init(mpstate):
    '''initialise module'''
    return parrot(mpstate)
