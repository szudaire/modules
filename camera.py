import cv2 as cv
import time
import subprocess

#apt-get install v4l-utils

video = cv.VideoCapture(1)
command = "v4l2-ctl -d 0 -c exposure_auto=1 -c exposure_absolute=1000"
output = subprocess.call(command, shell=True)
fourcc = cv.cv.CV_FOURCC(*'XVID')
out = cv.VideoWriter('output.avi',fourcc, 5.0, (640,480))
t1 = time.time()
delta = 0.2

while(True):
        if (time.time() > t1 + delta):
                t1 = time.time()
        else:
                time.sleep(0.05)
                continue

        start = time.time()
        ret, frame = video.read()

        #out.write(frame)

        end = time.time()
        print(end - start)
        cv.imshow('frame',frame)
        cv.waitKey(1)

video.release()
out.release()
cv.destroyAllWindows()
