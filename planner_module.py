from threading import Thread
#from multiprocessing import Process
from multiprocessing import Queue
from MAVProxy.modules import planner_util_message as mesg

class Module(Thread):
	def __init__(self,args):
		super(Module,self).__init__()
		self._queue_command = Queue()
		
		self._sharedmem = args[0]
		self._queue_print = args[1]
		self._queue_message = args[2]
		self._queue_mavproxy_command = args[3]
		self._mission = args[4]
		
		self._suscription_packets = []
		self._process_type = 0 #1 has independent processes, 0 no independent processes
		
		self.init()
		self._started_flag = False

		#self._controllables = []
	
	def init(self):
		return
	
	def _started(self):
		return
	
	def run(self):
		self._started()
		while (True):
			command = self._queue_command.get()
			if (command == 'exit'):
				self._exit_routine()
				break
			getattr(self,'_' + command)()
	
	def _exit_routine(self):
		while (self._queue_command.qsize() > 0):
			self._queue_command.get()

	def set_event_queue(self,event_queue):
		self._queue_event = event_queue
	
	def _add_to_event_queue(self,event):
		self._queue_event.put(event)
	
	def _add_to_command_queue(self,command):
		self._queue_command.put(command)
	
	def _add_to_mavproxy_command_queue(self,command):
		self._queue_mavproxy_command.put(command)
	
	def _add_to_print_queue(self,text):
		self._queue_print.put(text)

	def _add_to_message_queue(self,code,text):
		self._queue_message.put(mesg.aux_message_to_str(code,text))
	
	def get_suscription_packets(self):
		return self._suscription_packets
		
	def _set_suscription_packets(self,packets):
		self._suscription_packets = packets
	
	def start_process(self):
		if (self._process_type):
                        if (not self._started_flag):
                                self.start()
                                self._started_flag = True
		
	def exit_process(self):
		if (self._process_type):
			self._add_to_command_queue('exit')
			self._exit()

        def _exit(self):
                return
	
	def join_process(self):
		if (self._process_type):
			self.join()
	
	def mavlink_packet(self,m):
		return
	
