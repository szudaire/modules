from Lepton3 import Lepton3
import cv2 as cv
import time
from picamera import PiCamera
from os.path import expanduser
import numpy as np

class Camera():
	def __init__(self):
		home = expanduser("~")
		self.path = home + '/.cache/mavproxy/captures/'
		
		self._started()
		return
	
	def _started(self):
                
                #This causes a memory leak, don't use namedwindow and replace it with waitKey(0)
                '''
		cv.namedWindow('capture thermal', cv.CV_WINDOW_AUTOSIZE)
                cv.namedWindow('capture rgb', cv.CV_WINDOW_AUTOSIZE)
                cv.startWindowThread()
                '''
                self.lepton = Lepton3()
                
                self.width_rgb = 640
                self.height_rgb = 480
                
                self.picam = PiCamera()
                self.picam.resolution = (self.width_rgb, self.height_rgb)
                self.picam.framerate = 24

                self._cant_error = 0
                self._cant_timeout = 0
                self._cant_ok = 0
                self._max_time = 0
                self._max_code = ""
                
	def _capture(self):
                #self._add_to_print_queue("Roll value: " + str(self._sharedmem.get_roll()))
                
                captured = self._capture_thermal()
                if (captured):
                        self._capture_rgb()
		return

        def _capture_rgb(self):
                t1 = time.time()
                
                image = np.empty((self.height_rgb * self.width_rgb * 3,), dtype=np.uint8)
                self.picam.capture(image,format='bgr',use_video_port=True)
                image = image.reshape((self.height_rgb, self.width_rgb, 3))
                
                t2 = time.time()
                print("RGB in " + str(t2-t1) + " secs.")

                cv.imwrite(self.path + str(self._cant_ok) + "_rgb.png",image)
                #cv.imshow('capture rgb',image)
                return
        
	def _capture_thermal(self):
                t1 = time.time()
                error = False
                correct = False
                with self.lepton:
                        while (not correct):
                                timeout,frame_ushort,frame_id = self.lepton.capture(timeout=0.15)
                                
                                if (not timeout):
                                        ind = np.where(frame_ushort == 0)
                                        if (len(ind[0]) == 0):
                                                correct = True
                                                break
                                        else:
                                                error = True
                                                break
                                else:
                                        break
                
                if (error):
                        t2 = time.time()
                        #print("Error frame TIMEOUT in " + str(t2-t1))
                        self._cant_error += 1
                        code = "ERROR"
                elif (timeout):
                        t2 = time.time()
                        #print("Garbage frame TIMEOUT in "+ str(t2-t1))
                        self._cant_timeout += 1
                        code = "GARBAGE"
                else:
                        #EXTRA CONTRAST
                        #cv.normalize(frame_ushort, frame_ushort, 0, 65535, cv.NORM_MINMAX) # extend contrast
                        #frame_byte = np.uint8(np.right_shift(frame_ushort, 8)) # fit data into 8 bits
                        
                        #CALIBRATED
                        frame_ushort = (frame_ushort - 7300)*31
                        frame_byte = np.uint8(np.right_shift(frame_ushort, 8))
                        
                        t2 = time.time()
                        self._cant_ok += 1
                        code = "OK"

                        image = frame_byte
                        cv.imwrite(self.path + str(self._cant_ok) + "_thermal.png",image)
                        ratio = 3
                        image = cv.resize(frame_byte,(160*ratio,120*ratio))
                        #cv.imshow('capture thermal',image)

                if (t2 - t1 > self._max_time):
                        self._max_time = t2-t1
                        self._max_code = code
                total = self._cant_error + self._cant_timeout + self._cant_ok
                print("RESULTS: E: " + str(self._cant_error) + " G: " + str(self._cant_timeout) + " OK: " + str(self._cant_ok) + " Total: " + str(total))
                print("Max time: " + str(self._max_time) + " Max code: " + self._max_code)
                
                return correct
        
