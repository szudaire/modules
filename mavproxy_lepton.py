#!/usr/bin/env python
'''
Example Module
Peter barker, September 2016

This module simply serves as a starting point for your own MAVProxy module.

1. copy this module sidewise (e.g. "cp mavproxy_example.py mavproxy_coolfeature.py"
2. replace all instances of "example" with whatever your module should be called
(e.g. "coolfeature")

3. trim (or comment) out any functionality you do not need
'''

import os
import os.path
import numpy as np
import time

from MAVProxy.modules.lib.mp_module import MPModule
#from MAVProxy.modules.lib import mp_util
#from MAVProxy.modules.lib import mp_settings
from MAVProxy.modules.lib.textconsole import SimpleConsole
from MAVProxy.modules import planner_util_message as mesg
from MAVProxy.modules.Lepton3 import Lepton3

import numpy as np
import cv2 as cv
from picamera import PiCamera
from os.path import expanduser
import math

class lepton(MPModule):
	def __init__(self, mpstate):
		"""Initialise module"""
		super(lepton, self).__init__(mpstate, "lepton", "")
		#self.add_command('start', self.cmd_start, "start mission")
	
		self.mpstate.functions.process_stdin("set moddebug 3", immediate=True)
		#self.mpstate.functions.process_stdin("module load graph", immediate=True)
		#self.mpstate.functions.process_stdin("set requireexit True", immediate=True)
		self._txt_console = SimpleConsole()
		
		self._init_time = time.time()
		self._config_time = 5
		self._configured = False
		
                self.lepton = Lepton3()
                
                self.width_rgb = 640
                self.height_rgb = 480
                
                self.picam = PiCamera()
                self.picam.resolution = (self.width_rgb, self.height_rgb)
                self.picam.framerate = 24
                
                self.lat = 0
		self.lon = 0
		self.relative_alt = 0
		self.alt = 0
		self.roll = 0
		self.direction = 0

                self._cant_error = 0
                self._cant_timeout = 0
                self._cant_ok = 0
                self._max_time = 0
                self._max_code = ""

                home = expanduser("~")
		self.path = home + '/.cache/mavproxy/captures/'
		
        def ping(self):
                mesg.send_message(self,mesg.PING,"")

        def capture(self):
                self._capture_rgb()
                self._capture_thermal()
                return

        def position(self):
                mesg.send_message(self,mesg.PRINT,"Lat: " + str(self.lat) + " Lon: " + str(self.lon))
                mesg.send_message(self,mesg.PRINT,"Rel. alt.: " + str(self.relative_alt))
                mesg.send_message(self,mesg.PRINT,"Alt: " + str(self.alt) + " Roll: " + str(self.roll))
                mesg.send_message(self,mesg.PRINT,"Direction: " + str(self.direction))

        def _capture_thermal(self):
                t1 = time.time()
                error = False
                correct = False
                with self.lepton:
                        while (not correct):
                                timeout,frame_ushort,frame_id = self.lepton.capture(timeout=0.2)
                                
                                if (not timeout):
                                        ind = np.where(frame_ushort == 0)
                                        if (len(ind[0]) == 0):
                                                correct = True
                                        else:
                                                error = True
                                                break
                                else:
                                        break
                
                if (error):
                        t2 = time.time()
                        #print("Error frame TIMEOUT in " + str(t2-t1))
                        self._cant_error += 1
                        code = "ERROR"
                elif (timeout):
                        t2 = time.time()
                        #print("Garbage frame TIMEOUT in "+ str(t2-t1))
                        self._cant_timeout += 1
                        code = "GARBAGE"
                else:
                        #EXTRA CONTRAST
                        #cv.normalize(frame_ushort, frame_ushort, 0, 65535, cv.NORM_MINMAX) # extend contrast
                        #frame_byte = np.uint8(np.right_shift(frame_ushort, 8)) # fit data into 8 bits

                        #CALIBRATED
                        frame_ushort = (frame_ushort - 7300)*31
                        frame_byte = np.uint8(np.right_shift(frame_ushort, 8))
                        
                        t2 = time.time()
                        self._cant_ok += 1
                        code = "OK"
                                
                        ratio = 3
                        image = cv.resize(frame_byte,(160*ratio,120*ratio))
                        cv.imshow('capture thermal',image)

                if (t2 - t1 > self._max_time):
                        self._max_time = t2-t1
                        self._max_code = code
                total = self._cant_error + self._cant_timeout + self._cant_ok
                mesg.send_message(self,mesg.PRINT,"RESULTS: E: " + str(self._cant_error) + " G: " + str(self._cant_timeout) + " OK: " + str(self._cant_ok) + " Total: " + str(total))
                mesg.send_message(self,mesg.PRINT,"Max time: " + str(self._max_time) + " Max code: " + self._max_code)
                #cv.imwrite(self.path + "1.png",image)
                return
        def _capture_rgb(self):
                t1 = time.time()
                
                image = np.empty((self.height_rgb * self.width_rgb * 3,), dtype=np.uint8)
                self.picam.capture(image,format='bgr',use_video_port=True)
                image = image.reshape((self.height_rgb, self.width_rgb, 3))
                
                t2 = time.time()
                mesg.send_message(self,mesg.PRINT,"RGB in " + str(t2-t1) + " secs.")

                cv.imshow('capture rgb',image)
                return
	
	def _print_ln(self,print_str):
		self._txt_console.writeln(print_str)

	def _process_command(self,command):
		self.mpstate.functions.process_stdin(command, immediate=True)
	
	def idle_task(self):
                if (not self._configured):
                        if (time.time() > self._init_time + self._config_time):
                                cv.namedWindow('capture thermal', cv.CV_WINDOW_AUTOSIZE)
                                cv.namedWindow('capture rgb', cv.CV_WINDOW_AUTOSIZE)
                                cv.startWindowThread()

                                self._configured = True

        def _position_update(self, GLOBAL_POSITION_INT):
		'''update gps position'''
		self.lat = GLOBAL_POSITION_INT.lat*0.0000001
		self.lon = GLOBAL_POSITION_INT.lon*0.0000001
		self.relative_alt = GLOBAL_POSITION_INT.relative_alt*0.001 # mm2meter conversion
		self.alt = GLOBAL_POSITION_INT.alt*0.001
		
		'''update ground velocity'''
		vx = GLOBAL_POSITION_INT.vx*0.01 # cm2metre
		vy = GLOBAL_POSITION_INT.vy*0.01 # cm2metre
		vz = GLOBAL_POSITION_INT.vz*0.01 # cm2metre
		
		'''update heading angle'''
		hdg = GLOBAL_POSITION_INT.hdg*0.01 # cdeg2deg
		
		direction = np.array([vy,vx])
		mod = np.linalg.norm(direction,axis=0)
		
		if(mod > 2): # m/s
			self.direction = direction/mod
		else:
			self.direction = np.array([math.sin(math.radians(hdg)),math.cos(math.radians(hdg))])		

        def _attitude_update(self, ATTITUDE):
                self.roll = ATTITUDE.roll
	
	def mavlink_packet(self, m):
		mtype = m.get_type()
		
		if (mtype == "STATUSTEXT"):
			if (m.text[0] == '_'):
				code = mesg.code(m.text)
				if (code == mesg.CAPTURE):
					self.capture()
                                elif (code == mesg.PING):
                                        self.ping()
                                elif (code == mesg.POSITION):
                                        self.position()

		elif mtype == "GLOBAL_POSITION_INT":
			self._position_update(m)

                elif mtype == "ATTITUDE":
                        self._attitude_update(m)
                        
		return

def init(mpstate):
	'''initialise module'''
	return lepton(mpstate)
