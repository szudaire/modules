# mission DATA_COLLECT
# mission COW
# mission FIRE
#mission SIM_PATROL
mission SIM_FOLLOW
#mission SIM_DELIVERY

#automata Explicit/test.txt

#uav_type quad
#sort lindist
#sort none
#reached_word at
reached_word arrived

uav_type plane
sort dist
motion multi
#motion uni

#height 20
#low_height 10
#high_height 20
#grid_param 50
#grid_angle 45

height 100
grid_param 200
grid_angle 0

turn_radius 50
#turn_radius 85

#height 1.5
#low_height 1
#high_height 2
#grid_param 10
#grid_angle 140

mark_wps True
auto_takeoff False
parrot False

no_disc False
