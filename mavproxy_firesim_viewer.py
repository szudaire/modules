#!/usr/bin/env python

import os
import os.path
import sys
from pymavlink import mavutil
import errno
import time
import socket
import thread
import numpy as np

from MAVProxy.modules.lib import mp_module
from MAVProxy.modules.lib import mp_util
from MAVProxy.modules.lib import mp_settings
from MAVProxy.modules import planner_util_message as mesg
from MAVProxy.modules.lib.ANUGA import lat_long_UTM_conversion as llutm
from MAVProxy.modules.mavproxy_map import mp_slipmap

from multiprocessing import Queue
import cv2 as cv

class firesim_viewer(mp_module.MPModule):
    def __init__(self, mpstate):
        """Initialise module"""
        super(firesim_viewer, self).__init__(mpstate, "firesim_viewer", "")
        self.mpstate.functions.process_stdin("set moddebug 3", immediate=True)
        self._queue_message = Queue()
        self._queue_fire = Queue()

        self._nx = 801
        self._ny = 801
        self._x_orig = 268962.0
        self._y_orig = 5401414.0
        self._cellsize = 30.0
        self.identifier = 'FIRESIM_VIEWER'

        self._maxi = 0
        self._fire = np.zeros((self._nx,self._ny),np.uint8)
        self._unsafe = np.zeros((self._nx,self._ny),np.uint8)

    def receive_data_from_socket(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        try: 
            host_name = socket.gethostname() 
            host_ip = socket.gethostbyname(host_name + ".local")
        except:
            self._add_to_message_queue(mesg.ERROR,"Unable to get Host IP")
            return
        
        s.bind((host_ip, mesg.PORT_FIRE))
        s.listen(1)
        self._add_to_message_queue(mesg.FIREIP,host_ip)
        
        conn, addr = s.accept()

        size_msg = self._nx*self._ny
        message = ''
        while 1:
            data = conn.recv(1024)
            if not data:
                break
            
            if (len(message + data) >= size_msg):
                ind = size_msg - len(message)
                message += data[:ind]
                terrain_flat = np.fromstring(message,np.uint8)
                self._queue_fire.put(terrain_flat)
                message = data[ind:]
            else:
                message += data
                
        conn.close()
        s.close()

    def _add_to_message_queue(self,code,text):
	self._queue_message.put(mesg.aux_message_to_str(code,text))
	
    def idle_task(self):
        while(self._queue_message.qsize() > 0):
            message = self._queue_message.get()
            code = mesg.code(message)
            text = mesg.message(message)
            mesg.send_message(self,code,text)

        repaint = False
        while(self._queue_fire.qsize() > 0):
            terrain = self._queue_fire.get()
            terrain = terrain.reshape((self._nx,self._ny))
            self._fire[np.where(terrain == 11)] = 1
            self._unsafe[np.where(terrain > 0)] = 1
            #print terrain[np.where(terrain != 0)]
            repaint = True

        if repaint:
            t1 = time.time()
            for i in range(self._maxi+1):
                try:
                    map_obj.remove_object("fire" + str(i))
                except:
                    pass
            i = 0
            contours, hierarchy = cv.findContours(self._fire, cv.RETR_TREE, cv.CHAIN_APPROX_NONE)
            i = self.paint_contours(contours,[180,0,0],i)
            '''
            contours, hierarchy = cv.findContours(self._unsafe, cv.RETR_TREE, cv.CHAIN_APPROX_NONE)
            i = self.paint_contours(contours,[0,0,180],i)
            '''
            #print "Repaint delta time: " + str(time.time() - t1)

    def paint_contours(self,contours,color,i_start):
        i = i_start
        for contour in contours:
            if (i > self._maxi):
                self._maxi = i
            contour = contour[:,0,:]
            points = []
            for elem in contour:
                x_utm = self._x_orig + elem[0] * self._cellsize
                y_utm = self._y_orig + (self._ny - elem[1]) * self._cellsize
                (ZoneNumber, UTMEasting, UTMNorthing) = llutm.LLtoUTM(self._pos_lat,self._pos_lon)
                (lat,lon) = llutm.UTMtoLL(y_utm,x_utm,ZoneNumber,isSouthernHemisphere=True)
                points.append([lat,lon])

            map_module = self.mpstate.module("map")
            map_obj = map_module.map

            points.append(points[0])
            map_obj.add_object(mp_slipmap.SlipPolygon('fire' + str(i),
                        points, 2, color, 2, arrow = False, popup_menu=None))
            i += 1
        return i
    
    def _process_command(self,command):
        self.mpstate.functions.process_stdin(command, immediate=True)

    def mavlink_packet(self, m):
        mtype = m.get_type()
        
        if mtype == "GLOBAL_POSITION_INT":
            self._pos_lat = m.lat*0.0000001
            self._pos_lon = m.lon*0.0000001

        if (mtype == "STATUSTEXT"):
            if (mesg.analyze(self,m.text[0])):
                code = mesg.code(m.text)
                if (code == mesg.FIRESTART):
                    thread.start_new_thread(self.receive_data_from_socket,())
                
def init(mpstate):
    '''initialise module'''
    return firesim_viewer(mpstate)
