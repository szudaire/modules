from MAVProxy.modules.planner_module import Module

class Battery(Module):
	def init(self):
		self._set_suscription_packets(["SYS_STATUS"])
		self._battery_thresh = -3000000 #Percent
		self._battery_warning = False
		self.controllables = []
		return
	
	def mavlink_packet(self,m):
		mtype = m.get_type()
		if mtype == "SYS_STATUS":
                        self._sharedmem.set_battery(m.battery_remaining,m.voltage_battery)
                        
			if (self._battery_warning == False):
				if(m.battery_remaining <= self._battery_thresh):
					self._add_to_event_queue('no_battery')
					self._battery_warning = True
			
