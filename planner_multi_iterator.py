from MAVProxy.modules.planner_module import Module
from multiprocessing import Array,Value
from ctypes import c_bool
import numpy as np
import time
from MAVProxy.modules import planner_util_message as mesg
from MAVProxy.modules.planner_util_stat import levy

class Iterator(Module):
    def init(self):
        self._process_type = 0 #1 has independent processes, 0 no independent processes

        self.controllables = ['go_next','go_same']

        self.region_num = 0
        self.regions = {}
        self.indexes = {}
        self.locs = {}
        self._sharedmem._multi_iterator.value = True
        self._pond = False

    def add_robot(self,robot):
        self.robot = robot
        
    def add_locations(self,pos_locations):
        return

    def add_region(self,region_name,region_locs):
        self.regions[region_name] = self.region_num
        self.region_num += 1
        self.indexes[region_name] = 0
        self.locs[region_name] = region_locs

    def go_next(self,region):
        self.sort_locs(region)
        loc = self.locs[region][self.indexes[region]]
        
        self.indexes[region] += 1
        if self.indexes[region] == len(self.locs[region])-1:
            self._sharedmem._at_response.value = 1
        elif self.indexes[region] >= len(self.locs[region]):
            self._sharedmem._at_response.value = 2
            self.indexes[region] = 0
        else:
            self._sharedmem._at_response.value = 0
        self._go_to_loc(loc,region)

    def go_same(self,region):
        loc = self.locs[region][self.indexes[region]]
        self._sharedmem._at_response.value = -1
        self._go_to_loc(loc,region)

    def _go_to_loc(self,loc,region):
        self._sharedmem._region_response.value = self.regions[region]
        self.robot.go(loc)

    def sort_locs(self,region):
        #Check if there is no current location
        loc_current = self._sharedmem.get_next_location()
        if(loc_current == -1):
            loc_current_pos = self._sharedmem.get_position()
        else:
            loc_current_pos = self._sharedmem.get_discretizer().get_position(loc_current)
        
        indexes = np.array(self.locs[region][self.indexes[region]:],np.int)
        
        if (1):#self._sort_type == SORT_DIST):
            if (len(indexes) <= 1):
                self._add_to_print_queue("Sorting process finished, nothing to sort")
                return
            #Set trajectory parameters
            aux_positions = self._sharedmem.get_discretizer().get_position(indexes)
            self._sharedmem.set_trajectory_parameters(loc_current_pos,grid=aux_positions,auxwp=False,overwp=True,check_ang=True,pond=self._pond)

            #Order locations with straight distance first (fast)
            aux_locations = np.linalg.norm((aux_positions-loc_current_pos)*self._sharedmem.get_scale(),axis=1)
            aux_sorted_ind = aux_locations.argsort()
            indexes = indexes[aux_sorted_ind]
            aux_positions = aux_positions[aux_sorted_ind]
            aux_locations = aux_locations[aux_sorted_ind]
            
            index = 0
            batch_size = 1000
            while (index < len(indexes)):
                if (index + batch_size > len(indexes)):
                    batch_size = len(indexes) - index
                pos_array = aux_positions[index:index+batch_size]

                #Opcion python
                '''
                dist_array = np.zeros(len(pos_array))
                for j in range(len(dist_array)):
                    dist_array[j] = self._sharedmem.calculate_trajectory(pos_array[j])
                '''
                #Opcion clib
                dist_array = self._sharedmem.batch_calculate_trajectory(pos_array)
                
                aux_locations[index:index+batch_size] = dist_array
                
                index += batch_size
                #When there's only one processor, sleep to give time to other processes
                #time.sleep(0.01)
            
            self._add_to_print_queue("Calculated " + str(index) + " elements of " + str(len(aux_locations)) + " to be calculated")
            
            #Order and write to location list ordered elements (very fast)
            self.locs[region][self.indexes[region]:] = indexes[aux_locations.argsort()]
            
        
    
