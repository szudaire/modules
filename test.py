import planner_util_trajectory as ty
import numpy as np
import math
import time

pos_inicial = np.array([0.0,0.0])
pos_final = np.array([0.0,0.0])
dir_inicial = np.array([0.0,1.0])
dir_final = np.array([0.0,1.0])
radio = 20.0

tray = ty.Trajectory()
scale = np.array([1.0,1.0])
auxwp = False
overwp = False
check_ang = True
tray.set_parameters(pos_inicial, dir_inicial,radio,scale,auxwp,overwp,check_ang)
tray.set_overwp_dist(10)

t1 = time.time()
wp_list,dist,dir = tray.calcular_trayectoria_paralela_direccion(pos_final,dir_final)
#wp_list,dist,dir = tray.calcular_trayectoria_minima(pos_final)	
t2 = time.time()
print("Delta time: " + str(t2-t1))              

print(wp_list)
print(dist)
print(dir)


