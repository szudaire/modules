from MAVProxy.modules.planner_module import Module
from MAVProxy.modules import planner_util_message as mesg
from MAVProxy.modules.lib.ANUGA import lat_long_UTM_conversion as llutm
import numpy as np
import time
from multiprocessing import Value
from random import random

class Battery(Module):
    def init(self):
        self._set_suscription_packets(["GLOBAL_POSITION_INT"])

        self._orig_budget = 1200 #seconds
        self._sharedmem.set_budget(self._orig_budget) #seconds
        
        self._sharedmem.set_battery_warning(0)
        self._sharedmem.set_total_dist(0)
        self.controllables = ['bat_level',"return_base"]

        self._time_set = False
        self._sharedmem.perc_hi.value = 0.6
        self._sharedmem.perc_med.value = 0.2
        return

    def return_base(self):
        self._add_to_mavproxy_command_queue("rtl")
        return

    def bat_level(self):
        if self._sharedmem.get_budget() - self._sharedmem.get_total_dist() > self._sharedmem.perc_hi.value * self._sharedmem.get_budget():
            return "high_bat"
        elif self._sharedmem.get_budget() - self._sharedmem.get_total_dist() > self._sharedmem.perc_med.value * self._sharedmem.get_budget():
            return "medium_bat"
        else:
            if (self._sharedmem.get_total_dist() > self._sharedmem.get_budget()):
                self._add_to_mavproxy_command_queue("rtl")
            return "low_bat"
	
    def mavlink_packet(self,m):
        mtype = m.get_type()
        if mtype == "GLOBAL_POSITION_INT":
            vx = m.vx*0.01
            vy = m.vy*0.01
            vel = np.sqrt(vx**2 + vy**2)

            if vel < 10: #Don't start counting until takeoff
                return

            
            if not self._time_set:
                self._time = time.time()
                self._time_set = True
            else:
                
                if time.time() - self._time < 0.1:
                    return

                dt = time.time() - self._time
                self._time = time.time()
                aux = self._sharedmem.get_total_dist() #Now reinterpreted as time
                
                aux += dt
                self._sharedmem.set_total_dist(aux)


def model_step(x,y,ang,vel,omega,dt):
    x_dot = vel*np.cos(ang)
    y_dot = vel*np.sin(ang)
    ang_dot = omega

    x += x_dot*dt
    y += y_dot*dt
    ang += ang_dot*dt

    return x,y,ang

class Target(Module):
    def init(self):
        self.controllables = ['distance']

        self.x = Value('d',550.0) #hay que sumar 50 metros por las locs
        self.y = Value('d',550.0) #hay que sumar 50 metros por las locs
        self.ang = Value('d',0.0)

        self.random_interval = 10
        self.random_time = self.random_interval
        self.report_time = 0
        self.omega = 0
        self.vel = 0
        self.dt = 0.1

        self._sharedmem.high_dist_threshold.value = 500
        self._sharedmem.low_dist_threshold.value = 250
        self._sharedmem.x0target.value = self.x.value
        self._sharedmem.y0target.value = self.y.value
        self._sharedmem.yaw0target.value = self.ang.value
        
        self._process_type = 1

    def _started(self):
        self._sharedmem.update_time.value = time.time()
        self._add_to_command_queue("simu_step")

    def report(self):
        vec_x = self._sharedmem.meters2latlon(self.x.value-50)[0]
        vec_y = self._sharedmem.meters2latlon(self.y.value-50)[1]
        loc_current_pos = self._sharedmem.get_discretizer().get_position(0)
        self._add_to_message_queue(mesg.TARGET,str(loc_current_pos[1]+vec_y) + ' ' + str(loc_current_pos[0]+vec_x))
        
        ftest = open("/root/.cache/mavproxy/logs/target_pos.txt",'a+')
        ftest.write(str(self.x.value)+","+str(self.y.value)+"\n")
        ftest.close()

    def _simu_step(self):
        self.random_time += self.dt

        if self.random_time > self.random_interval:
            self.random_time = 0
            self.omega = 0.2*(random() - 0.5)
            self.vel = 5*random() + 5
            #self.vel = 3*random() + 3 #Changed model is slower
            #self.vel = 7*random() + 7 #Changed model is slower

        self.report_time += self.dt
        if self.report_time > 1:
            self.report_time = 0
            self.report()

        self.x.value,self.y.value,self.ang.value = model_step(self.x.value,self.y.value,self.ang.value,self.vel,self.omega,self.dt)
        time.sleep(self.dt)

        self._add_to_command_queue("simu_step")

    def distance(self):
        position = self._sharedmem.get_position()
        (ZoneNumber, UTMEasting, UTMNorthing) = llutm.LLtoUTM(position[1],position[0])
        loc0 = self._sharedmem.get_discretizer().get_position(0)
        (ZoneNumber, UTMEasting0, UTMNorthing0) = llutm.LLtoUTM(loc0[1],loc0[0])

        x_uav = UTMEasting - UTMEasting0 + 50
        y_uav = UTMNorthing - UTMNorthing0 + 50

        dist = np.sqrt((x_uav-self.x.value)**2 + (y_uav-self.y.value)**2)
        
        self._add_to_message_queue(mesg.PRINT,"Dist: "+str(dist))
        
        ftest = open("/root/.cache/mavproxy/logs/time_log.txt",'a+')
        ftest.write("dist: "+str(dist)+",")
        
        if dist < self._sharedmem.high_dist_threshold.value: #solo actualizar la posicion si se detecta el UAV
            self._sharedmem.x0target.value = self.x.value
            self._sharedmem.y0target.value = self.y.value
            self._sharedmem.yaw0target.value = self.ang.value
            self._sharedmem.update_time.value = time.time()

            if dist < self._sharedmem.low_dist_threshold.value:
                self._add_to_event_queue('close')
                ftest.write("close\n")
            else:
                self._add_to_event_queue('far')
                ftest.write("far\n")
        else:
            self._add_to_event_queue('not_seen')
            ftest.write("not_seen\n")
        
        ftest.close()
    
