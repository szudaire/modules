set PlaneC = {takeoff, go_next, land, abort_go,battery_charged,sort_locations}
set PlaneU = {initial_config, arrived, takeoff_ended, landed}
set PlaneA = {PlaneC,PlaneU}

set IteratorC = {has_next,reset_iterator,remove_next}
set IteratorU = {yes_next, no_next}
set IteratorA = {IteratorC,IteratorU}

set SensorsC = {is_safe_next,capture}
set SensorsU = {yes_safe_next,no_safe_next,yes_person,no_person}
set Sensors = {SensorsC,SensorsU}

Plane = (initial_config -> ConfigDone),
ConfigDone = (	takeoff -> sort_locations -> takeoff_ended -> TakeoffEnded),
TakeoffEnded = (	{IteratorC,SensorsC} -> TakeoffEnded | 
					go_next -> sort_locations -> GoTo |
					land -> landed -> Landed),
Landed = (battery_charged -> Plane),
GoTo = (arrived -> TakeoffEnded | 
		abort_go -> TakeoffEnded).

Iterator = (has_next -> (yes_next -> YesNext | no_next -> NoNext)),
YesNext = (remove_next -> Iterator),
NoNext = (reset_iterator -> Iterator).

CurrentQuestions = (has_next -> CurrentQuestions | arrived -> CQNext),
CQNext = (has_next -> CurrentQuestions | {camera} -> CQNext).

GoNext = (yes_next -> (go_next -> ({remove_next} -> GoNext) 
		| {remove_next} -> GoNext)).

NextQuestions = ({remove_next} -> NextQuestions | yes_next -> NQYes),
NQYes = ({remove_next} -> NextQuestions | {is_safe_next} -> NQYes).

SafeSensor = (is_safe_next -> (yes_safe_next -> SafeSensor | no_safe_next -> SafeSensor)).
Camera = (capture -> (yes_person -> Camera | no_person -> Camera)).

set Alphabet = {IteratorA,PlaneA,Sensors}
set Controlables = {IteratorC,PlaneC,SensorsC}
||Environment = (Plane || Iterator || NextQuestions || GoNext || SafeSensor || Camera).

////////////False and True
fluent True = <Alphabet,Alphabet\Alphabet> initially 1
fluent False = <Alphabet\Alphabet,Alphabet> initially 0

////////////LIVENESS
fluent F_has_next = <has_next,Alphabet\{has_next}> initially 0
assert A_HasNext = (F_has_next)

////////////SAFETY
fluent F_NoNext = <no_next,landed> initially 0
fluent F_Arrived = <arrived,has_next> initially 0
fluent F_Gone = <go_next,{has_next}> initially 0

fluent F_YesSafeNext = <yes_safe_next,has_next>
fluent F_NoSafeNext = <no_safe_next,has_next>
assert GoFindSafe = F_YesSafeNext
assert NotGoFindSafe = F_NoSafeNext
assert CheckFindSafe = ((GoFindSafe && go_next) || NotGoFindSafe)
assert GoneFindSafe = (NotGoFindSafe -> !F_Gone)  
assert Achieve_FindSafe = (!{IteratorC} W CheckFindSafe)
assert Maintain_FindSafe = (GoneFindSafe W {IteratorC})
ltl_property L_FindSafeMode = []((yes_next) -> (Achieve_FindSafe && Maintain_FindSafe))

assert Achieve_Camera_BeforeIterator = (!{IteratorC} W {yes_person,no_person})
ltl_property L_SearchOnceArrivedAtCell = [](arrived -> Achieve_Camera_BeforeIterator)

fluent F_Found = <yes_person,landed>

ltl_property L_LandWhenFound = []((land -> (F_Found || F_NoNext)) && ((yes_person || no_next) -> (!{IteratorC} W land)))
ltl_property L_AbortNever = [](abort_go -> False)

controllerSpec Controller_Spec = {
        safety = {L_LandWhenFound,L_AbortNever,
					L_FindSafeMode,L_SearchOnceArrivedAtCell}
        assumption = {}
        liveness = {A_HasNext}
        controllable = {Controlables}
}

controller ||Controller = Environment~{Controller_Spec}.
minimal ||TestController = (Environment || Controller).
