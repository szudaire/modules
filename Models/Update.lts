set PlaneC = {takeoff, go_next, rtl ,battery_charged,do_spin,sort_locations}
set PlaneU = {initial_config, arrived, takeoff_ended, rtl_ended, spin_finished}
set PlaneA = {PlaneC,PlaneU}

set IteratorC = {has_next,reset_iterator,remove_next}
set IteratorU = {yes_next, no_next}
set IteratorA = {IteratorC,IteratorU}

set SensorsC = {has_A_next,has_B_next,has_Cover_next}
set SensorsU = {yes_A_next,no_A_next,yes_B_next,no_B_next,yes_Cover_next,no_Cover_next}
set Sensors = {SensorsC,SensorsU}

Plane = (initial_config -> ConfigDone),
ConfigDone = (	takeoff -> sort_locations -> takeoff_ended -> TakeoffEnded),
TakeoffEnded = (	{IteratorA,Sensors} -> TakeoffEnded | do_spin -> spin_finished -> TakeoffEnded | 
					go_next -> sort_locations -> GoTo |
					rtl -> rtl_ended -> Landed),
Landed = (battery_charged -> Plane),
GoTo = (arrived -> TakeoffEnded).

Spin = (do_spin -> spin_finished -> Spin).

Iterator = (has_next -> (yes_next -> YesNext | no_next -> NoNext) | reset_iterator -> Iterator),
YesNext = (remove_next -> Iterator),
NoNext = (reset_iterator -> Iterator).

GoNext = (yes_next -> (go_next -> ({remove_next} -> GoNext) 
		| {remove_next} -> GoNext)).

NextQuestions = ({remove_next} -> NextQuestions | yes_next -> NQYes),
NQYes = ({remove_next} -> NextQuestions | {SensorsC} -> NQYes).

ABSensor = (has_A_next -> (yes_A_next -> ABSensor | no_A_next -> ABSensor) | has_B_next -> (yes_B_next -> ABSensor | no_B_next -> ABSensor)).
CoverSensor = (has_Cover_next -> (yes_Cover_next -> CoverSensor | no_Cover_next -> CoverSensor)).

set Alphabet = {IteratorA,PlaneA,Sensors}
set Controlables = {IteratorC,PlaneC,SensorsC}
||Environment = (Plane || Iterator || NextQuestions || GoNext || ABSensor || CoverSensor || Spin).

set SensorABSpin_Alph = {has_A_next,yes_A_next,no_A_next,has_B_next,yes_B_next,no_B_next,do_spin,spin_finished}
set SensorCover_Alph = {has_Cover_next,yes_Cover_next,no_Cover_next}
NeverABSpin = ({Alphabet\SensorABSpin_Alph} -> NeverABSpin)+{Alphabet}.
NeverCover = ({Alphabet\SensorCover_Alph} -> NeverCover)+{Alphabet}.

||OldEnvironment = (Environment || NeverCover).
||NewEnvironment = (Environment || NeverABSpin).

//Mapping Environment
BEFORE_RECONF = (reconfigure -> AFTER_RECONF | {Alphabet\SensorCover_Alph} -> BEFORE_RECONF),
AFTER_RECONF = ({Alphabet\SensorABSpin_Alph} -> AFTER_RECONF).
||MappingEnvironment = (Environment || BEFORE_RECONF).

////////////False and True
fluent True = <Alphabet,Alphabet\Alphabet> initially 1
fluent False = <Alphabet\Alphabet,Alphabet> initially 0

//----------------- OLD CONTROLLER SPEC -------------------

////////////LIVENESS
fluent F_has_next = <has_next,Alphabet\{has_next}> initially 0
assert A_HasNext = (F_has_next)

////////////SAFETY
fluent F_NoNext = <no_next,has_next> initially 0
fluent F_PatrolA = <yes_B_next,yes_A_next> initially 1
fluent F_PatrolB = <yes_A_next,yes_B_next> initially 0
fluent F_Arrived = <arrived,has_next> initially 1
fluent F_Gone = <go_next,{has_next}> initially 0

assert A_EndPatrolA = (F_Arrived && F_YesANext)
assert A_EndPatrolB = (F_Arrived && F_YesBNext)

fluent F_YesANext = <yes_A_next,has_next> initially 1
fluent F_NoANext = <no_A_next,has_next>
assert GoPatrolA = F_YesANext
assert NotGoPatrolA = F_NoANext
assert CheckPatrolA = ((GoPatrolA && go_next) || NotGoPatrolA)
assert GonePatrolA = (NotGoPatrolA -> !F_Gone)  
assert Achieve_PatrolA = (!{IteratorC,do_spin} W CheckPatrolA)
assert Maintain_PatrolA = (GonePatrolA W {IteratorC})
ltl_property L_PatrolAMode = []((yes_next && F_PatrolA) -> (Achieve_PatrolA && Maintain_PatrolA))

assert Achieve_Spin = (!{SensorsC,IteratorC} W {spin_finished})
ltl_property L_Spin = [](arrived -> Achieve_Spin)

fluent F_YesBNext = <yes_B_next,has_next>
fluent F_NoBNext = <no_B_next,has_next>
assert GoPatrolB = F_YesBNext
assert NotGoPatrolB = F_NoBNext
assert CheckPatrolB = ((GoPatrolB && go_next) || NotGoPatrolB)
assert GonePatrolB = (NotGoPatrolB -> !F_Gone)
assert Achieve_PatrolB = (!{IteratorC,do_spin} W CheckPatrolB)
assert Maintain_PatrolB = (GonePatrolB W {IteratorC})
ltl_property L_PatrolBMode = []((yes_next && F_PatrolB) -> (Achieve_PatrolB && Maintain_PatrolB))

ltl_property L_RTLNever = [](rtl -> False)
ltl_property L_ResetOnNoNext = [](reset_iterator -> F_NoNext)

controllerSpec OldSpecSafe = {
        safety = {L_RTLNever,L_Spin,L_ResetOnNoNext,
					L_PatrolAMode,L_PatrolBMode}
//        assumption = {}
//        liveness = {}
        controllable = {Controlables}
}

controllerSpec OldSpecSafeLive = {
        safety = {L_RTLNever,L_Spin,L_ResetOnNoNext,
					L_PatrolAMode,L_PatrolBMode}
        assumption = {}
        liveness = {A_HasNext}
        controllable = {Controlables}
}

controller ||OldControllerSafe = OldEnvironment~{OldSpecSafe}.
controller ||OldControllerSafeLive = OldEnvironment~{OldSpecSafeLive}.
minimal ||EnvironmentAndControllerSafe = (OldEnvironment || OldControllerSafe).
minimal ||EnvironmentAndControllerSafeLive = (OldEnvironment || OldControllerSafeLive).

//----------------- NEW CONTROLLER SPEC -------------------

fluent F_YesCoverNext = <yes_Cover_next,has_next> initially 1
fluent F_NoCoverNext = <no_Cover_next,has_next>
assert GoPatrolCover = F_YesCoverNext
assert NotGoPatrolCover = F_NoCoverNext
assert CheckPatrolCover = ((GoPatrolCover && go_next) || NotGoPatrolCover)
assert GonePatrolCover = (NotGoPatrolCover -> !F_Gone)  
assert Achieve_PatrolCover = (!{IteratorC,do_spin} W CheckPatrolCover)
assert Maintain_PatrolCover = (GonePatrolCover W {IteratorC})
ltl_property L_PatrolCoverMode = []((yes_next) -> (Achieve_PatrolCover && Maintain_PatrolCover))

ltl_property L_RTLOnNoNext = []((rtl -> F_NoNext) && (no_next -> (!{IteratorC,SensorsC} W rtl)))

controllerSpec NewSpecSafe = {
        safety = {L_PatrolCoverMode,L_ResetOnNoNext,L_RTLOnNoNext}
//        assumption = {}
//        liveness = {}
        controllable = {Controlables}
}

controllerSpec NewSpecSafeLive = {
        safety = {L_PatrolCoverMode,L_ResetOnNoNext,L_RTLOnNoNext}
        assumption = {}
        liveness = {A_HasNext}
        controllable = {Controlables}
}

controller ||NewControllerSafe = NewEnvironment~{NewSpecSafe}.
controller ||NewControllerSafeLive = NewEnvironment~{NewSpecSafeLive}.
minimal ||EnvironmentAndNewControllerSafe = (NewEnvironment || NewControllerSafe).
minimal ||EnvironmentAndNewControllerSafeLive = (NewEnvironment || NewControllerSafeLive).


//----------------- UPDATE CONTROLLER SPEC -------------------
//Updating controller fluents
fluent InTransition = <stopOldSpec, startNewSpec> 
fluent StopOldSpec = <stopOldSpec, beginUpdate>
fluent StartNewSpec= <startNewSpec, beginUpdate>
fluent Reconfigure = <reconfigure, beginUpdate>

ltl_property NoTransition = [](!StopOldSpec || StartNewSpec)

fluent F_IteratorEmpty = <reset_iteratoor,has_next>
fluent F_SensingA = <has_A_next,{yes_A_next,no_A_next}>
fluent F_SensingB = <has_B_next,{yes_B_next,no_B_next}>
fluent F_Spinning = <do_spin,spin_finished>
ltl_property NotProcessing = []((reconfigure -> StartNewSpec) &&
								((StopOldSpec && !StartNewSpec) -> (!{PlanceC,SensorsC})) &&
								(startNewSpec -> F_IteratorEmpty) &&
								(reconfigure -> (!F_SensingA && !F_SensingB && !F_Spinning)))

updatingController UpdCont = {
    oldController = EnvironmentAndControllerSafeLive,
    mapping = MappingEnvironment,
    safetyOldGoal = OldSpecSafe,
    safetyNewGoal = NewSpecSafe,
	livenessNewGoal = {A_HasNext}
	transition = NotProcessing,
	nonblocking
}

||UPDATE_CONTROLLER = (UpdCont).

