set PlaneC = {takeoff, go_next, land, abort_go,battery_charged,sort_locations}
set PlaneU = {initial_config, arrived, takeoff_ended, landed}
set PlaneA = {PlaneC,PlaneU}

set IteratorC = {has_next,reset_iterator,remove_next}
set IteratorU = {yes_next, no_next}
set IteratorA = {IteratorC,IteratorU}

set SensorsC = {has_A_next,has_B_next}
set SensorsU = {yes_A_next,no_A_next,yes_B_next,no_B_next}
set Sensors = {SensorsC,SensorsU}

Plane = (initial_config -> ConfigDone),
ConfigDone = (	takeoff -> sort_locations -> takeoff_ended -> TakeoffEnded),
TakeoffEnded = (	{IteratorC,SensorsC} -> TakeoffEnded | 
					go_next -> sort_locations -> GoTo |
					land -> landed -> Landed),
Landed = (battery_charged -> Plane),
GoTo = (arrived -> TakeoffEnded | 
		abort_go -> TakeoffEnded).

Iterator = (has_next -> (yes_next -> YesNext | no_next -> NoNext)),
YesNext = (remove_next -> Iterator),
NoNext = (reset_iterator -> Iterator).

GoNext = (yes_next -> (go_next -> ({remove_next} -> GoNext) 
		| {remove_next} -> GoNext)).

NextQuestions = ({remove_next} -> NextQuestions | yes_next -> NQYes),
NQYes = ({remove_next} -> NextQuestions | {SensorsC} -> NQYes).

ASensor = (has_A_next -> (yes_A_next -> ASensor | no_A_next -> ASensor)).
BSensor = (has_B_next -> (yes_B_next -> BSensor | no_B_next -> BSensor)).

set Alphabet = {IteratorA,PlaneA,Sensors}
set Controlables = {IteratorC,PlaneC,SensorsC}
||Environment = (Plane || Iterator || NextQuestions || GoNext || ASensor || BSensor).

set SensorA_Alph = {has_A_next,yes_A_next,no_A_next}
set SensorB_Alph = {has_B_next,yes_B_next,no_B_next}
NeverSensorA = ({Alphabet\{SensorA_Alph}}  -> NeverSensorA)+{Alphabet}.
NeverSensorB = ({Alphabet\{SensorB_Alph}}  -> NeverSensorB)+{Alphabet}.

||OldEnvironment = (Environment || NeverSensorB).
||NewEnvironment = (Environment || NeverSensorA).

//Mapping Environment
BEFORE_RECONF = (reconfigure -> AFTER_RECONF | {Alphabet\{SensorB_Alph}} -> BEFORE_RECONF),
AFTER_RECONF = ({Alphabet\{SensorA_Alph}} -> AFTER_RECONF).
||MappingEnvironment = (Environment || BEFORE_RECONF).

////////////False and True
fluent True = <Alphabet,Alphabet\Alphabet> initially 1
fluent False = <Alphabet\Alphabet,Alphabet> initially 0

//----------------- OLD CONTROLLER SPEC -------------------
////////////LIVENESS
fluent F_has_next = <has_next,Alphabet\{has_next}> initially 0
assert A_HasNext = (F_has_next)

////////////SAFETY
fluent F_NoNext = <no_next,landed> initially 0
fluent F_Arrived = <arrived,has_next> initially 0
fluent F_Gone = <go_next,{has_next}> initially 0

fluent F_YesANext = <yes_A_next,has_next>
fluent F_NoANext = <no_A_next,has_next>
assert GoCoverA = F_YesANext
assert NotGoCoverA = F_NoANext
assert CheckCoverA = ((GoCoverA && go_next) || NotGoCoverA)
assert GoneCoverA = (NotGoCoverA -> !F_Gone)  
assert Achieve_CoverA = (!{IteratorC} W CheckCoverA)
assert Maintain_CoverA = (GoneCoverA W {IteratorC})
ltl_property L_CoverAMode = []((yes_next) -> (Achieve_CoverA && Maintain_CoverA))

ltl_property L_LandOnNoNext = []((land -> F_NoNext) && (no_next -> (!{IteratorC} W land)))
ltl_property L_AbortNever = [](abort_go -> False)

controllerSpec OldSpecSafe = {
        safety = {L_LandOnNoNext,L_AbortNever,
					L_CoverAMode}
//        assumption = {}
//        liveness = {}
        controllable = {Controlables}
}

controllerSpec OldSpecSafeLive = {
        safety = {L_LandOnNoNext,L_AbortNever,
					L_CoverAMode}
        assumption = {}
        liveness = {A_HasNext}
        controllable = {Controlables}
}

controller ||OldControllerSafe = OldEnvironment~{OldSpecSafe}.
controller ||OldControllerSafeLive = OldEnvironment~{OldSpecSafeLive}.
minimal ||EnvironmentAndControllerSafe = (OldEnvironment || OldControllerSafe).
minimal ||EnvironmentAndControllerSafeLive = (OldEnvironment || OldControllerSafeLive).

//----------------- NEW CONTROLLER SPEC -------------------

fluent F_YesBNext = <yes_B_next,has_next>
fluent F_NoBNext = <no_B_next,has_next>
assert GoCoverB = F_YesBNext
assert NotGoCoverB = F_NoBNext
assert CheckCoverB = ((GoCoverB && go_next) || NotGoCoverB)
assert GoneCoverB = (NotGoCoverB -> !F_Gone)  
assert Achieve_CoverB = (!{IteratorC} W CheckCoverB)
assert Maintain_CoverB = (GoneCoverB W {IteratorC})
ltl_property L_CoverBMode = []((yes_next) -> (Achieve_CoverB && Maintain_CoverB))

controllerSpec NewSpecSafe = {
        safety = {L_LandOnNoNext,L_AbortNever,
					L_CoverBMode}
//        assumption = {}
//        liveness = {}
        controllable = {Controlables}
}

controllerSpec NewSpecSafeLive = {
        safety = {L_LandOnNoNext,L_AbortNever,
					L_CoverBMode}
        assumption = {}
        liveness = {A_HasNext}
        controllable = {Controlables}
}

controller ||NewControllerSafe = NewEnvironment~{NewSpecSafe}.
controller ||NewControllerSafeLive = NewEnvironment~{NewSpecSafeLive}.
minimal ||EnvironmentAndNewControllerSafe = (NewEnvironment || NewControllerSafe).
minimal ||EnvironmentAndNewControllerSafeLive = (NewEnvironment || NewControllerSafeLive).


//----------------- UPDATE CONTROLLER SPEC -------------------
//Updating controller fluents
fluent InTransition = <stopOldSpec, startNewSpec> 
fluent StopOldSpec = <stopOldSpec, beginUpdate>
fluent StartNewSpec= <startNewSpec, beginUpdate>
fluent Reconfigure = <reconfigure, beginUpdate>

ltl_property NoTransition = [](!StopOldSpec || StartNewSpec)
fluent F_SensingA = <has_A_next,{yes_A_next,no_A_next}>
ltl_property NotProcessing = []((!StopOldSpec || StartNewSpec) && 
								(reconfigure -> !F_SensingA))

updatingController UpdCont = {
    oldController = EnvironmentAndControllerSafeLive,
    mapping = MappingEnvironment,
    safetyOldGoal = OldSpecSafe,
    safetyNewGoal = NewSpecSafe,
	livenessNewGoal = {A_HasNext}
	transition = NotProcessing,
	nonblocking
}

||UPDATE_CONTROLLER = (UpdCont).

