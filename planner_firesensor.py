from MAVProxy.modules.planner_module import Module
from MAVProxy.modules.mavproxy_map import mp_tile
from MAVProxy.modules import planner_util_message as mesg
from MAVProxy.modules.lib.ANUGA import lat_long_UTM_conversion as llutm
from multiprocessing import Value,Array,Queue
import cv2 as cv
import numpy as np
import time
from os.path import expanduser
from ctypes import c_ubyte

class FireSensor(Module):
    def init(self):
        self._set_suscription_packets(["GLOBAL_POSITION_INT"])
        
        self._nx = 801
        self._ny = 801
        self._process_type = 1
        self._fire = np.zeros((self._nx,self._ny),np.uint8)
        self._border = np.zeros((self._nx,self._ny),np.uint8)
        self._x_orig = 268962.0
        self._y_orig = 5401414.0
        self._cellsize = 60.0

        self._pos_queue = Queue()
        self._border_array = Array(c_ubyte,[0]*self._nx*self._ny)
        self._fire_array = Array(c_ubyte,[0]*self._nx*self._ny)

        self._loc_curr = -1
        self._firesim = None

        self.controllables = ["has_border_next"]
    
    def _started(self):
        pass
    
    def _capture(self):
        next = self._sharedmem.get_next_location()
        image = self._image_list[next]

        cv.imshow('capture',image)

        ret = self._process_image(image)

        if (ret):
            self._add_to_event_queue('yes_border_next')
        else:
            self._add_to_event_queue('no_border_next')
        #cv.imwrite(self.path + str(current) + ".png",image)
        return
        
    def _download_image(self,lat,lon,ground_width):
        image = self._tile.area_to_image(lat, lon, self._width, self._height, ground_width)
        while (self._tile.tiles_pending() > 0):
            time.sleep(0.01)
        image = self._tile.area_to_image(lat, lon, self._width, self._height, ground_width)

        image = cv.cvtColor(image,cv.COLOR_RGB2BGR)
        
        #self._image_list.append(image)
        #self.descarga_num += 1
        #cv.imwrite(self.path + 'data_set/' + str(self.descarga_num) + ".png",image)
        
        #cv.imshow('capture',image)
        return image

    def add_locations(self,pos_locations):
        pass
    
    def _process_image(self,image):
        hsv = cv.cvtColor(image, cv.COLOR_BGR2HSV)
        # define range of blue color in HSV
        lower_blue = np.array([90,100,30])
        upper_blue = np.array([110,255,255])
        mask = cv.inRange(hsv, lower_blue, upper_blue)
        
        blur = cv.blur(mask,(5,5))
        #cv.imshow('process',mask)

        next = self._sharedmem.get_next_location()
        cant = len(blur[blur<50])
        if (cant > 30000):
            ground_width = self._field_of_view*self._sharedmem.get_flight_height()
            area = self._sharedmem.get_discretizer().get_capture_polygon(next,ground_width)
            elements = self._sharedmem.get_discretizer().get_locations_from_area(area,0)
            for elem in elements:
                if (elem not in self._get_list()):
                    self._add_to_list(elem)
            loc_next_pos = self._sharedmem.get_discretizer().get_position(next)
            #self._add_to_message_queue(mesg.FOUND,str(loc_next_pos[1]) + ' ' + str(loc_next_pos[0]))
            #self._add_to_event_queue('yes_border_next')
            return True
        else:
            if (next in self._get_list()):
                self._remove_from_list(next)
            #self._add_to_event_queue('no_border_next')
            return False

    def has_border_next(self):
        next_pos = self._sharedmem.get_next_location()
        if (self._border_array[next_pos] == 1): # and self.is_safe_next()):
            return "yes_border_next"
        else:
            return "no_border_next"

    def is_safe_next(self):
        t1 = time.time()
        
        loc_next = self._sharedmem.get_next_location()
        loc_next_pos = self._sharedmem.get_discretizer().get_position(loc_next)
        loc_current = self._sharedmem.get_current_location()
        if(loc_current == -1):
            loc_current_pos = self._sharedmem.get_position()
        else:
            loc_current_pos = self._sharedmem.get_discretizer().get_position(loc_current)

        position = self._sharedmem.get_position()
        if (self._compare_distance(position,loc_current_pos,self._aux_radius*0.75)):
            loc_current_pos = position
        
        self._sharedmem.set_trajectory_parameters(loc_current_pos,auxwp=True,overwp=True,check_ang=True,pond=False)
        wp_list,distance,dir = self._sharedmem.calculate_trajectory(loc_next_pos,update_dir=False)

        elements = self._sharedmem.get_discretizer()._select_elements_from_trajectory(wp_list)
        self._add_to_print_queue("Evaluation time (is.safe.next): " + str(time.time() - t1))

        issafe = True
        for i in range(len(elements)):
            if self._nofly_list[elements[i]] == 1:
                issafe = False
                break
        self._add_to_print_queue("Is safe: " + str(issafe))
        
        return issafe

    def _compare_distance(self,pos1,pos2,distance):
        dist_latlon = self._sharedmem.meters2latlon(distance)[1]

        vec = pos1 - pos2
        scale = self._sharedmem.get_scale()
        vec = vec*scale

        if (np.linalg.norm(vec) > dist_latlon):
            return True
        else:
            return False

    def setup_firesim(self,module):
        self._firesim = module
        self._safe_dist = self._firesim.safe_dist
        self._fire_cellsize = self._firesim.cellsize

    def analyze_fire(self):
        self._add_to_command_queue("analyze_fire")

    def _analyze_fire(self):
        [posx,posy] = self._pos_queue.get()
        
        x = int(np.floor((posx - self._fire_cellsize/2 - self._x_orig) / self._fire_cellsize))
        y = self._ny - int(np.floor((posy - self._fire_cellsize/2 - self._y_orig) / self._fire_cellsize))

        delta = 2*int(np.ceil(self._safe_dist/self._cellsize))
        min_x = max(x-delta,0)
        min_y = max(y-delta,0)
        max_x = min(x+delta+2,self._nx)
        max_y = min(y+delta+2,self._ny)

        found = False
        for i in range(min_x,max_x):
            for j in range(min_y,max_y):
                aux_posx = i*self._fire_cellsize + self._x_orig + self._fire_cellsize/2
                aux_posy = (self._ny - j)*self._fire_cellsize + self._y_orig + self._fire_cellsize/2
                if ((aux_posx - posx)**2 + (aux_posy - posy)**2) < self._safe_dist**2:
                    if (self._firesim.fire[j,i] == 11):
                        index = j*self._ny+i
                        if (self._fire_array[index] == 0):
                            self._fire_array[j*self._ny+i] = 1
                            (lat,lon) = llutm.UTMtoLL(aux_posy,aux_posx,self._ZoneNumber,isSouthernHemisphere=True)
                            self._add_to_message_queue(mesg.NOFLY,str(lat) + " " + str(lon))
                            found = True
        if found:
            self._compute_border()
        return found

    def _compute_border(self):
        fire = np.array(self._fire_array[:],np.uint8).reshape((self._nx,self._ny))
        kernel,param = self._get_kernel(self._safe_dist/self._cellsize)
        blur_grid = cv.filter2D(fire,-1,kernel)
        thresh_low = param*param
        
        unsafe = np.zeros((self._nx,self._ny),np.uint8)
        unsafe[(blur_grid < thresh_low)] = 0
        unsafe[(blur_grid>=thresh_low)] = 1

        contours, hierarchy = cv.findContours(unsafe, cv.RETR_TREE, cv.CHAIN_APPROX_NONE)
        border = np.zeros((self._nx,self._ny),np.uint8)
        i = 0
        for contour in contours:
            if hierarchy[0,i,3] != -1:
                continue
            for elem in contour:
                elem = elem[0]
                border[elem[1],elem[0]] = 1
                
                aux_posx = elem[0]*self._fire_cellsize + self._x_orig + self._fire_cellsize/2
                aux_posy = (self._ny - elem[1])*self._fire_cellsize + self._y_orig + self._fire_cellsize/2
                (lat,lon) = llutm.UTMtoLL(aux_posy,aux_posx,self._ZoneNumber,isSouthernHemisphere=True)
                self._add_to_message_queue(mesg.BORDER,str(lat) + " " + str(lon))
                
            i += 1
        
        self._add_to_message_queue(mesg.BORDER_DRAW,str(lat) + " " + str(lon))
        self._border_array[:] = border.flatten()
        return
        
    def _get_kernel(self,safe_dist):
        param = (int) (np.ceil(safe_dist)*2 + 1)
        kernel = np.zeros((param,param),np.float)
        
        center = (param-1)/2
        for i in range(param):
            for j in range(param):
                dist = np.linalg.norm([i-center,j-center])
                if dist <= safe_dist:
                    kernel[i,j] = param*param
                elif dist > safe_dist and dist <= safe_dist + 1:
                    kernel[i,j] = 1
        
        return kernel,param

    def _get_index(self,loc):
        coord = self._sharedmem.get_discretizer().get_position(loc)
        (ZoneNumber, UTMEasting, UTMNorthing) = llutm.LLtoUTM(coord[1],coord[0])

        i = int(np.floor((UTMEasting - self._x_orig) / self._fire_cellsize))
        j = self._ny - int(np.floor((UTMNorthing - self._y_orig) / self._fire_cellsize))
        print "I: " + str(i) + " J: " + str(j)
        return j*self._nx + i

    def set_fire(self,loc_list):
        for loc in loc_list:
            self._fire_array[self._get_index(loc)] = 1
            self._compute_border()
    
    def mavlink_packet(self,m):
        mtype = m.get_type()
        
        if mtype == "GLOBAL_POSITION_INT" and self._started_flag:
            lat = m.lat*0.0000001
            lon = m.lon*0.0000001
	    (ZoneNumber, UTMEasting, UTMNorthing) = llutm.LLtoUTM(lat,lon)

            i = np.floor((UTMEasting - self._x_orig) / self._cellsize)
            j = np.floor((UTMNorthing - self._y_orig) / self._cellsize)
	    x_utm = i * self._cellsize + self._x_orig + self._cellsize/2
	    y_utm = j * self._cellsize + self._y_orig + self._cellsize/2

	    (lat,lon) = llutm.UTMtoLL(y_utm,x_utm,ZoneNumber,isSouthernHemisphere=True)
            self._ZoneNumber = ZoneNumber

	    position = np.array([lon,lat])
	    index = self._sharedmem.get_discretizer().get_closest_element(position)
	    
            if np.linalg.norm(position - self._sharedmem.get_discretizer().get_position(index)) < 0.000001:
                if (index != self._loc_curr):
                    self._loc_curr = index
                    self._pos_queue.put([x_utm,y_utm])
                    
                    # CHANGED REGION
                    self.analyze_fire()
                    self._add_to_print_queue("Entered region: " + str(index))
    


