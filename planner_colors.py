class pcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

    @staticmethod
    def color(text,color):
        if (color == 'yellow'):
            return pcolors.WARNING + text + pcolors.ENDC
        elif (color == 'pink'):
            return pcolors.HEADER + text + pcolors.ENDC
        elif (color == 'blue'):
            return pcolors.OKBLUE + text + pcolors.ENDC
        elif (color == 'green'):
            return pcolors.OKGREEN + text + pcolors.ENDC
        elif (color == 'red'):
            return pcolors.FAIL + text + pcolors.ENDC
        else:
            return text
