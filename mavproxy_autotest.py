#!/usr/bin/env python
'''
Example Module
Peter barker, September 2016

This module simply serves as a starting point for your own MAVProxy module.

1. copy this module sidewise (e.g. "cp mavproxy_example.py mavproxy_coolfeature.py"
2. replace all instances of "example" with whatever your module should be called
(e.g. "coolfeature")

3. trim (or comment) out any functionality you do not need
'''

import os
import os.path
import sys
from pymavlink import mavutil
import errno
import time

from MAVProxy.modules.lib import mp_module
from MAVProxy.modules.lib import mp_util
from MAVProxy.modules.lib import mp_settings


class autotest(mp_module.MPModule):
	def __init__(self, mpstate):
		"""Initialise module"""
		super(autotest, self).__init__(mpstate, "autotest", "")
		
		# Battery levels
		self.battery_level = -1
		self.voltage_level = -1
		self.current_battery = -1
		
		# Position
		self.lat = 0
		self.lon = 0
		self.alt = 0
		
		# Velocity
		self.vx = 0
		self.vy = 0
		self.vz = 0
		
		# Initial time
		self.init_time = time.time()
		self.config_time = 20
		self.config_done = False
		self.config_way = False
		self.config_auto = False
		self.config_armed = False
		self.prev_armed = time.time()
		self.armed_time = 2
		
		# Control time
		self.prev_control = time.time()
		self.control_time = 0.05 # 20 Hz
		self.count = 0
		
		# Report time
		self.report_time = 5
		self.prev_report = time.time()
		
		# Waypoint list
		self.wp_list = [0*10]*6
		self.wp_list[0] = [0,0,16,0.000000,0.000000,0.000000,0.000000,-35.363262,149.165237,584.299988,1]
		self.wp_list[1] = [0,3,22,15.000000,0.000000,0.000000,0.000000,-35.359833,149.164703,41.029999,1]
		self.wp_list[2] = [0,3,16,0.000000,0.000000,0.000000,0.000000,-35.359585,149.161392,100.000000,1]
		self.wp_list[3] = [0,3,16,0.000000,0.000000,0.000000,0.000000,-35.366463,149.162231,100.000000,1]
		self.wp_list[4] = [0,3,16,0.000000,0.000000,0.000000,0.000000,-35.366131,149.164581,100.000000,1]
		self.wp_list[5] = [0,3,21,0.000000,0.000000,0.000000,0.000000,-35.359276,149.163757,0.000000,1]
		
		# Waypoint list extra data
		self.header = "QGC WPL 110"
		self.filename = "testway.txt"
		self.wp_ind = 0
		self.wait_ack = False
		self.wait_count = 0
		
		'''
			w = fn(self.target_system, self.target_component,
				   int(a[0]),    # seq
				   int(a[2]),    # frame
				   int(a[3]),    # command
				   int(a[1]),    # current
				   int(a[11]),   # autocontinue
				   float(a[4]),  # param1,
				   float(a[5]),  # param2,
				   float(a[6]),  # param3
				   float(a[7]),  # param4
				   float(a[8]),  # x (latitude)
				   float(a[9]),  # y (longitude)
				   float(a[10])  # z (altitude)
				   )
		'''
		
	
	def battery_update(self, SYS_STATUS):
		'''update battery level'''
		# main flight battery
		self.battery_level = SYS_STATUS.battery_remaining
		self.voltage_level = SYS_STATUS.voltage_battery
		self.current_battery = SYS_STATUS.current_battery
		
	def position_update(self, GLOBAL_POSITION_INT):
		'''update gps position'''
		self.lat = GLOBAL_POSITION_INT.lat
		self.lon = GLOBAL_POSITION_INT.lon
		self.alt = GLOBAL_POSITION_INT.relative_alt*0.001 # mm2metre conversion
		
		'''update ground velocity'''
		self.vx = GLOBAL_POSITION_INT.vx*0.01 # cm2metre
		self.vy = GLOBAL_POSITION_INT.vy*0.01 # cm2metre
		self.vz = GLOBAL_POSITION_INT.vz*0.01 # cm2metre
		
	
	def idle_task(self):
		'''called on idle'''
		
		time_aux = time.time()
		
		delta_time = time_aux - self.prev_control
		self.say("Delta tiempo: %s" % delta_time)
		self.prev_control = time_aux
		
		
	
	def update_waypoints(self):
		self.wait_ack = True
		if (self.wp_ind < len(self.wp_list) - 2):
			self.wp_ind = self.wp_ind + 1
			self.load_waypoints([self.wp_ind,self.wp_ind+1])
	
	def load_waypoints(self, ind_list):
		file_object = open(self.filename,'w')
		file_object.write(self.header + "\n")
		file_object.write(self.create_string(0))
		for ind in ind_list:
			file_object.write(self.create_string(ind))
		file_object.close()
		
		self.mpstate.functions.process_stdin("wp load " + self.filename, immediate=True)
	
	def set_waypoint(self):
		self.mpstate.functions.process_stdin("wp set 1", immediate=True)
	
	def create_string(self,ind):
		wp_params = self.wp_list[ind]
		if (ind > 0):
			aux_string = str(ind - self.wp_ind + 1)
		else:
			aux_string = str(0)
		for param in wp_params:
			aux_string = aux_string + "\t" + str(param)
		aux_string = aux_string + "\n"
		return aux_string
	
	def mavlink_packet(self, m):
		'''handle mavlink packets'''
		mtype = m.get_type()
		if mtype == "SYS_STATUS":
			self.battery_update(m)
		elif mtype == "GLOBAL_POSITION_INT":
			self.position_update(m)
		elif mtype == "MISSION_ITEM_REACHED":
			self.update_waypoints()
		elif mtype == "MISSION_ACK":
			if (self.wait_ack == True):
				self.wait_count = self.wait_count + 1
				# Recien al segundo MISSION_ACK es que recibi� los waypoints
				if (self.wait_count >= 2):
					self.wait_count = 0
					self.wait_ack = False
					self.set_waypoint()
		elif mtype == "COMMAND_ACK":
			if (m.command == 400):
				if (m.result == 0):
					self.config_armed = True
			

def init(mpstate):
	'''initialise module'''
	return autotest(mpstate)
