from MAVProxy.modules.planner_module import Module
from MAVProxy.modules import planner_util_message as mesg
from multiprocessing import Array,Value
import cv2 as cv
import time
import numpy as np
from os.path import expanduser
import numpy as np

class CowSensor(Module):
    def init(self):
        home = expanduser("~")
        self.path = "/home/saz/Documents/ardupilot/ArduCopter/fotos/"

        self._process_type = 0

	self.controllables = ['count']
	self._curr_image = 20
	
    def count(self):
        time.sleep(2)
        
        image = cv.imread(self.path + "DJI_00" + str(self._curr_image) + "_red.JPG")
        image = cv.resize(image,(640,480))
        cv.imshow("Capture",image)
        cv.moveWindow('Capture',0,0)
        cv.waitKey(2000)

        image = cv.imread(self.path + "test_" + str(self._curr_image) + ".JPG")
        image = cv.resize(image,(640,480))
        cv.imshow("Capture",image)
        cv.moveWindow('Capture',0,0)
        cv.waitKey(2000)
        
        image = cv.imread(self.path + "test_" + str(self._curr_image) + "_count.jpg")
        image = cv.resize(image,(640,480))
        cv.imshow("Capture",image)
        cv.moveWindow('Capture',0,0)
        cv.waitKey(3000)
        
        self._curr_image += 1
        return 'counted'
