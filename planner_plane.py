from MAVProxy.modules.planner_module import Module
from ctypes import c_bool
from multiprocessing import Value
import numpy as np
from MAVProxy.modules import planner_util_message as mesg
import math
import time

class Plane(Module):
    def init(self):
        self._set_suscription_packets(["ATTITUDE","MISSION_ACK","GLOBAL_POSITION_INT","MISSION_ITEM_REACHED"])
        
        # Initial time
        self._init_time = time.time()
        self._config_time = 5
        self._config_init = False
        
        # Waypoint data
        self._MIN_PITCH = 15.0
        self._CMD_TAKEOFF = 22
        self._CMD_LAND = 21
        self._CMD_WP = 16
        
        # Waypoint list extra data
        self._header = "QGC WPL 110"
        self._filename = "wp_temp.txt"
        
        # Waypoint list
        self._wp_default = np.array([0.000000,0.000000,0.000000,0.000000,-35.363262,149.165237,100.0])
        
        self._wp_count = Value('i',0)
        self._wp_len = Value('i',0)
        self._x_coord = Value('i',-1)
        self._y_coord = Value('i',-1)
        self._wait_ack = Value(c_bool,False)
        self._first_wp = Value(c_bool,True)
        self._takeoff_ended = False
        
        self._in_flight = Value(c_bool,False)
        self._takeoff_flag = Value(c_bool,False)

        #Time the flight
        self._start_time = Value('d',0.0)
        self._started_time = False

        #Automatic takeoff and landing
        self._automatic = True

        #Acceptance radius
        self._loc_radius = 20.0 #Radius for locations to visit
        self._aux_radius = 40.0 #Radius for intermediate waypoints
        self._default_radius = 40.0 #Default radius
        self._straight_dist = 10000.0 #350.0 #Straight wp distance

        self._log_in_flight = False
        self._using_iterator = Value(c_bool,False)
        self._count_ack = 0

        self._regions = []
        
	self.controllables = ['takeoff', 'go_next', 'land', 'abort_go','go','rtl','set_high_height','set_low_height']

    def add_region(self,region):
        self._regions.append(region)

    def set_high_height(self):
        self._sharedmem.set_flight_height(self._high_height)
    
    def set_low_height(self):
        self._sharedmem.set_flight_height(self._low_height)
    
    def init_config(self):
        time_aux = time.time()
        
        # Initial conditions
        if (time_aux > self._init_time + self._config_time):
            if (self._config_init == False):
                self._add_to_mavproxy_command_queue("set moddebug 3")
                self._add_to_mavproxy_command_queue("param set ARMING_CHECK 0")
                '''
                #Set initial turn radius
                if (self._sharedmem.get_turn_radius() == 0.0):
                    radius = 85.0
                    self._sharedmem.set_turn_radius(radius)
                    self._add_to_message_queue(mesg.PRINT,"DEFAULT: Radius set to " + str("%.2f" % radius) + " meters")'''
                
                self.set_home()
                self._config_init = True
                return True
        return False
    
    def _set_waypoint(self):
        self._add_to_mavproxy_command_queue("wp set 1")

        #Update waypoints on ground station
        self._add_to_message_queue(mesg.UPDATE_WP,"")
    
    def _wp_along_direction(self,position,distance,direction):
        return position + direction*np.flip(self._sharedmem.meters2latlon(distance),0)
        
    def _load_waypoints(self, command, list_wp, altitude,dir,radius):
        file_object = open(self._filename,'w')
        file_object.write(self._header + "\n")
        
        #Waypoint home
        file_object.write(self._create_wp_string(self._CMD_WP,0,self._wp_default,altitude,self._default_radius))
        
        #Waypoints in the middle
        i = 0
        for i in range(len(list_wp)-1):
            i += 1
            file_object.write(self._create_wp_string(command,i,list_wp[i-1],altitude,self._aux_radius))
        
        #Waypoint of interest (location)
        file_object.write(self._create_wp_string(command,len(list_wp),list_wp[-1],altitude,self._loc_radius))
        
        #Waypoint extra along direction
        for i in range(self._get_wp_len() - len(list_wp)):
            file_object.write(self._create_wp_string(self._CMD_WP,i+2,self._wp_along_direction(list_wp[-1],self._straight_dist,dir),altitude,self._default_radius))
        file_object.write(self._create_wp_string(self._CMD_WP,i+2,self._wp_along_direction(list_wp[-1],self._straight_dist,dir),altitude,self._default_radius))
            
        file_object.close()
        
        self._set_wait_ack(True)
        self._set_wp_len(len(list_wp))
        self._set_wp_count(0)
        self._add_to_mavproxy_command_queue("wp load " + self._filename)
    
    def _create_wp_string(self,command,ind,pos,alt,radius):
        #Default intermediate waypoint radius is radius
        
        aux_string = str(ind) + "\t" + str(0)
        
        if (ind == 0): #HOME
            axis = 0
            wp_params = np.array([0.000000,0.000000,0.000000,0.000000,self._wp_home[0],self._wp_home[1],self._wp_home_alt])
        else: #WAYPOINT
            axis = 3 #Relative altitudes
            wp_params = np.copy(self._wp_default)
            wp_params[-1] = alt #Altitude
            wp_params[4:6] = pos 
            wp_params[1] = radius #Radius hit waypoint
            
            if (command == self._CMD_TAKEOFF):
                wp_params[0] = self._MIN_PITCH
            
        aux_string = aux_string + "\t" + str(axis) + "\t" + str(command)
        
        for param in wp_params:
            aux_string = aux_string + "\t" + str(param)
        aux_string = aux_string + "\t" + str(1) + "\n" # Autocontinue
        return aux_string
    
    def rtl(self):
        self._add_to_mavproxy_command_queue("rtl")
    
    def takeoff(self):
        if (self._log_in_flight):
            self._sharedmem.set_logging(True)
        
        if (not self._started_time):
            self._started_time = True
            self._start_time.value = time.time()
            
        if (self._automatic == False):
            #Instead of automatic takeoff set initial loiter waypoints
            self._loiter_waypoints()
            pass
        else:
            #Initial waypoints for automatic takeoff
            direction = np.flip(self._sharedmem.get_direction(),0)
            position = np.flip(self._sharedmem.get_position(),0)
            self._load_waypoints(self._CMD_TAKEOFF,[self._wp_along_direction(position,200.0,direction)],40.0,direction,self._default_radius)
            self._add_to_mavproxy_command_queue("arm throttle")

        #Flag used to put the autopilot in AUTO when it finishes loading the waypoints
        self._set_takeoff_flag(True)
        return

    def _loiter_waypoints(self):
        loc_next_pos = self._sharedmem.get_position()
        loc_current_pos = self._sharedmem.get_position()
        
        self._sharedmem.set_trajectory_parameters(loc_current_pos,auxwp=True,overwp=False,check_ang=False,pond=False)
        wp_list,distance,dir = self._sharedmem.calculate_trajectory(loc_next_pos,update_dir=True)
        
        wp_list = np.flip(wp_list,1)

        altitude = self._sharedmem.get_flight_height()
        self._load_waypoints(self._CMD_WP,wp_list,altitude,np.flip(dir,0),self._loc_radius)
        return
    
    def land(self):
        if (self._log_in_flight):
            self._sharedmem.set_logging(False)
        total_flight_time = str(time.time() - self._start_time.value)
        self._add_to_print_queue("TOTAL FLIGHT TIME: " + total_flight_time)
        self._add_to_message_queue(mesg.TIMER,total_flight_time)
        
        
        self._sharedmem.set_iteration_status(False)
        if (self._automatic == False):
            #Return to launch instead of automatic landing
            self.rtl()
        else:
            self.rtl()
            '''
            #Waypoints for automatic landing
            direction = np.flip(self._sharedmem.get_direction(),0)
            position = np.flip(self._sharedmem.get_position(),0)
            self._load_waypoints(self._CMD_LAND,[self._wp_along_direction(position,800.0,direction)]*self._get_wp_len(),0.0,direction,self._default_radius)
            '''
        return
        
    def go(self,loc_next):
        if len(loc_next) > 1:
            M = 11 #number of locations per row
            self._x_coord.value = loc_next[0]
            self._y_coord.value = loc_next[1]
            loc_next = loc_next[0] + M*loc_next[1]
            
        loc_next = int(loc_next)
        self._set_using_iterator(False)
        
        loc_next_pos = self._sharedmem.get_discretizer().get_position(loc_next)
        self._sharedmem.set_next_location(loc_next)

        self._go_to_loc(loc_next_pos)
        
    def go_next(self):
        self._set_using_iterator(True)
        self._sharedmem.set_iteration_status(False)
        
        loc_next = self._sharedmem.get_next_location()
        loc_next_pos = self._sharedmem.get_discretizer().get_position(loc_next)

        self._go_to_loc(loc_next_pos)

    def _go_to_loc(self,loc_next_pos):
        self._set_in_flight(True)
        loc_current = self._sharedmem.get_current_location()
        if(loc_current == -1):
            loc_current_pos = self._sharedmem.get_position()
            self._sharedmem.set_arrived_dir_flag(False)
        else:
            loc_current_pos = self._sharedmem.get_discretizer().get_position(loc_current)

        position = self._sharedmem.get_position()
        if (self._compare_distance(position,loc_current_pos,self._aux_radius*0.75)): #Quizas esto es poco
            loc_current_pos = position
            self._sharedmem.set_arrived_dir_flag(False)
        
        self._sharedmem.set_trajectory_parameters(loc_current_pos,auxwp=True,overwp=True,check_ang=True,pond=False)
        wp_list,distance,dir = self._sharedmem.calculate_trajectory(loc_next_pos,update_dir=True)
        
        wp_list = np.flip(wp_list,1)

        altitude = self._sharedmem.get_flight_height()
        self._load_waypoints(self._CMD_WP,wp_list,altitude,np.flip(dir,0),self._loc_radius)

    def _compare_distance(self,pos1,pos2,distance):
        dist_latlon = self._sharedmem.meters2latlon(distance)[1]

        vec = pos1 - pos2
        scale = self._sharedmem.get_scale()
        vec = vec*scale

        if (np.linalg.norm(vec) > dist_latlon):
            return True
        else:
            return False
    
    def abort_go(self):
        self._set_in_flight(False)
        self._sharedmem.set_current_location(-1)
        self._sharedmem.set_arrived_dir_flag(False)

    def set_home(self):
        pos = self._sharedmem.get_position()
        self._sharedmem.set_home_utm(pos)
        self._wp_home = np.flip(pos,0)
        self._wp_home_alt = self._sharedmem.get_altitude()
        self._add_to_message_queue(mesg.PRINT,"Home set to " + str("%.5f" % self._wp_home[0]) + " " + str("%.5f" % self._wp_home[1]) + " alt: " + str("%.2f" % self._wp_home_alt))
    
    def _set_in_flight(self,value):
        self._in_flight.value = value
    
    def _get_in_flight(self):
        return self._in_flight.value
        
    def _set_wait_ack(self,value):
        self._wait_ack.value = value
    
    def _get_wait_ack(self):
        return self._wait_ack.value
    
    def _set_wp_len(self,value):
        self._wp_len.value = value
    
    def _get_wp_len(self):
        return self._wp_len.value
    
    def _get_using_iterator(self):
        return self._using_iterator.value

    def _set_using_iterator(self,value):
        self._using_iterator.value = value
        
    def _set_wp_count(self,value):
        self._wp_count.value = value
        
    def _add_wp_count(self):
        self._wp_count.value += 1
    
    def _get_wp_count(self):
        return self._wp_count.value
    
    def _set_first_wp(self,value):
        self._first_wp.value = value
    
    def _get_first_wp(self):
        return self._first_wp.value

    def _set_takeoff_flag(self,value):
        self._takeoff_flag.value = value

    def _get_takeoff_flag(self):
        return self._takeoff_flag.value
    
    def _position_update(self, GLOBAL_POSITION_INT):
        '''update gps position'''
        lat = GLOBAL_POSITION_INT.lat*0.0000001
        lon = GLOBAL_POSITION_INT.lon*0.0000001
        relative_alt = GLOBAL_POSITION_INT.relative_alt*0.001 # mm2meter conversion
        alt = GLOBAL_POSITION_INT.alt*0.001
        
        self._sharedmem.set_position(np.array([lon,lat]))
        self._sharedmem.set_altitude(alt)
        self._sharedmem.set_relative_altitude(relative_alt)
        
        '''update ground velocity'''
        vx = GLOBAL_POSITION_INT.vx*0.01 # cm2metre
        vy = GLOBAL_POSITION_INT.vy*0.01 # cm2metre
        vz = GLOBAL_POSITION_INT.vz*0.01 # cm2metre
        
        '''update heading angle'''
        hdg = GLOBAL_POSITION_INT.hdg*0.01 # cdeg2deg
        self._sharedmem.set_yaw(hdg)
        
        direction = np.array([vy,vx])
        mod = np.linalg.norm(direction,axis=0)
        
        if(mod > 2): # m/s
            self._sharedmem.set_direction(direction/mod)
        else:
            direction = np.array([math.sin(math.radians(hdg)),math.cos(math.radians(hdg))])
            self._sharedmem.set_direction(direction)
        

    def _attitude_update(self, ATTITUDE):
        roll = ATTITUDE.roll
        self._sharedmem.set_roll(np.rad2deg(roll))
    
    def mavlink_packet(self,m):
        mtype = m.get_type()
        
        if mtype == "GLOBAL_POSITION_INT":
            self._position_update(m)

        if mtype == "ATTITUDE":
            self._attitude_update(m)
        
        elif mtype == "MISSION_ACK":
            if (self._get_wait_ack()):
                self._count_ack += 1
                if (self._count_ack == 2):
                    self._count_ack = 0
                    self._set_wait_ack(False)
                    self._set_waypoint()

                    if (self._get_takeoff_flag()):
                        self._add_to_mavproxy_command_queue("auto")
                        self._set_in_flight(True)
                        self._set_takeoff_flag(False)
        
        elif mtype == "MISSION_ITEM_REACHED":
            self._add_wp_count()
            if (self._get_wp_count() >= self._get_wp_len()):
                if(self._get_in_flight()):
                    self._set_in_flight(False)
                    
                    if (self._sharedmem.get_battery_warning() and self._sharedmem.get_next_location() != 0 and not self._mission.startswith("SIM_DELIVERY")):
                        pass
                    elif (not self._takeoff_ended):
                        if self._auto_takeoff:
                            self._add_to_event_queue('takeoff_ended')
                        self._takeoff_ended = True
                    else:
                        loc_next = self._sharedmem.get_next_location()
                        self._sharedmem.set_current_location(loc_next)
                        if (self._get_using_iterator()):
                            self._add_to_event_queue(self._reached_word)
                        else:
                            if self._sharedmem._multi_iterator.value == False:
                                if self._x_coord.value == -1:
                                    self._add_to_event_queue(self._reached_word+'[' + str(loc_next) + "]")
                                else:
                                    self._add_to_event_queue(self._reached_word+'[' + str(self._x_coord.value) + "]"+'[' + str(self._y_coord.value) + "]")
                            else:
                                region = self._regions[self._sharedmem._region_response.value]
                                self._add_to_event_queue('at.' + region)
                                if self._sharedmem._at_response.value == 0:
                                    self._add_to_event_queue('many.' + region)
                                elif  self._sharedmem._at_response.value == 1:
                                    self._add_to_event_queue('one.' + region)
                                elif  self._sharedmem._at_response.value == 2:
                                    self._add_to_event_queue('visit.' + region)
                            
                        #Send location reached to ground station
                        loc_current = self._sharedmem.get_current_location()
                        loc_current_pos = self._sharedmem.get_discretizer().get_position(loc_current)
                        self._add_to_message_queue(mesg.MARK_WP,str(loc_current_pos[1]) + ' ' + str(loc_current_pos[0]))
                        time.sleep(0.01)
                        self._add_to_message_queue(mesg.MARK_WP,str(loc_current_pos[1]) + ' ' + str(loc_current_pos[0]))
        
