cambios en mp_slipmap_ui
nohup mavproxy.py --master=/dev/ttyS0 --baudrate=57600 --load-module planner --daemon 2>&1 &


------------------------------------------------------------------------------------------------------------------------
CORRER EL SOFTWARE

1) Abrir dos terminales en la carpeta ~/Desktop/ardupilot/ArduPlane/

	--> Terminal 1:

	a) Conseguir permisos Root (la pass es 'uav'): 
	su

	b) Correr el simulador SITL + Planner (arriba del avion): 
	sim_vehicle.py -N -m '--load-module planner_viewer' -L 'TestPatrol' --console --map

	--> Terminal 2:

	a) Conseguir permisos Root (la pass es 'uav'):
	su

	b) Correr el visor de la mision (computadora en tierra):
	mavproxy.py --master=tcp:0.0.0.0:5762 --console --load-module planner

2) Entre la Terminal 1 y el mapa se define la mision

	a) En el Mapa hacer click derecho y seleccionar: Fence -> Draw
	b) Ir haciendo click en el mapa en los lugares que definiran el poligono de la region de vuelo
	c) Para terminar hacer click derecho
	d) Si se quiere cambiar el poligono volver a hacer a)
	e) Cuando la region este definida, correr este comando en la terminal: pstart
	f) Cuando aparezca en la terminal el mensaje 'Waiting for LOITER mode' correr el comando: loiter
	g) El UAV se empezara a mover y a implementar el plan cargado. Si no despega, esperar unos 5 segundos y correr el comando: arm throttle
	h) Cuando termine la mision por defecto ingresara al modo RTL y volvera a base, se puede cambiar esto por aterrizaje automatico en el archivo 'planner_plane.py'
	i) Para cortar la simulacion en cualquier momento correr el comando: pexit

------------------------------------------------------------------------------------------------------------------------
CAMBIAR EL AUTOMATA

Esta el modelo del MTSA para la mision cubrir todo esta en la carpeta ~/Desktop/MAVProxy/MAVProxy/Models/

1) Generar el controlador con el MTSA y guardarlo como un .txt (desde la pestaña 'Transitions')

2) Copiar el .txt a la carpeta ~/Desktop/MAVProxy/MAVProxy/Automatas/

3) Modificar el archivo ./config_file.txt de esta carpeta para que apunte al nuevo automata

4) Si se agregaron acciones controlables al modelo, es necesario ingresarlas a la lista 'self._controllable_events' en 'planner_automata.py'

5) Correr como root el archivo: './install.sh'

------------------------------------------------------------------------------------------------------------------------
HACER CAMBIOS AL SOFTWARE

1) Modificar los archivos que se desee de ~/Desktop/MAVProxy/MAVProxy/modules/. 
Los modulos de MAVProxy que cargan el planner y el visor de mision son:
--> mavproxy_planner.py
--> mavproxy_planner_viewer.py

El resto de los archivos y modulos auxiliares siguen la siguiente notacion:
--> planner_*

2) Correr como root el archivo: './install.sh'

------------------------------------------------------------------------------------------------------------------------
PARA REPLICAR INSTALACION EN OTRA PC:

Pasos para instalar SITL:
apt-get install git
git clone https://github.com/ArduPilot/ardupilot
cd ardupilot
git submodule update --init --recursive --progress
Copiar el archivo install-prereqs-debian.sh a la carpeta ardupilot/Tools/scripts/ (probablemente se tenga que hacer en sudo)
chmod -R 755 ../ardupilot/
Tools/scripts/install-prereqs-debian.sh -y
Agregar al path la ubicacion de sim_vehicle.py
Agregar al path la ubicacion /root/.local/bin <-- el mavproxy va a parar aca
Quitar del path la ubicacion /usr/local/bin

Paquetes para que funcione el mavproxy
# pip install future
apt-get install python-dev python-opencv python-wxgtk3.0 python-pip python-matplotlib python-pygame python-lxml python-yaml

Pasos para que funcione el planner
apt-get install libgeos-dev
pip install shapely

Copiar la carpeta MAVProxy a la nueva PC y volver a correr ./install.sh
