#include <stdio.h>
#include <stdlib.h>
#include <time.h> 
#include <math.h>
//#include "opencv2/highgui/highgui.hpp"
//#include "opencv2/imgproc.hpp"

//using namespace cv;
double TOL = 0.00001;
struct ObsVec *obstacles;
int num_obs = 0;

struct Tree
{
	double *x;
	struct Tree* branch;
	int bsize;
	bool isedge;
};

struct ObsVec
{
	int size;
	double *x;
	double *y;
};

struct VectorX
{
	struct Tree tr;
	int size;
	double **x;
	int iter;
};

struct Vertex
{
	int *path;
	int psize;
	double dist;
	int isedge;
};

struct VectorX solution;


bool pnpoly(int nvert, double *vertx, double *verty, double testx, double testy)
{
  int i, j;
  bool c = 0;
  for (i = 0, j = nvert-1; i < nvert; j = i++) {
    if ( ((verty[i]>testy) != (verty[j]>testy)) &&
     (testx < (vertx[j]-vertx[i]) * (testy-verty[i]) / (verty[j]-verty[i]) + vertx[i]) )
       c = !c;
  }
  return c;
}

bool issafe(double *x, struct ObsVec *obs, int num_obs)
{
	bool safe = true;
	for (int i=0; i<num_obs; i++)
	{
		if (pnpoly(obs[i].size,obs[i].x,obs[i].y, x[0], x[1]) == true)
		{
			safe = false;
			break;
		}
	}
	return safe;
}

double distance_sqr(double *x1, double *x2)
{
	return (pow(x1[0]-x2[0],2) + pow(x1[1]-x2[1],2));
}

bool issafeline(double *x1, double *x2, double step, struct ObsVec *obs, int num_obs)
{
	double x_aux[2];
	double dist_max = sqrt(distance_sqr(x1,x2));
	double dist = 0;
	while (dist < dist_max)
	{
		x_aux[0] = x1[0] + dist*(x2[0]-x1[0])/dist_max;
		x_aux[1] = x1[1] + dist*(x2[1]-x1[1])/dist_max;
		dist += step;
		if (issafe(x_aux,obs,num_obs) == false)
		{
			return false;
		}
	}
	return true;
}

struct Tree create_tree(double *x_init)
{
	struct Tree tr;
	tr.x = (double *) malloc(2*sizeof(double));
	
	tr.x[0] = x_init[0];
	tr.x[1] = x_init[1];
	tr.bsize = 0;
	tr.isedge = 0;
	
	return tr;
}

void destroy_vertex(struct Vertex ver)
{
	if (ver.psize > 0)
	{
		free(ver.path);		
	}
}

void destroy_tree(struct Tree tr)
{
	free(tr.x);
	for (int i=0; i<tr.bsize; i++)
	{
		destroy_tree(tr.branch[i]);
	}
}

double random_double(double min, double max)
{
	double scale = rand() / (double) RAND_MAX; /* [0, 1.0] */
    return min + scale * ( max - min );      /* [min, max] */
}

double *random_coord(double *limits)
{
	double *x = (double *) malloc(2*sizeof(double));
	x[0] = random_double(limits[0], limits[1]);
	x[1] = random_double(limits[2], limits[3]);
	return x;
}

double factor(double *xa, double *xb, double *xc)
{
	double dist = distance_sqr(xa,xb);
	if (dist < pow(TOL,2))
	{
		return 2.0; //greater than 1
	}
	
	double x_ab[2];
	double x[2];
	x_ab[0] = xb[0] - xa[0];
	x_ab[1] = xb[1] - xa[1];
	x[0] = xc[0] - xa[0];
	x[1] = xc[1] - xa[1];
	double dot_prod = pow(x_ab[0]*x[0] + x_ab[1]*x[1],2);
	double x_cos = dot_prod/dist;
	
	return x_cos/dist;
}

double distance_edge(double *xa, double *xb, double *xc)
{
	double dist = distance_sqr(xa,xb);
	if (dist < pow(TOL,2))
	{
		return -1.0;
	}
	
	double x_ab[2];
	double x[2];
	x_ab[0] = xb[0] - xa[0];
	x_ab[1] = xb[1] - xa[1];
	x[0] = xc[0] - xa[0];
	x[1] = xc[1] - xa[1];
	double dot_prod = x_ab[0]*x[0] + x_ab[1]*x[1];
	double dot_prod_sqr = pow(dot_prod,2);
	double x_cos = dot_prod_sqr/dist;
	if (x_cos > dist || dot_prod < 0)
	{
		return -1.0;
	}
	
	double cross_prod = pow(x_ab[0]*x[1] - x_ab[1]*x[0],2);
	double x_sin = cross_prod/dist;
	return x_sin;
}

struct Vertex get_nearest_edge(double *x_rand, struct Tree tr, double *x_prev, int has_prev)
{
	struct Vertex ver;
	ver.psize = 0;
	ver.isedge = 1;
	if (has_prev == 0)
	{
		ver.dist = distance_sqr(x_rand,tr.x);
	} else
	{
		ver.dist = distance_edge(x_prev,tr.x,x_rand);
		if (ver.dist < 0)
		{
			ver.dist = distance_sqr(x_rand,tr.x);
			ver.isedge = 0;
		}
	}
	for(int i=0; i<tr.bsize; i++)
	{
		struct Vertex ver_aux = get_nearest_edge(x_rand, tr.branch[i], tr.x, 1);
		if (ver.dist > ver_aux.dist)
		{
			ver.dist = ver_aux.dist;
			if (ver.psize > 0)
			{
				free(ver.path);
			}
			ver.isedge = ver_aux.isedge;
			ver.psize = 1 + ver_aux.psize;
			ver.path = (int *) malloc(sizeof(int)*ver.psize);
			ver.path[0] = i;
			for (int j=1; j<ver.psize; j++)
			{
				ver.path[j] = ver_aux.path[j-1];
			}
		}
		destroy_vertex(ver_aux);
	}
	return ver;
}

void print_path(struct Vertex ver)
{
	printf("Path: { ");
	for (int i=0; i<ver.psize; i++)
	{
		printf("%d ",ver.path[i]);
	}
	printf("}\n");
}

bool add_vertex(double *x, struct Vertex ver, struct Tree *tr, double step, struct ObsVec *obs, int num_obs)
{
	for(int i=0; i<ver.psize-1; i++)
	{
		tr = &((*tr).branch[ver.path[i]]);
	}
	
	struct Tree br_old;
	int ind;
	if (ver.psize == 0)
	{
		br_old = (*tr);
	} else
	{
		ind = ver.path[ver.psize-1];
		br_old = (*tr).branch[ver.path[ver.psize-1]];
	}
	
	double fac = factor((*tr).x,br_old.x,x);
	//printf("factor: %f\n",fac);
	if (fac > 1)
	{
		if (ver.psize > 0)
		{
			tr = &((*tr).branch[ind]);
		}
		if (issafeline((*tr).x,x,step,obs,num_obs) == false)
		{
			return false;
		}
		
		struct Tree *br_aux = (struct Tree*) malloc((*tr).bsize*sizeof(struct Tree));
		for (int i=0; i<(*tr).bsize; i++)
		{
			br_aux[i] = (*tr).branch[i];
		}
	
		if ((*tr).bsize > 0)
		{
			free((*tr).branch);
		}
		(*tr).bsize += 1;
		(*tr).branch = (struct Tree*) malloc((*tr).bsize*sizeof(struct Tree));
		for (int i=0; i<(*tr).bsize-1; i++)
		{
			(*tr).branch[i] = br_aux[i];
		}
		(*tr).branch[(*tr).bsize-1] = create_tree(x);
		free(br_aux);
	} else
	{
		double x_edge[2];
		double sqrt_fac = sqrt(fac);
		x_edge[0] = (*tr).x[0] + sqrt_fac*(br_old.x[0]-(*tr).x[0]);
		x_edge[1] = (*tr).x[1] + sqrt_fac*(br_old.x[1]-(*tr).x[1]);
		
		if (issafeline(x_edge,x,step,obs,num_obs) == false)
		{
			return false;
		}
		
		struct Tree br_new = create_tree(x_edge);
		br_new.isedge = 1;

		br_new.branch = (struct Tree*) malloc(2*sizeof(struct Tree));
		br_new.branch[0] = br_old;
		br_new.branch[1] = create_tree(x);
		br_new.bsize = 2;
		(*tr).branch[ind] = br_new;
	}
	return true;
}

int goal_reached(double *x_rand,double *x_goal,double radius)
{
	if (distance_sqr(x_rand,x_goal) < pow(radius,2))
	{
		return 1;
	} else
	{
		return 0;
	}
}

struct VectorX retrieve_path(struct Vertex ver, double *x_goal, struct Tree tr)
{
	double **x_path = (double **) malloc((ver.psize+1)*sizeof(double *));
	struct VectorX vec;
	int cant = 0;
	vec.size = 0;
	for(int j=0; j<ver.psize+1; j++)
	{
		//printf("J=%d [%f , %f] %d %d\n",j,tr.x[0],tr.x[1],tr.isedge,ver.path[j]);
		if (j<ver.psize && tr.isedge == 1 && ver.path[j] == 0)
		{
			//Este es el caso para ignorar
		} else 
		{
			vec.size += 1;
			x_path[cant] = (double *) malloc(2*sizeof(double));
			x_path[cant][0] = tr.x[0];
			x_path[cant][1] = tr.x[1];
			cant += 1;
		}
		if (j<ver.psize)
		{
			tr = tr.branch[ver.path[j]];
		}
	}
	vec.x = (double **) malloc((cant+1)*sizeof(double *));
	for (int i=0; i<cant; i++)
	{
		vec.x[i] = x_path[i];
	}
	vec.size += 1;
	vec.x[cant] = x_goal;
	free(x_path);
	return vec;
}

struct VectorX RRT_solve(double *x_init, double *x_goal, double *limits, int max_iter, double radius, struct ObsVec *obs, int num_obs, double step)
{
	struct Tree tr = create_tree(x_init);
	struct VectorX solution;
	solution.size = 0;
	solution.iter = max_iter;
	int found = 0;
	
	if (issafeline(x_init,x_goal,step,obs,num_obs) == 1)
	{
		solution.x = (double **) malloc(2*sizeof(double *));
		solution.x[0] = x_init;
		solution.x[1] = x_goal;
		solution.size = 2;
		solution.iter = 0;
		solution.tr = tr;
		return solution;
	}
	
	for (int i=0; i<max_iter; i++)
	{
		double *x_rand = random_coord(limits);
		
		if (issafe(x_rand,obs,num_obs) == 0)
		{
			//printf("%f %f unsafe\n",x_rand[0],x_rand[1]);
			continue;
		}
		
		//printf("%f %f\n",x_rand[0],x_rand[1]);
		struct Vertex ver = get_nearest_edge(x_rand, tr, x_rand, 0);
		//printf("Isedge: %d\n",(int) ver.isedge);
		
		if (add_vertex(x_rand,ver,&tr,step,obs,num_obs) == false)
		{
			//printf("Failed to add vertex\n");
		} else
		{
			if (goal_reached(x_rand,x_goal,radius) == 1)
			{
				solution = retrieve_path(ver,x_goal,tr);
			
				//printf("Found solution path: \n");
				ver = get_nearest_edge(x_rand, tr, x_rand, 0);
				//print_path(ver);
			
				found = 1;
				solution.iter = i;
				i = max_iter + 1; //break out of loop
			}
		}
		
		destroy_vertex(ver);
		free(x_rand);
	}
	
	destroy_tree(tr);  
	//solution.tr = tr;
	return  solution;
}

void print_vectorx(struct VectorX vec)
{
	printf("Solution: {\n");
	for (int i=0; i<vec.size; i++)
	{
		printf(" [%f , %f] \n",vec.x[i][0],vec.x[i][1]);
	}
	printf("}\n");
	printf("Iterations: %d\n", vec.iter);
}
/*
void paint_tree(struct Tree tr, Mat img, int thickness, int height, int prad, int scale, Point prev_p, int has_prev)
{
	Point p(tr.x[0]*scale,height - tr.x[1]*scale);
	circle(img, p, prad, CV_RGB(0.0, 0.0, 80.0),thickness);
	if (has_prev == 1)
	{
		line(img, prev_p, p, (0,0,0), thickness);
	}
	for(int i=0; i<tr.bsize; i++)
	{
		paint_tree(tr.branch[i],img,thickness,height,prad,scale,p,1);
	}
}

void paint_obstacles(Mat img,struct ObsVec *obs, int num_obs, int scale, int height)
{
	int thickness = 2;
	for(int i=0; i<num_obs; i++)
	{
		for(int j=0; j<obs[i].size-1; j++)
		{
			Point p1(obs[i].x[j]*scale,height - obs[i].y[j]*scale);
			Point p2(obs[i].x[j+1]*scale,height - obs[i].y[j+1]*scale);
			line(img, p1, p2, CV_RGB(250.0, 0.0, 0.0), thickness);
		}
	}
}

void paint_solution(Mat img, struct VectorX sol, int scale, int height)
{
	int thickness = 4;
	for (int i=1; i<sol.size; i++)
	{
		Point p1(sol.x[i-1][0]*scale,height - sol.x[i-1][1]*scale);
		Point p2(sol.x[i][0]*scale,height - sol.x[i][1]*scale);
		line(img, p1, p2, CV_RGB(0.0, 250.0, 0.0), thickness);
	}
}
*/
struct ObsVec create_Obs(double *vec_x, double *vec_y, int size)
{
	struct ObsVec obs;
	obs.size = size;
	
	obs.x = (double *) malloc(size*sizeof(double));
	obs.y = (double *) malloc(size*sizeof(double));
	for(int i=0; i<size; i++)
	{
		obs.x[i] = vec_x[i];
		obs.y[i] = vec_y[i];
	}
	return obs;
}

extern "C" void add_obstacle(double *vec_x, double *vec_y, int size)
{
	int seed = time(0);
	//seed = 1579631768;
	srand(seed);
	//printf("Seed: %d\n",seed);
	
	struct ObsVec *obs_aux = (struct ObsVec *) malloc(num_obs*sizeof(struct ObsVec));
	for (int i=0; i<num_obs; i++)
	{
		obs_aux[i] = obstacles[i];
	}
	num_obs += 1;
	obstacles = (struct ObsVec *) malloc(num_obs*sizeof(struct ObsVec));
	for (int j=0; j<num_obs-1; j++)
	{
		obstacles[j] = obs_aux[j];
	}
	obstacles[num_obs-1] = create_Obs(vec_x,vec_y,size);
	free(obs_aux);
	
	/*
	struct ObsVec *obs = obstacles;
	for(int i=0; i<num_obs; i++)
	{
		printf("Obs:%d %d:\n",i,obs[i].size);
		for (int j=0; j<obs[i].size; j++)
		{
			printf("\t[%f %f]\n",obs[i].x[j],obs[i].y[j]);
		}
	}*/
}

extern "C" void get_solution(double *x)
{
	for (int i=0; i<solution.size; i++)
	{
		x[2*i] = solution.x[i][0];
		x[2*i+1] = solution.x[i][1];
	}
}

extern "C" int rrt_solver(double *x_init, double *x_goal, double *limits, int max_iter, double radius, double step, double tol)
{
	TOL = tol;
	struct VectorX sol;
	
	sol = RRT_solve(x_init,x_goal,limits,max_iter,radius,obstacles,num_obs,step);
	
	solution.iter = sol.iter;
	solution.size = sol.size;
	solution.x = (double **) malloc(sol.size*sizeof(double *));
	for (int i=0; i<sol.size; i++)
	{
		solution.x[i] = sol.x[i];
	}
	free(sol.x);
	//print_vectorx(sol);
	return sol.size;
}

int main()
{
	double x_init[2] = {0.5,1.5};
	double x_goal[2] = {5,0.2};
	double limits[4] = {0,6,0,3}; //x0_min,x0_max, x1_min,x1_max
	int max_iter = 10000;
	double radius = 0.1;
	//int num_obs = 2;
	double step = 0.01;
	//struct ObsVec obstacles[num_obs];
	double vec_x1[5] = {1.0,3.0,3.0,1.0,1.0};
	double vec_y1[5] = {1.0,1.0,2.0,2.0,1.0};
	obstacles[0] = create_Obs(vec_x1, vec_y1, 5);
	double vec_x2[5] = {3.5,4.5,4.5,3.5,3.5};
	double vec_y2[5] = {0.7,0.7,2.8,2.8,0.7};
	obstacles[1] = create_Obs(vec_x2, vec_y2, 5);
	
	num_obs = 2;
	
	struct VectorX sol;
	sol = RRT_solve(x_init,x_goal,limits,max_iter,radius, obstacles, num_obs,step);
	
	print_vectorx(sol);
	return 0;
	
	/*
	int scale = 200;
	int height = limits[3]*scale;
	int width = limits[1]*scale;
	int prad = 1;
	int thickness = 2;
	Mat img(height, width, CV_8UC3, Scalar(255,255, 255));
	Point p_init(x_init[0]*scale,height - x_init[1]*scale);
	circle(img, p_init, radius*scale, CV_RGB(0.0, 0.0, 80.0),thickness);
	Point p_goal(x_goal[0]*scale,height - x_goal[1]*scale);
	circle(img, p_goal, radius*scale, CV_RGB(0.0, 80.0, 0.0),thickness);

	paint_obstacles(img,obstacles,num_obs,scale,height);
	paint_tree(sol.tr,img,thickness,height,prad,scale,p_goal,0);
	paint_solution(img,sol,scale,height);

	namedWindow("solution", WINDOW_AUTOSIZE);
	imshow("solution", img);
	waitKey(0);
	destroyWindow("solution");  
	
	return 0;*/
}
