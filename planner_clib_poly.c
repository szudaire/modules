#include <math.h>
#include <stdbool.h> 
#include <stdio.h>

bool pnpoly(int nvert, double *vertx, double *verty, double testx, double testy)
{
  int i, j;
  bool c = 0;
  for (i = 0, j = nvert-1; i < nvert; j = i++) {
    if ( ((verty[i]>testy) != (verty[j]>testy)) &&
     (testx < (vertx[j]-vertx[i]) * (testy-verty[i]) / (verty[j]-verty[i]) + vertx[i]) )
       c = !c;
  }
  return c;
}

bool vecpoly(int nvert, double *vertx, double *verty, int ntest, double *testx, double *testy)
{
	int i;
	for (i=0; i<ntest; i++)
	{
		if(pnpoly(nvert,vertx,verty,testx[i],testy[i]) == 1)
		{
			return 1;
		}
	}
	return 0;
}

void get_polygon_from_location(double *location, double grid_param, double angle, double *vertx, double *verty)
{
	double c_ang = cos(angle);
	double s_ang = sin(angle);
	double dirx[2] = {c_ang,s_ang};
	double diry[2] = {-s_ang,c_ang};
	double delta = 1.01*grid_param/2; //El factor 1.01 sirve evaluar trayectorias que van por los bordes
	vertx[0] = location[0] + delta*(dirx[0] + diry[0]);
	verty[0] = location[1] + delta*(dirx[1] + diry[1]);
	vertx[1] = location[0] + delta*(-dirx[0] + diry[0]);
	verty[1] = location[1] + delta*(-dirx[1] + diry[1]);
	vertx[2] = location[0] + delta*(-dirx[0] - diry[0]);
	verty[2] = location[1] + delta*(-dirx[1] - diry[1]);
	vertx[3] = location[0] + delta*(dirx[0] - diry[0]);
	verty[3] = location[1] + delta*(dirx[1] - diry[1]);
}

void test_elements(int nloc, double *locx, double *locy, bool *inside, double grid_param, double angle, int ntest, double *testx, double *testy)
{
	int i;
	double vertx[4];
	double verty[4];
	double location[2];
	for (i=0; i<nloc; i++)
	{
		location[0] = locx[i];
		location[1] = locy[i];
		get_polygon_from_location(location,grid_param,angle,vertx,verty);
		inside[i] = vecpoly(4,vertx,verty,ntest,testx,testy);
	}
}