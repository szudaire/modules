from MAVProxy.modules.planner_module import Module
from multiprocessing import Value
from ctypes import c_bool
from MAVProxy.modules import planner_util_message as mesg
import subprocess as sp
from os.path import expanduser
import time

class VideoCamera(Module):
    def init(self):
        self.controllables = []
        self._process_type = 1
        self._num = 0
	home = expanduser("~")
	self.path = home + '/.cache/mavproxy/captures/'

    def _started(self):
        self._add_to_command_queue("capture")

    def _capture(self):
        t1 = time.time()
        sp.call("ffmpeg -f h264 -loglevel panic -probesize 32 -analyzeduration 0  -y -i tcp://192.168.1.1:5555 -vframes 1 " + self.path + "out"+str(self._num)+".png",shell=True)
        self._add_to_message_queue(mesg.VIDEO_AUX,"Capturing image "+str(self._num)+"  time: " + str(time.time() - t1))
	self._num += 1

	time.sleep(5)
	self._add_to_command_queue("capture")

    def run3(self):
        count = 0
        while (not self.exit_code.value):
            count += 1
            file_name = "output" + str(count) + ".avi"
            time = 60 #seconds
            sp.call(["python","/home/pi/Desktop/Planner/Run MAVProxy/camera.py",str(time),"/home/pi/Desktop/Planner/Run MAVProxy/" + file_name])
            print("CAPTURED VIDEO " + str(count))

    def run2(self):
        video = cv.VideoCapture(0)
        fourcc = cv.cv.CV_FOURCC(*'XVID')
        out = cv.VideoWriter(self._file_name,fourcc, 5.0, (640,480))
        delta = 0.2
        
        ret, frame = video.read()
        out.write(frame)
        t1 = time.time()
        while(not self.exit_code.value):
            t_actual = time.time()
            if (t_actual > t1 + delta):
                t1 = t1 + delta
            else:
                time.sleep(0.02)
                continue

            ret, frame = video.read()
            out.write(frame)

        video.release()
        out.release()
        
    def exit_process(self):
        #self.exit_code.value = True
        return
