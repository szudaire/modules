import numpy as np

def levy(l0,mu,cant):
    u = np.random.rand(cant)
    l = l0 * ((1.0 - u)**(1.0/(1.0-mu)))
    return l

def dist_levy(l0,mu,l):
    f = np.zeros(len(l),np.float)
    f[l>=l0] = (mu-1)*(l0**(mu-1))*(l[l>=l0]**(-mu))
    return f
