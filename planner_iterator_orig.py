from MAVProxy.modules.planner_module import Module
from multiprocessing import Array,Value
from ctypes import c_bool
import numpy as np
import time
from MAVProxy.modules import planner_util_message as mesg

SORT_DIST = 1
SORT_RANDOM = 2
SORT_FIRST = 3
SORT_LAST = 4

class Iterator(Module):
	def init(self):
		self._process_type = 1 #1 has independent processes, 0 no independent processes

		self._sort_type = SORT_DIST
		self._pond = True

		self._switch = False

	def _started(self):
		if (self._sharedmem._CUDA):
			self._sharedmem.compile_cuda()
		
	def add_locations(self,pos_locations):
		self._size_locations = len(pos_locations)
		
		self._locations = np.zeros((self._size_locations,2),np.int32) #location
		self._locations[:,0] = np.array(range(self._size_locations),np.int32)
		
		self._location_list = Array('i',range(self._size_locations))
		self._array_ind = Value('i',-1)
		self._array_skip = Value('i',0)
		self._sorting_status = Value(c_bool,False) #True sorting, False not sorting
		return

        def _switch_in_location_list(self,ind1,ind2):
                aux = self._location_list[ind1]
                self._location_list[ind1] = self._location_list[ind2]
                self._location_list[ind2] = aux
	
	def has_next(self):
                self._set_iterating()
                
                array_ind = self._get_array_ind()
                array_skip = self._get_array_skip()
		if(array_ind < self._size_locations):
			self._sharedmem.set_next_location(self._location_list[array_ind])
			return 'yes_next'
		elif(array_skip < self._size_locations):
                        # Loop the elements
                        self._set_array_ind(self._get_array_skip())
                        array_ind = self._get_array_ind()
			self._sharedmem.set_next_location(self._location_list[array_ind])
                        return 'reached_end'
                else:
			return 'no_next'
        
        def _set_iterating(self):
                if (not self._sharedmem.get_iteration_status()):
                        # Stop sorting if its sorting
                        self._sharedmem.set_iteration_status(True)
                                
                        # Sleep until sorting is finished, to avoid rewriting
                        while (self._get_sorting_status()):
                                time.sleep(0.01)
	
	def skip_next(self):
                self._set_iterating()
                #self._put_next_location_first()
                
		self._add_to_array_ind()

	def reset_skipped(self):
                return 'yes_next'

        def _put_next_location_first(self):
                next_loc = self._sharedmem.get_next_location()
                array_ind = self._get_array_ind()
                if (self._location_list[array_ind] == next_loc):
                        return
                
                aux_list = np.array(self._location_list[array_ind:])
                ind = np.flatnonzero(aux_list == next_loc)
                if (len(ind) > 0):
                        ind = ind[0]
                        self._location_list[array_ind+1:array_ind+ind+1] = self._location_list[array_ind:array_ind+ind]
                        self._location_list[array_ind] = next_loc
                
	def remove_next(self):
                self._set_iterating()
                #self._put_next_location_first()
                
                self._switch_in_location_list(self._get_array_ind(),self._get_array_skip())
                self._add_to_array_ind()
		self._add_to_array_skip()
		return
	
	def reset_iterator(self):
		#Consider again the whole array
		#Array will not be in order
		self._set_array_ind(0)
		self._set_array_skip(0)
	
	def sort_locations(self):
		self._set_sorting_status(True)
		#Add to command queue to be processed in parallel
		self._add_to_command_queue("sort_locations")
	
	def _sort_locations(self):
		start_sorting = time.time()

		#Check if there is no current location
		loc_current = self._sharedmem.get_next_location()
		if(loc_current == -1):
			loc_current_pos = self._sharedmem.get_position()
		else:
			loc_current_pos = self._sharedmem.get_discretizer().get_position(loc_current)
		
		# Assume we won't remove the element
		array_ind = self._get_array_ind()+1
		if (array_ind == 0):
			self._add_to_array_ind()
			
		indexes = np.array(self._location_list[array_ind:],np.int)
		
		#Set trajectory parameters
		aux_positions = self._sharedmem.get_discretizer().get_position(indexes)
		self._sharedmem.set_trajectory_parameters(loc_current_pos,grid=aux_positions,auxwp=False,overwp=True,check_ang=True,pond=self._pond)

                if (self._sort_type == SORT_DIST):
                        #Order locations with straight distance first (fast)
                        aux_locations = np.linalg.norm(aux_positions-loc_current_pos,axis=1)
                        aux_sorted_ind = aux_locations.argsort()
                        indexes = indexes[aux_sorted_ind]
                        aux_positions = aux_positions[aux_sorted_ind]
                        aux_locations = aux_locations[aux_sorted_ind]
                        
                        index = 0
                        batch_size = 1000
                        while (index < len(indexes)):
                                if (index + batch_size > len(indexes)):
                                        batch_size = len(indexes) - index
                                pos_array = aux_positions[index:index+batch_size]

                                #Opcion python
                                '''
                                dist_array = np.zeros(len(pos_array))
                                for j in range(len(dist_array)):
                                        dist_array[j] = self._sharedmem.calculate_trajectory(pos_array[j])
                                '''
                                #Opcion clib
                                dist_array = self._sharedmem.batch_calculate_trajectory(pos_array)
                                
                                aux_locations[index:index+batch_size] = dist_array
                                
                                if (self._sharedmem.get_iteration_status()):
                                        break
                                
                                index += batch_size
                                #When there's only one processor, sleep to give time to other processes
                                #time.sleep(0.01)
                        
                        self._add_to_print_queue("Calculated " + str(index) + " elements of " + str(len(aux_locations)) + " to be calculated")
                        
                        #Order and write to location list ordered elements (very fast)
                        self._location_list[array_ind:] = indexes[aux_locations.argsort()]
                        
                elif (self._sort_type == SORT_RANDOM):
                        np.random.shuffle(indexes)
                        self._location_list[array_ind:] = indexes
                        
                elif (self._sort_type == SORT_FIRST):
                        self._switch = not self._switch
                        
                        if (self._switch):
                                ind = 165
                        else:
                                ind = 297
                                
                        aux = np.flatnonzero(indexes == ind)
                        if (len(aux) > 0):
                                aux = aux[0]
                                aux_var = indexes[aux]
                                indexes[aux] = indexes[0]
                                indexes[0] = aux_var

                        #print aux,indexes[0]
                        self._location_list[array_ind:] = indexes
                        
                elif (self._sort_type == SORT_LAST):
                        self._switch = not self._switch
                        
                        if (self._switch):
                                ind = 165
                        else:
                                ind = 297
                                
                        aux = np.flatnonzero(indexes == ind)
                        if (len(aux) > 0):
                                aux = aux[0]
                                aux_var = indexes[aux]
                                indexes[aux] = indexes[-1]
                                indexes[-1] = aux_var

                        #if (len(indexes) > 0):
                                #print aux,indexes[-1]
                        self._location_list[array_ind:] = indexes
                        
		#self._add_to_print_queue(str(self._location_list[array_ind:]))
		self._set_sorting_status(False)
		delta_time = time.time() - start_sorting
		self._add_to_print_queue("Sorting process finished, elapsed time: " + str(delta_time))
	
	def _set_sorting_status(self,status):
		self._sorting_status.value = status
		
	def _get_sorting_status(self):
		return self._sorting_status.value
	
	def _get_array_ind(self):
		return self._array_ind.value
	
	def _set_array_ind(self,value):
		self._array_ind.value = value
	
	def _add_to_array_ind(self):
		self._array_ind.value += 1

	def _get_array_skip(self):
		return self._array_skip.value
	
	def _set_array_skip(self,value):
		self._array_skip.value = value
	
	def _add_to_array_skip(self):
		self._array_skip.value += 1
