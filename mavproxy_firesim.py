#!/usr/bin/env python

import os
import os.path
import sys
from pymavlink import mavutil
import errno
import time

from MAVProxy.modules.lib import mp_module
from MAVProxy.modules.lib import mp_util
from MAVProxy.modules.lib import mp_settings
from MAVProxy.modules.lib.textconsole import SimpleConsole
from MAVProxy.modules import planner_util_message as mesg

import thread
from multiprocessing import Queue
from ctypes import *
import numpy as np
import socket

class firesim(mp_module.MPModule):
    def __init__(self, mpstate):
        """Initialise module"""
        super(firesim, self).__init__(mpstate, "firesim", "",public=True)
        
        self.add_command('fstart', self.cmd_fire_start, "Start fire simulation")
        self.mpstate.functions.process_stdin("set moddebug 3", immediate=True)
        self._txt_console = SimpleConsole()

        self._fire_queue = Queue()
        self._data_queue = Queue()
        self.fire = None
        self.safe_dist = 130.0
        self.cellsize = 30.0

        self._firesim_started = False
        self.identifier = 'FIRESIM'
        self._setup()

    def _setup(self):
        self.firesim = cdll.LoadLibrary("/root/.cache/firesim/main_fires.so")
        self.firesim.setup.argtypes = (c_int,c_float,c_float)
        self.firesim.bounded_time_propagation.argtypes = (c_float,c_float)
        self.firesim.get_map.argtypes = (c_float,POINTER(c_char))

        buffer_size = 10
        fire_speed = 9.0 #m/s
        self._fire_speed = fire_speed/self.cellsize #normalizacion del fire_speed
        self.firesim.setup(c_int(buffer_size),c_float(self._fire_speed),c_float(self.safe_dist))

    def cmd_fire_start(self, args):
        if (self._firesim_started):
            self._print_ln("FAIL: Firesim already started")
        else:
            self._firesim_started = True
            mesg.send_message(self,mesg.FIRESTART,"")
            thread.start_new_thread(self._fire_propagation,(self._fire_queue,self._data_queue,))

    def _fire_propagation(self,fire_queue,data_queue):
        time_init = time.time()
        
        N = 801
        dt = 1.0/self._fire_speed
        terrain = np.zeros((N,N),np.int8)
        c_terrain = terrain.ctypes.data_as(POINTER(c_char))
        time_curr = 0.0
        while 1:
            t1 = time.time()
            self.firesim.bounded_time_propagation(c_float(time_curr),c_float(2*dt))

            self.firesim.get_map(c_float(time_curr),c_terrain)

            fire_queue.put(terrain)
            #data_queue.put(terrain.tostring())

            time.sleep(1.0)
            #print "Delta time: " + str(time.time() - t1)
            time_curr += 1.0

    def _send_fire_data(self,data_queue,hostip):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((hostip, mesg.PORT_FIRE))
        while 1:
            data = data_queue.get()
            s.sendall(data)
        s.close()
    
    def idle_task(self):
        while self._fire_queue.qsize() > 0:
            self.fire = self._fire_queue.get()
        
        
    def _print_ln(self,print_str):
        self._txt_console.writeln(print_str)

    def mavlink_packet(self, m):
        mtype = m.get_type()
        
        if (mtype == "STATUSTEXT"):
            if (mesg.analyze(self,m.text[0])):
                code = mesg.code(m.text)
                if (code == mesg.FIREIP):
                    hostip = mesg.message(m.text)
                    thread.start_new_thread(self._send_fire_data,(self._data_queue,hostip,))

def init(mpstate):
    '''initialise module'''
    return firesim(mpstate)
