from MAVProxy.modules.planner_module import Module
from MAVProxy.modules.mavproxy_map import mp_tile
from MAVProxy.modules import planner_util_message as mesg
from multiprocessing import Array,Value
import cv2 as cv
import numpy as np
import time
from os.path import expanduser

class BorderSensor(Module):
	def init(self):
		home = expanduser("~")
		path = home + '/.cache/mavproxy/cache/'
		self.path = home + '/.cache/mavproxy/captures/'
		self._tile = mp_tile.MPTile(cache_path=path,debug=False)
		self._height = 300
		self._width = 300
		self._field_of_view = 50.0/150.0 #100 meters at 100 meters of altitude
		self._aux_radius = 40.0 #Radius for intermediate waypoints
		
		self._process_type = 1
		return
	
	def _started(self):
		#cv.namedWindow('capture', cv.CV_WINDOW_AUTOSIZE)
		#cv.namedWindow('process', cv.CV_WINDOW_AUTOSIZE)
		#cv.startWindowThread()
		#self.descarga_num = 0
                #This causes a memory leak, don't use namedwindow and replace it with waitKey(0)
                pass
	
	def _capture(self):
                next = self._sharedmem.get_next_location()
		image = self._image_list[next]

		cv.imshow('capture',image)

		ret = self._process_image(image)

		if (ret):
                        self._add_to_event_queue('yes_border_next')
                else:
                        self._add_to_event_queue('no_border_next')
                #cv.imwrite(self.path + str(current) + ".png",image)
		return
		
	def _download_image(self,lat,lon,ground_width):
		image = self._tile.area_to_image(lat, lon, self._width, self._height, ground_width)
		while (self._tile.tiles_pending() > 0):
			time.sleep(0.01)
		image = self._tile.area_to_image(lat, lon, self._width, self._height, ground_width)

                image = cv.cvtColor(image,cv.COLOR_RGB2BGR)
		
		#self._image_list.append(image)
		#self.descarga_num += 1
		#cv.imwrite(self.path + 'data_set/' + str(self.descarga_num) + ".png",image)
		
		#cv.imshow('capture',image)
		return image

        def add_locations(self,pos_locations):
                self._image_list = [[]]*len(pos_locations)
		self._border_list = Array('i',range(len(pos_locations)))
		self._nofly_list = Array('i',range(len(pos_locations)))
                self._add_to_command_queue('add_locations')
                
		self._location_list = Array('i',range(len(pos_locations)))
		self._array_end = Value('i',0)
                
        def _add_locations(self):
		ground_width = self._field_of_view*self._sharedmem.get_flight_height()

                cant = 0
                for i in range(len(self._image_list)):
                        elem = self._sharedmem.get_discretizer().get_capture_polygon(i,ground_width)[1,:]
                        image = self._download_image(elem[1],elem[0],ground_width)
                        self._image_list[i] = image

                        cant += 1
                        if (cant == 300): #Print a download message every X locations
                                self._add_to_print_queue("Downloaded location " + str(i+1) + " of " + str(len(self._image_list)))
                                cant = 0

                self._add_to_print_queue("Finished Downloading")
                
                fire_vec = np.array([0]*len(self._image_list))
                for i in range(len(self._image_list)):
                        ret = self._process_image(self._image_list[i])
                        if (ret):
                                fire_vec[i] = 1

                locations = self._sharedmem.get_discretizer().get_position(range(len(self._image_list)))
                locations = locations * self._sharedmem.get_scale()
                fire_loc = locations[np.flatnonzero(fire_vec == 1)]
                safe_dist1 = self._sharedmem.meters2latlon(230)[1]
                safe_dist2 = self._sharedmem.meters2latlon(270)[1]
                nofly_dist = self._sharedmem.meters2latlon(60)[1]
                for i in range(len(self._image_list)):
                        min_dist = np.min(np.linalg.norm(fire_loc - locations[i],axis=1))
                        
                        if (min_dist > safe_dist1 and min_dist < safe_dist2):
                                self._border_list[i] = 1
                                loc_pos = self._sharedmem.get_discretizer().get_position(i)
                                self._add_to_message_queue(mesg.BORDER,str(loc_pos[1]) + ' ' + str(loc_pos[0]))
                        else:
                                self._border_list[i] = 0

                        if (min_dist < nofly_dist):
                                self._nofly_list[i] = 1
                                loc_pos = self._sharedmem.get_discretizer().get_position(i)
                                self._add_to_message_queue(mesg.NOFLY,str(loc_pos[1]) + ' ' + str(loc_pos[0]))
                        else:
                                self._nofly_list[i] = 0

                self._add_to_print_queue("Finished Calculating Border and NoFly")
                
                
	def _add_to_array_end(self):
                self._array_end.value += 1
                
	def _substract_to_array_end(self):
                self._array_end.value -= 1

        def _get_array_end(self):
                return self._array_end.value

        def _add_to_list(self,value):
                self._location_list[self._get_array_end()] = value
                self._add_to_array_end()

        def _get_list(self):
                return self._location_list[0:self._get_array_end()]

        def _remove_from_list(self,value):
                array = np.array(self._get_list())
                ind = np.flatnonzero(array==value)[0]
                aux = self._location_list[ind+1:self._get_array_end()]
                self._substract_to_array_end()
                self._location_list[ind:self._get_array_end()] = aux

        def _process_image(self,image):
                hsv = cv.cvtColor(image, cv.COLOR_BGR2HSV)
                # define range of blue color in HSV
                lower_blue = np.array([90,100,30])
                upper_blue = np.array([110,255,255])
                mask = cv.inRange(hsv, lower_blue, upper_blue)
                
                blur = cv.blur(mask,(5,5))
		#cv.imshow('process',mask)

                next = self._sharedmem.get_next_location()
                cant = len(blur[blur<50])
                if (cant > 30000):
                        ground_width = self._field_of_view*self._sharedmem.get_flight_height()
                        area = self._sharedmem.get_discretizer().get_capture_polygon(next,ground_width)
                        elements = self._sharedmem.get_discretizer().get_locations_from_area(area,0)
                        for elem in elements:
                                if (elem not in self._get_list()):
                                        self._add_to_list(elem)
                        loc_next_pos = self._sharedmem.get_discretizer().get_position(next)
                        #self._add_to_message_queue(mesg.FOUND,str(loc_next_pos[1]) + ' ' + str(loc_next_pos[0]))
                        #self._add_to_event_queue('yes_border_next')
                        return True
                else:
                        if (next in self._get_list()):
                                self._remove_from_list(next)
                        #self._add_to_event_queue('no_border_next')
                        return False

	def has_border_next(self):
                next_pos = self._sharedmem.get_next_location()
                if (self._border_list[next_pos] == 1 and self.is_safe_next()):
                        return "yes_border_next"
                else:
                        return "no_border_next"
		#self._add_to_command_queue('capture')

        def is_safe_next(self):
                t1 = time.time()
                
		loc_next = self._sharedmem.get_next_location()
		loc_next_pos = self._sharedmem.get_discretizer().get_position(loc_next)
		loc_current = self._sharedmem.get_current_location()
		if(loc_current == -1):
			loc_current_pos = self._sharedmem.get_position()
		else:
			loc_current_pos = self._sharedmem.get_discretizer().get_position(loc_current)

                position = self._sharedmem.get_position()
		if (self._compare_distance(position,loc_current_pos,self._aux_radius*0.75)):
                        loc_current_pos = position
		
		self._sharedmem.set_trajectory_parameters(loc_current_pos,auxwp=True,overwp=True,check_ang=True,pond=False)
		wp_list,distance,dir = self._sharedmem.calculate_trajectory(loc_next_pos,update_dir=False)

                elements = self._sharedmem.get_discretizer()._select_elements_from_trajectory(wp_list)
                self._add_to_print_queue("Evaluation time (is.safe.next): " + str(time.time() - t1))

                issafe = True
                for i in range(len(elements)):
                        if self._nofly_list[elements[i]] == 1:
                                issafe = False
                                break
                self._add_to_print_queue("Is safe: " + str(issafe))
                
                return issafe


        def _compare_distance(self,pos1,pos2,distance):
                dist_latlon = self._sharedmem.meters2latlon(distance)[1]

                vec = pos1 - pos2
                scale = self._sharedmem.get_scale()
                vec = vec*scale

                if (np.linalg.norm(vec) > dist_latlon):
                        return True
                else:
                        return False
	


