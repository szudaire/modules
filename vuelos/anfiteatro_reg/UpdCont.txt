Process:
	UpdCont
States:
	508
Transitions:
	UpdCont = Q0,
	Q0	= (beginUpdate -> Q157
		  |initial_config -> Q162),
	Q1	= (go[32] -> Q26
		  |beginUpdate -> Q216),
	Q2	= (arrived[53] -> Q462),
	Q3	= (beginUpdate -> Q160
		  |arrived[68] -> Q268),
	Q4	= (go[109] -> Q30
		  |go[123] -> Q76
		  |go[96] -> Q202),
	Q5	= (go[123] -> Q76),
	Q6	= (arrived[23] -> Q267),
	Q7	= (stopOldSpec -> Q479),
	Q8	= (arrived[47] -> Q194),
	Q9	= (arrived[67] -> Q31
		  |beginUpdate -> Q372),
	Q10	= (arrived[134] -> Q32),
	Q11	= (arrived[134] -> Q32),
	Q12	= (arrived[35] -> Q36),
	Q13	= (arrived[35] -> Q36
		  |reconfigure -> Q155),
	Q14	= (arrived[126] -> Q35),
	Q15	= (battery_charged -> Q37),
	Q16	= (arrived[120] -> Q292),
	Q17	= (arrived[120] -> Q292),
	Q18	= (arrived[47] -> Q40
		  |beginUpdate -> Q193),
	Q19	= (arrived[94] -> Q367),
	Q20	= (arrived[93] -> Q38
		  |reconfigure -> Q435),
	Q21	= (arrived[94] -> Q367),
	Q22	= (battery_charged -> Q45),
	Q23	= (arrived[32] -> Q215),
	Q24	= (arrived[68] -> Q47),
	Q25	= (arrived[82] -> Q46
		  |reconfigure -> Q440),
	Q26	= (arrived[32] -> Q321
		  |beginUpdate -> Q438),
	Q27	= (arrived[82] -> Q46),
	Q28	= (reconfigure -> Q443),
	Q29	= (stopOldSpec -> Q130),
	Q30	= (arrived[109] -> Q445),
	Q31	= (go[54] -> Q56
		  |go[68] -> Q251
		  |beginUpdate -> Q310),
	Q32	= (go[121] -> Q55),
	Q33	= (go[34] -> Q383),
	Q34	= (reconfigure -> Q33
		  |land -> Q57
		  |go[36] -> Q115
		  |go[25] -> Q135
		  |go[34] -> Q158),
	Q35	= (go[125] -> Q81),
	Q36	= (go[34] -> Q158),
	Q37	= (initial_config -> Q58),
	Q38	= (reconfigure -> Q459),
	Q39	= (arrived[132] -> Q382),
	Q40	= (beginUpdate -> Q52
		  |go[36] -> Q63),
	Q41	= (arrived[92] -> Q64),
	Q42	= (arrived[106] -> Q291),
	Q43	= (go[67] -> Q189),
	Q44	= (arrived[55] -> Q203),
	Q45	= (initial_config -> Q67),
	Q46	= (go[96] -> Q209),
	Q47	= (go[82] -> Q440),
	Q48	= (arrived[35] -> Q36),
	Q49	= (land -> Q73
		  |go[81] -> Q168
		  |go[68] -> Q188
		  |go[96] -> Q209
		  |reconfigure -> Q468),
	Q50	= (beginUpdate -> Q68
		  |arrived[43] -> Q280),
	Q51	= (arrived[35] -> Q36
		  |reconfigure -> Q153),
	Q52	= (stopOldSpec -> Q190),
	Q53	= (initial_config -> Q78),
	Q54	= (arrived[35] -> Q36),
	Q55	= (arrived[121] -> Q212),
	Q56	= (arrived[54] -> Q80
		  |beginUpdate -> Q231),
	Q57	= (landed -> Q82),
	Q58	= (takeoff -> Q83),
	Q59	= (arrived[121] -> Q211),
	Q60	= (startNewSpec -> Q279),
	Q61	= (arrived[121] -> Q212),
	Q62	= (go[94] -> Q154),
	Q63	= (arrived[36] -> Q85
		  |beginUpdate -> Q103),
	Q64	= (go[106] -> Q86
		  |go[93] -> Q123),
	Q65	= (arrived[120] -> Q292),
	Q66	= (arrived[120] -> Q292),
	Q67	= (takeoff -> Q88),
	Q68	= (stopOldSpec -> Q71
		  |arrived[43] -> Q93),
	Q69	= (arrived[43] -> Q489),
	Q70	= (arrived[24] -> Q89),
	Q71	= (arrived[43] -> Q489
		  |reconfigure -> Q491),
	Q72	= (arrived[68] -> Q29
		  |stopOldSpec -> Q105),
	Q73	= (landed -> Q92),
	Q74	= (arrived[24] -> Q89),
	Q75	= (arrived[32] -> Q7
		  |stopOldSpec -> Q104),
	Q76	= (arrived[123] -> Q96),
	Q77	= (stopOldSpec -> Q335),
	Q78	= (takeoff -> Q97),
	Q79	= (go[36] -> Q337),
	Q80	= (go[55] -> Q99
		  |go[42] -> Q138
		  |beginUpdate -> Q170),
	Q81	= (arrived[125] -> Q480),
	Q82	= (battery_charged -> Q100),
	Q83	= (takeoff_ended -> Q482),
	Q84	= (arrived[133] -> Q447),
	Q85	= (beginUpdate -> Q210
		  |go[35] -> Q217),
	Q86	= (arrived[106] -> Q291),
	Q87	= (arrived[93] -> Q461),
	Q88	= (takeoff_ended -> Q489),
	Q89	= (go[34] -> Q227
		  |go[23] -> Q243),
	Q90	= (go[54] -> Q229),
	Q91	= (stopOldSpec -> Q165),
	Q92	= (battery_charged -> Q109),
	Q93	= (stopOldSpec -> Q488),
	Q94	= (stopOldSpec -> Q125
		  |arrived[55] -> Q172),
	Q95	= (reconfigure -> Q4),
	Q96	= (go[124] -> Q163),
	Q97	= (takeoff_ended -> Q499),
	Q98	= (go[122] -> Q134),
	Q99	= (beginUpdate -> Q116
		  |arrived[55] -> Q240),
	Q100	= (initial_config -> Q118),
	Q101	= (arrived[31] -> Q120),
	Q102	= (arrived[134] -> Q32),
	Q103	= (arrived[36] -> Q210
		  |stopOldSpec -> Q357),
	Q104	= (reconfigure -> Q23
		  |arrived[32] -> Q482),
	Q105	= (reconfigure -> Q24
		  |arrived[68] -> Q129),
	Q106	= (arrived[32] -> Q482),
	Q107	= (arrived[68] -> Q129),
	Q108	= (arrived[32] -> Q482),
	Q109	= (initial_config -> Q133),
	Q110	= (arrived[43] -> Q93
		  |stopOldSpec -> Q472),
	Q111	= (arrived[68] -> Q47),
	Q112	= (arrived[33] -> Q500),
	Q113	= (arrived[33] -> Q500),
	Q114	= (arrived[36] -> Q385
		  |reconfigure -> Q428),
	Q115	= (arrived[36] -> Q385),
	Q116	= (arrived[55] -> Q172
		  |stopOldSpec -> Q264),
	Q117	= (arrived[95] -> Q60),
	Q118	= (takeoff -> Q139),
	Q119	= (land -> Q140
		  |go[30] -> Q239
		  |go[32] -> Q348
		  |go[21] -> Q375
		  |go[42] -> Q404
		  |reconfigure -> Q446),
	Q120	= (go[42] -> Q404),
	Q121	= (stopOldSpec -> Q49),
	Q122	= (arrived[123] -> Q96),
	Q123	= (arrived[93] -> Q461),
	Q124	= (arrived[119] -> Q354),
	Q125	= (reconfigure -> Q44
		  |arrived[55] -> Q284),
	Q126	= (arrived[55] -> Q284),
	Q127	= (reconfigure -> Q43
		  |land -> Q142
		  |go[53] -> Q244
		  |go[42] -> Q249
		  |go[55] -> Q266
		  |go[67] -> Q271),
	Q128	= (go[67] -> Q271),
	Q129	= (go[82] -> Q27),
	Q130	= (go[82] -> Q27
		  |reconfigure -> Q47
		  |land -> Q146
		  |go[55] -> Q246
		  |go[67] -> Q269),
	Q131	= (arrived[32] -> Q7
		  |stopOldSpec -> Q452),
	Q132	= (arrived[96] -> Q278),
	Q133	= (takeoff -> Q148),
	Q134	= (arrived[122] -> Q150),
	Q135	= (arrived[25] -> Q419),
	Q136	= (startNewSpec -> Q455),
	Q137	= (go[134] -> Q156),
	Q138	= (beginUpdate -> Q145
		  |arrived[42] -> Q152),
	Q139	= (takeoff_ended -> Q36),
	Q140	= (landed -> Q161),
	Q141	= (startNewSpec -> Q361),
	Q142	= (landed -> Q164),
	Q143	= (arrived[42] -> Q166),
	Q144	= (go[31] -> Q287),
	Q145	= (arrived[42] -> Q91
		  |stopOldSpec -> Q250),
	Q146	= (landed -> Q169),
	Q147	= (stopOldSpec -> Q297
		  |arrived[33] -> Q424),
	Q148	= (takeoff_ended -> Q46),
	Q149	= (go[123] -> Q122),
	Q150	= (go[121] -> Q192),
	Q151	= (arrived[43] -> Q93
		  |stopOldSpec -> Q465),
	Q152	= (go[43] -> Q50
		  |beginUpdate -> Q91
		  |go[31] -> Q176),
	Q153	= (arrived[35] -> Q33),
	Q154	= (arrived[94] -> Q136),
	Q155	= (arrived[35] -> Q33),
	Q156	= (arrived[134] -> Q32),
	Q157	= (initial_config -> Q77
		  |stopOldSpec -> Q316),
	Q158	= (arrived[34] -> Q178),
	Q159	= (arrived[34] -> Q178
		  |reconfigure -> Q383),
	Q160	= (arrived[68] -> Q29
		  |stopOldSpec -> Q187),
	Q161	= (battery_charged -> Q179),
	Q162	= (beginUpdate -> Q77
		  |takeoff -> Q180),
	Q163	= (arrived[124] -> Q408),
	Q164	= (battery_charged -> Q181),
	Q165	= (go[43] -> Q69
		  |reconfigure -> Q90
		  |land -> Q185
		  |go[31] -> Q285
		  |go[54] -> Q307
		  |go[41] -> Q322),
	Q166	= (go[54] -> Q307),
	Q167	= (arrived[55] -> Q203),
	Q168	= (arrived[81] -> Q414),
	Q169	= (battery_charged -> Q183),
	Q170	= (stopOldSpec -> Q127),
	Q171	= (arrived[42] -> Q90),
	Q172	= (stopOldSpec -> Q283),
	Q173	= (stopOldSpec -> Q159
		  |arrived[34] -> Q326),
	Q174	= (arrived[47] -> Q194
		  |reconfigure -> Q198),
	Q175	= (arrived[47] -> Q194),
	Q176	= (arrived[31] -> Q1
		  |beginUpdate -> Q197),
	Q177	= (go[35] -> Q54
		  |go[24] -> Q74
		  |land -> Q199
		  |reconfigure -> Q257
		  |go[33] -> Q298),
	Q178	= (go[33] -> Q298),
	Q179	= (initial_config -> Q200),
	Q180	= (takeoff_ended -> Q201
		  |beginUpdate -> Q497),
	Q181	= (initial_config -> Q206),
	Q182	= (arrived[55] -> Q203),
	Q183	= (initial_config -> Q207),
	Q184	= (arrived[67] -> Q208),
	Q185	= (landed -> Q204),
	Q186	= (stopOldSpec -> Q13
		  |arrived[35] -> Q248),
	Q187	= (reconfigure -> Q111
		  |arrived[68] -> Q129),
	Q188	= (arrived[68] -> Q129),
	Q189	= (arrived[67] -> Q208),
	Q190	= (reconfigure -> Q79
		  |land -> Q213
		  |go[48] -> Q352
		  |go[36] -> Q359),
	Q191	= (arrived[43] -> Q395),
	Q192	= (arrived[121] -> Q214),
	Q193	= (arrived[47] -> Q52
		  |stopOldSpec -> Q174),
	Q194	= (go[36] -> Q359),
	Q195	= (go[133] -> Q218
		  |go[144] -> Q237),
	Q196	= (arrived[95] -> Q141),
	Q197	= (arrived[31] -> Q216
		  |stopOldSpec -> Q288),
	Q198	= (arrived[47] -> Q79),
	Q199	= (landed -> Q220),
	Q200	= (takeoff -> Q221),
	Q201	= (go[55] -> Q222
		  |beginUpdate -> Q403),
	Q202	= (arrived[96] -> Q225),
	Q203	= (go[68] -> Q24),
	Q204	= (battery_charged -> Q228),
	Q205	= (arrived[31] -> Q446),
	Q206	= (takeoff -> Q226),
	Q207	= (takeoff -> Q230),
	Q208	= (go[81] -> Q345),
	Q209	= (arrived[96] -> Q362),
	Q210	= (stopOldSpec -> Q386),
	Q211	= (go[134] -> Q102
		  |go[108] -> Q234
		  |go[122] -> Q252
		  |go[120] -> Q272),
	Q212	= (go[122] -> Q253),
	Q213	= (landed -> Q235),
	Q214	= (go[120] -> Q274),
	Q215	= (go[43] -> Q191),
	Q216	= (stopOldSpec -> Q119),
	Q217	= (beginUpdate -> Q186
		  |arrived[35] -> Q236),
	Q218	= (arrived[133] -> Q449),
	Q219	= (go[35] -> Q155),
	Q220	= (battery_charged -> Q238),
	Q221	= (takeoff_ended -> Q120),
	Q222	= (beginUpdate -> Q232
		  |arrived[55] -> Q240),
	Q223	= (go[110] -> Q241
		  |go[95] -> Q262),
	Q224	= (go[110] -> Q242
		  |go[95] -> Q263),
	Q225	= (go[110] -> Q242),
	Q226	= (takeoff_ended -> Q128),
	Q227	= (arrived[34] -> Q178),
	Q228	= (initial_config -> Q247),
	Q229	= (arrived[54] -> Q43),
	Q230	= (takeoff_ended -> Q129),
	Q231	= (arrived[54] -> Q170
		  |stopOldSpec -> Q418),
	Q232	= (arrived[55] -> Q172
		  |stopOldSpec -> Q378),
	Q233	= (stopOldSpec -> Q114
		  |arrived[36] -> Q210),
	Q234	= (arrived[108] -> Q377),
	Q235	= (battery_charged -> Q256),
	Q236	= (beginUpdate -> Q248
		  |go[34] -> Q254),
	Q237	= (arrived[144] -> Q137),
	Q238	= (initial_config -> Q259),
	Q239	= (arrived[30] -> Q260),
	Q240	= (beginUpdate -> Q172
		  |go[43] -> Q261),
	Q241	= (arrived[110] -> Q5),
	Q242	= (arrived[110] -> Q5),
	Q243	= (arrived[23] -> Q267),
	Q244	= (arrived[53] -> Q462),
	Q245	= (reconfigure -> Q167
		  |arrived[55] -> Q284),
	Q246	= (arrived[55] -> Q284),
	Q247	= (takeoff -> Q265),
	Q248	= (stopOldSpec -> Q34),
	Q249	= (arrived[42] -> Q166),
	Q250	= (arrived[42] -> Q166
		  |reconfigure -> Q171),
	Q251	= (arrived[68] -> Q268
		  |beginUpdate -> Q289),
	Q252	= (arrived[122] -> Q149),
	Q253	= (arrived[122] -> Q149),
	Q254	= (beginUpdate -> Q173
		  |arrived[34] -> Q275),
	Q255	= (go[55] -> Q376),
	Q256	= (initial_config -> Q273),
	Q257	= (go[33] -> Q113),
	Q258	= (land -> Q276
		  |go[55] -> Q376
		  |reconfigure -> Q499),
	Q259	= (takeoff -> Q277),
	Q260	= (go[41] -> Q333),
	Q261	= (beginUpdate -> Q151
		  |arrived[43] -> Q280),
	Q262	= (arrived[95] -> Q282),
	Q263	= (arrived[95] -> Q282),
	Q264	= (reconfigure -> Q182
		  |arrived[55] -> Q284),
	Q265	= (takeoff_ended -> Q166),
	Q266	= (arrived[55] -> Q284),
	Q267	= (go[33] -> Q416
		  |go[22] -> Q439),
	Q268	= (beginUpdate -> Q29
		  |go[55] -> Q400),
	Q269	= (arrived[67] -> Q290),
	Q270	= (reconfigure -> Q184
		  |arrived[67] -> Q290),
	Q271	= (arrived[67] -> Q290),
	Q272	= (arrived[120] -> Q292),
	Q273	= (takeoff -> Q293),
	Q274	= (arrived[120] -> Q296),
	Q275	= (go[33] -> Q295
		  |beginUpdate -> Q326),
	Q276	= (landed -> Q294),
	Q277	= (takeoff_ended -> Q178),
	Q278	= (startNewSpec -> Q223),
	Q279	= (go[109] -> Q301
		  |go[94] -> Q341
		  |go[96] -> Q393),
	Q280	= (beginUpdate -> Q93
		  |go[32] -> Q300),
	Q281	= (go[109] -> Q302
		  |go[94] -> Q342
		  |go[96] -> Q394),
	Q282	= (go[109] -> Q302
		  |go[96] -> Q394),
	Q283	= (go[68] -> Q107
		  |reconfigure -> Q203
		  |land -> Q305
		  |go[54] -> Q415
		  |go[43] -> Q466),
	Q284	= (go[68] -> Q107),
	Q285	= (arrived[31] -> Q120),
	Q286	= (reconfigure -> Q208
		  |land -> Q306
		  |go[54] -> Q420
		  |go[81] -> Q442
		  |go[66] -> Q467
		  |go[68] -> Q493),
	Q287	= (arrived[31] -> Q120),
	Q288	= (arrived[31] -> Q120
		  |reconfigure -> Q205),
	Q289	= (arrived[68] -> Q29
		  |stopOldSpec -> Q492),
	Q290	= (go[81] -> Q442),
	Q291	= (go[119] -> Q124
		  |go[107] -> Q311),
	Q292	= (go[121] -> Q61),
	Q293	= (takeoff_ended -> Q194),
	Q294	= (battery_charged -> Q315),
	Q295	= (beginUpdate -> Q147
		  |arrived[33] -> Q314),
	Q296	= (go[121] -> Q59
		  |go[133] -> Q84
		  |go[107] -> Q312
		  |go[119] -> Q328),
	Q297	= (reconfigure -> Q113
		  |arrived[33] -> Q317),
	Q298	= (arrived[33] -> Q317),
	Q299	= (reconfigure -> Q224
		  |go[110] -> Q319
		  |go[95] -> Q339),
	Q300	= (beginUpdate -> Q75
		  |arrived[32] -> Q321),
	Q301	= (arrived[109] -> Q445),
	Q302	= (arrived[109] -> Q445),
	Q303	= (reconfigure -> Q224),
	Q304	= (go[80] -> Q330),
	Q305	= (landed -> Q323),
	Q306	= (landed -> Q324),
	Q307	= (arrived[54] -> Q128),
	Q308	= (stopOldSpec -> Q51
		  |arrived[35] -> Q248),
	Q309	= (arrived[66] -> Q304),
	Q310	= (stopOldSpec -> Q286),
	Q311	= (arrived[107] -> Q329),
	Q312	= (arrived[107] -> Q327),
	Q313	= (arrived[55] -> Q203),
	Q314	= (go[32] -> Q336
		  |beginUpdate -> Q424),
	Q315	= (initial_config -> Q334),
	Q316	= (reconfigure -> Q53
		  |initial_config -> Q334),
	Q317	= (go[32] -> Q454),
	Q318	= (go[23] -> Q6
		  |land -> Q338
		  |go[32] -> Q454
		  |go[34] -> Q498
		  |reconfigure -> Q500),
	Q319	= (arrived[110] -> Q95
		  |reconfigure -> Q241),
	Q320	= (go[95] -> Q117),
	Q321	= (beginUpdate -> Q7
		  |go[33] -> Q340),
	Q322	= (arrived[41] -> Q344),
	Q323	= (battery_charged -> Q343),
	Q324	= (battery_charged -> Q346),
	Q325	= (arrived[54] -> Q43),
	Q326	= (stopOldSpec -> Q177),
	Q327	= (go[106] -> Q42
		  |go[120] -> Q65
		  |go[93] -> Q87
		  |go[108] -> Q349),
	Q328	= (arrived[119] -> Q350),
	Q329	= (go[120] -> Q66
		  |go[108] -> Q351),
	Q330	= (arrived[80] -> Q62),
	Q331	= (arrived[32] -> Q215),
	Q332	= (arrived[32] -> Q215),
	Q333	= (arrived[41] -> Q344),
	Q334	= (takeoff -> Q353),
	Q335	= (reconfigure -> Q78
		  |takeoff -> Q353),
	Q336	= (beginUpdate -> Q131
		  |arrived[32] -> Q356),
	Q337	= (arrived[36] -> Q219),
	Q338	= (landed -> Q360),
	Q339	= (reconfigure -> Q262
		  |arrived[95] -> Q366),
	Q340	= (arrived[33] -> Q364
		  |beginUpdate -> Q495),
	Q341	= (arrived[94] -> Q367),
	Q342	= (arrived[94] -> Q367),
	Q343	= (initial_config -> Q370),
	Q344	= (go[53] -> Q2),
	Q345	= (arrived[81] -> Q320),
	Q346	= (initial_config -> Q373),
	Q347	= (reconfigure -> Q331
		  |arrived[32] -> Q482),
	Q348	= (arrived[32] -> Q482),
	Q349	= (arrived[108] -> Q377),
	Q350	= (go[120] -> Q16
		  |go[132] -> Q39
		  |go[106] -> Q379
		  |go[118] -> Q402),
	Q351	= (arrived[108] -> Q377),
	Q352	= (arrived[48] -> Q384),
	Q353	= (takeoff_ended -> Q255),
	Q354	= (go[120] -> Q17),
	Q355	= (reconfigure -> Q97
		  |takeoff_ended -> Q255),
	Q356	= (beginUpdate -> Q7
		  |go[43] -> Q381),
	Q357	= (reconfigure -> Q337
		  |arrived[36] -> Q385),
	Q358	= (go[124] -> Q387),
	Q359	= (arrived[36] -> Q385),
	Q360	= (battery_charged -> Q388),
	Q361	= (reconfigure -> Q281
		  |go[109] -> Q389
		  |go[94] -> Q436
		  |go[96] -> Q487),
	Q362	= (startNewSpec -> Q299),
	Q363	= (arrived[143] -> Q195),
	Q364	= (go[34] -> Q390
		  |beginUpdate -> Q424),
	Q365	= (go[95] -> Q369
		  |go[108] -> Q392
		  |go[93] -> Q437),
	Q366	= (reconfigure -> Q281),
	Q367	= (go[95] -> Q369
		  |go[108] -> Q392),
	Q368	= (arrived[95] -> Q282),
	Q369	= (arrived[95] -> Q282),
	Q370	= (takeoff -> Q396),
	Q371	= (arrived[43] -> Q395),
	Q372	= (stopOldSpec -> Q270
		  |arrived[67] -> Q310),
	Q373	= (takeoff -> Q397),
	Q374	= (go[94] -> Q21
		  |go[107] -> Q401
		  |go[109] -> Q423
		  |go[121] -> Q506),
	Q375	= (arrived[21] -> Q144),
	Q376	= (arrived[55] -> Q284),
	Q377	= (go[109] -> Q423
		  |go[121] -> Q506),
	Q378	= (arrived[55] -> Q284
		  |reconfigure -> Q313),
	Q379	= (arrived[106] -> Q291),
	Q380	= (arrived[34] -> Q257),
	Q381	= (beginUpdate -> Q110
		  |arrived[43] -> Q406),
	Q382	= (go[119] -> Q405
		  |go[133] -> Q427),
	Q383	= (arrived[34] -> Q257),
	Q384	= (go[47] -> Q8),
	Q385	= (go[35] -> Q12),
	Q386	= (go[35] -> Q12
		  |go[47] -> Q175
		  |reconfigure -> Q219
		  |land -> Q407),
	Q387	= (arrived[124] -> Q409),
	Q388	= (initial_config -> Q411),
	Q389	= (arrived[109] -> Q28
		  |reconfigure -> Q301),
	Q390	= (beginUpdate -> Q399
		  |arrived[34] -> Q413),
	Q391	= (arrived[108] -> Q377),
	Q392	= (arrived[108] -> Q377),
	Q393	= (arrived[96] -> Q225),
	Q394	= (arrived[96] -> Q225),
	Q395	= (go[55] -> Q44),
	Q396	= (takeoff_ended -> Q284),
	Q397	= (takeoff_ended -> Q290),
	Q398	= (arrived[68] -> Q47),
	Q399	= (arrived[34] -> Q326
		  |stopOldSpec -> Q494),
	Q400	= (arrived[55] -> Q240
		  |beginUpdate -> Q417),
	Q401	= (arrived[107] -> Q329),
	Q402	= (arrived[118] -> Q425),
	Q403	= (stopOldSpec -> Q258),
	Q404	= (arrived[42] -> Q166),
	Q405	= (arrived[119] -> Q354),
	Q406	= (beginUpdate -> Q93
		  |go[55] -> Q429),
	Q407	= (landed -> Q430),
	Q408	= (go[125] -> Q451),
	Q409	= (go[123] -> Q456),
	Q410	= (go[125] -> Q432
		  |go[137] -> Q453),
	Q411	= (takeoff -> Q431),
	Q412	= (arrived[124] -> Q409),
	Q413	= (beginUpdate -> Q326
		  |go[35] -> Q434),
	Q414	= (go[95] -> Q196),
	Q415	= (arrived[54] -> Q128),
	Q416	= (arrived[33] -> Q317),
	Q417	= (arrived[55] -> Q172
		  |stopOldSpec -> Q245),
	Q418	= (arrived[54] -> Q128
		  |reconfigure -> Q325),
	Q419	= (go[35] -> Q48
		  |go[24] -> Q70),
	Q420	= (arrived[54] -> Q128),
	Q421	= (reconfigure -> Q112
		  |arrived[33] -> Q317),
	Q422	= (arrived[42] -> Q90),
	Q423	= (arrived[109] -> Q445),
	Q424	= (stopOldSpec -> Q318),
	Q425	= (go[119] -> Q474),
	Q426	= (arrived[33] -> Q317),
	Q427	= (arrived[133] -> Q449),
	Q428	= (arrived[36] -> Q219),
	Q429	= (beginUpdate -> Q94
		  |arrived[55] -> Q448),
	Q430	= (battery_charged -> Q450),
	Q431	= (takeoff_ended -> Q317),
	Q432	= (arrived[125] -> Q480),
	Q433	= (arrived[138] -> Q410),
	Q434	= (beginUpdate -> Q308
		  |arrived[35] -> Q457),
	Q435	= (arrived[93] -> Q461),
	Q436	= (reconfigure -> Q341
		  |arrived[94] -> Q460),
	Q437	= (arrived[93] -> Q461),
	Q438	= (arrived[32] -> Q7
		  |stopOldSpec -> Q347),
	Q439	= (arrived[22] -> Q464),
	Q440	= (arrived[82] -> Q468),
	Q441	= (beginUpdate -> Q469
		  |arrived[82] -> Q490),
	Q442	= (arrived[81] -> Q414),
	Q443	= (go[122] -> Q463
		  |go[108] -> Q471
		  |go[95] -> Q486
		  |go[110] -> Q496),
	Q444	= (arrived[22] -> Q464),
	Q445	= (go[122] -> Q463
		  |go[110] -> Q496),
	Q446	= (go[42] -> Q422),
	Q447	= (go[134] -> Q10
		  |go[143] -> Q363
		  |go[120] -> Q475
		  |go[132] -> Q501),
	Q448	= (beginUpdate -> Q172
		  |go[68] -> Q476),
	Q449	= (go[134] -> Q11
		  |go[120] -> Q477),
	Q450	= (initial_config -> Q481),
	Q451	= (arrived[125] -> Q478),
	Q452	= (reconfigure -> Q332
		  |arrived[32] -> Q482),
	Q453	= (arrived[137] -> Q358),
	Q454	= (arrived[32] -> Q482),
	Q455	= (go[93] -> Q20
		  |reconfigure -> Q365
		  |go[95] -> Q458
		  |go[108] -> Q483),
	Q456	= (arrived[123] -> Q98),
	Q457	= (beginUpdate -> Q248
		  |go[36] -> Q484),
	Q458	= (arrived[95] -> Q366
		  |reconfigure -> Q368),
	Q459	= (go[94] -> Q19
		  |go[92] -> Q41
		  |go[107] -> Q485),
	Q460	= (reconfigure -> Q365),
	Q461	= (go[94] -> Q19
		  |go[107] -> Q485),
	Q462	= (go[66] -> Q309),
	Q463	= (arrived[122] -> Q149),
	Q464	= (go[32] -> Q106),
	Q465	= (reconfigure -> Q371
		  |arrived[43] -> Q489),
	Q466	= (arrived[43] -> Q489),
	Q467	= (arrived[66] -> Q304),
	Q468	= (go[96] -> Q132),
	Q469	= (stopOldSpec -> Q25
		  |arrived[82] -> Q121),
	Q470	= (reconfigure -> Q374),
	Q471	= (arrived[108] -> Q377),
	Q472	= (reconfigure -> Q191
		  |arrived[43] -> Q489),
	Q473	= (arrived[43] -> Q489),
	Q474	= (arrived[119] -> Q354),
	Q475	= (arrived[120] -> Q292),
	Q476	= (beginUpdate -> Q72
		  |arrived[68] -> Q502),
	Q477	= (arrived[120] -> Q292),
	Q478	= (go[126] -> Q14
		  |go[124] -> Q412
		  |go[138] -> Q433),
	Q479	= (go[31] -> Q101
		  |reconfigure -> Q215
		  |go[33] -> Q426
		  |go[22] -> Q444
		  |go[43] -> Q473
		  |land -> Q504),
	Q480	= (go[124] -> Q412),
	Q481	= (takeoff -> Q503),
	Q482	= (go[43] -> Q473),
	Q483	= (reconfigure -> Q391
		  |arrived[108] -> Q470),
	Q484	= (beginUpdate -> Q233
		  |arrived[36] -> Q505),
	Q485	= (arrived[107] -> Q329),
	Q486	= (arrived[95] -> Q282),
	Q487	= (arrived[96] -> Q303
		  |reconfigure -> Q393),
	Q488	= (go[32] -> Q108
		  |go[55] -> Q126
		  |go[42] -> Q143
		  |reconfigure -> Q395
		  |land -> Q507),
	Q489	= (go[55] -> Q126),
	Q490	= (go[68] -> Q3
		  |beginUpdate -> Q121),
	Q491	= (arrived[43] -> Q395),
	Q492	= (arrived[68] -> Q129
		  |reconfigure -> Q398),
	Q493	= (arrived[68] -> Q129),
	Q494	= (arrived[34] -> Q178
		  |reconfigure -> Q380),
	Q495	= (stopOldSpec -> Q421
		  |arrived[33] -> Q424),
	Q496	= (arrived[110] -> Q5),
	Q497	= (stopOldSpec -> Q355
		  |takeoff_ended -> Q403),
	Q498	= (arrived[34] -> Q178),
	Q499	= (go[55] -> Q313),
	Q500	= (go[32] -> Q332),
	Q501	= (arrived[132] -> Q382),
	Q502	= (go[67] -> Q9
		  |beginUpdate -> Q29
		  |go[55] -> Q400
		  |go[82] -> Q441),
	Q503	= (takeoff_ended -> Q385),
	Q504	= (landed -> Q15),
	Q505	= (go[47] -> Q18
		  |beginUpdate -> Q210
		  |go[35] -> Q217),
	Q506	= (arrived[121] -> Q212),
	Q507	= (landed -> Q22).
