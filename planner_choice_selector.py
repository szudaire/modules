# -*- coding: utf-8 -*-
from MAVProxy.modules.planner_module import Module
from MAVProxy.modules.lib.ANUGA import lat_long_UTM_conversion as llutm
from MAVProxy.modules import planner_util_message as mesg
from multiprocessing import Array,Value
import time
import numpy as np
from os.path import expanduser
from random import randint
import os

class ChoiceSelector(Module):
    def init(self):
        self._process_type = 1
	self.controllables = []

	self._set_suscription_packets(["ATTITUDE","GLOBAL_POSITION_INT"])

	self.state = Value('i',0)
        self._control_list = None
        self._evaluating = Value('i',0)
        self._stop = Value('i',0)
        self._min_mean = Value('d',-1)

        self._dir_plan = "/home/debian/powerdevs-master/examples/patrullaje-x3_PD30/"
        if self._mission.startswith("SIM_PATROL"):
            self._dir_plan += "plan-patrullaje"
        elif self._mission.startswith("SIM_DELIVERY"):
            self._dir_plan += "plan-delivery"
        elif self._mission.startswith("SIM_FOLLOW"):
            self._dir_plan += "plan-follow"
        self._dir_plan += "/plan.txt"

        self._mode = 2 #0: premade plan, 1: random choice, 2: simulate plans
        self._model_time = 900
        self._criteria = 3 #0: number of rounds, 1: level of battery, 2: simu_time (until visit_all), 3: average simu_time (with statistics)
        self._aux_state = Value('i',0)
        self._first_time = Value('i',0)

        self._last_report = Value('d',0)

    def _started(self):
        if self._mode == 2:
            self._last_report.value = time.time()
            self._add_to_command_queue("evaluate_current_plan")

    def _evaluate_current_plan(self):
        self._add_to_message_queue(mesg.PRINT,"Evaluating plan...")

        time.sleep(0.25)

        self._evaluating.value = 1
        self._min_mean.value = -1
        self._current_list[:] = self._option_list[:]

        home_utm = self._sharedmem.get_home_utm()
        position = self._sharedmem.get_position()
        (ZoneNumber, UTMEasting, UTMNorthing) = llutm.LLtoUTM(position[1],position[0])
        loc0 = self._sharedmem.get_discretizer().get_position(0)
        (ZoneNumber, UTMEasting0, UTMNorthing0) = llutm.LLtoUTM(loc0[1],loc0[0])

        
        yaw = self._sharedmem.get_yaw()
        budget = self._sharedmem.get_budget()
        active_alarm = self._sharedmem.get_battery_warning()
        batt_consumed = self._sharedmem.get_total_dist()
        batt_consumed_event = batt_consumed
        wind_spd,wind_ang = self._sharedmem.get_wind_data()
        self._aux_state.value = self.state.value

        turn_radius = 50
        #translate batt values to distance for powerdevs
        airspeed = 21.0 #We put airspeed of Ardupilot plane
        if self._mission.startswith("SIM_PATROL"):
            turn_radius = 85
            batt_consumed = batt_consumed*airspeed
            batt_consumed_event = batt_consumed_event*airspeed
            budget = budget*airspeed
            x0 = UTMEasting-home_utm[0]
            y0 = UTMNorthing-home_utm[1]
        else:
            x0 = UTMEasting - UTMEasting0 + 50
            y0 = UTMNorthing - UTMNorthing0 + 50
            self.x0 = x0
            self.y0 = y0
            self.yaw0 = yaw
            
        batt_threshold = int((1-self._sharedmem.batt_thresh.value) * budget)
        prob_c1 = self._sharedmem.prob_c1.value
        perc_hi = self._sharedmem.perc_hi.value
        perc_med = self._sharedmem.perc_med.value
        high_dist_threshold = self._sharedmem.high_dist_threshold.value
        low_dist_threshold = self._sharedmem.low_dist_threshold.value
        
        self.target_time = time.time() - self._sharedmem.update_time.value
        self._add_to_message_queue(mesg.PRINT,"Target time: "+str(self.target_time))

        model_time = self._model_time
        #Create param file once per simulation batch
        create_param_file(model_time,yaw,x0,y0,budget,active_alarm,batt_consumed_event,batt_consumed,wind_spd,wind_ang,turn_radius,
                          batt_threshold,prob_c1,perc_hi,perc_med,high_dist_threshold,
                          low_dist_threshold)

        #time.sleep(10000)

        if self._first_time.value == 0:
            self._add_to_command_queue("simulate_plan")
            self._first_time.value = 1

    def check_plan_change(self):
        self._sharedmem.num_change_choices.value = self._sharedmem.num_change_choices.value + 1
        #self._add_to_message_queue(mesg.PRINT,"Updated CHOICE: "+str(self._sharedmem.num_change_choices.value))
                                        
    def _simulate_plan(self):
        #self._add_to_message_queue(mesg.PRINT,"Simulating plan...")
        
        txtfile = open("/home/debian/powerdevs-master/examples/patrullaje-x3_PD30/params/real_flight_semivalidation_initial_conditions_params.ini","r")
        lines = txtfile.readlines()
        txtfile.close()
        #print(lines[0:6])
        #print("State: "+str(self._aux_state.value))
        
        model_time = self._model_time

        self.print_to_file(self._dir_plan,ctrl_list=self._current_list[:],init_state=self._aux_state.value)
        #self._add_to_message_queue(mesg.PRINT,"Started sim...")
        total_simu_time = 0
        cant_simu = 15
        simu_time = 0
        t1 = time.time()
        for i in range(cant_simu):
            if self._mission.startswith("SIM_FOLLOW"):
                x0target = self._sharedmem.x0target.value
                y0target = self._sharedmem.y0target.value
                yaw0target = self._sharedmem.yaw0target.value
                if self.target_time < 5 and 1==0: #seconds
                    os.system("cd /home/debian/powerdevs-master/output/ && ./target -tf "+str(self.target_time)+" -print_model_params -x0target="+str(x0target)+" -y0target="+str(y0target)+" -yaw0target="+str(yaw0target)+" -seed1 "+str(randint(0,20000))+ " -seed2 "+str(randint(0,20000)))
                    x0target, y0target, yaw0target = get_summary_target()
                os.system("cd /home/debian/powerdevs-master/output/ && ./model -tf "+str(model_time)+" -c ../examples/patrullaje-x3_PD30/params/real_flight_semivalidation_initial_conditions_params.ini -print_model_params -seed1 "+str(randint(0,20000))+ " -seed2 "+str(randint(0,20000))+ " -x0="+str(self.x0)+" -y0="+str(self.y0)+" -yaw0="+str(self.yaw0)+" -x0target="+str(x0target)+" -y0target="+str(y0target)+" -yaw0target="+str(yaw0target))
            else:
                os.system("cd /home/debian/powerdevs-master/output/ && ./model -tf "+str(model_time)+" -c ../examples/patrullaje-x3_PD30/params/real_flight_semivalidation_initial_conditions_params.ini -print_model_params -seed "+str(randint(0,200000000000)))#  -debugLevel -1  > ./out.txt
                
            if self._mission.startswith("SIM_PATROL"):
                mean, rounds, simu_time, batt = get_summary_log_patrol()
                break
            elif self._mission.startswith("SIM_DELIVERY"):
                _, _, simu_time, _ = get_summary_log_patrol()
                total_simu_time += simu_time
            elif self._mission.startswith("SIM_FOLLOW"):
                dist = get_summary_follow(cant=1)
                total_simu_time += dist
            
            if self._stop.value == 1:
                break
                
        t2 = time.time()
        #time.sleep((t2-t1)*(10-1))
        self._add_to_message_queue(mesg.PRINT,"Ended sim..."+str(i) + " time: "+str(t2-t1))
        
        #print("finished")SIM_PATROL
        #self._add_to_message_queue(mesg.PRINT,"Calculated rounds: "+str(rounds))
        
        if self._stop.value != 1:
            if self._criteria == 0:
                self._add_to_message_queue(mesg.PRINT,"Calculated rounds: "+str(rounds))
                if self._min_mean.value < 0 or rounds > self._min_mean.value:
                    self._min_mean.value = rounds
                    self._option_list[:] = self._current_list[:]
                    self.check_plan_change()
            elif self._criteria == 1:
                self._add_to_message_queue(mesg.PRINT,"Calculated time elapsed: "+str(self._sharedmem.get_budget() - batt/21.0))
                if self._min_mean.value < 0 or batt > self._min_mean.value:
                    self._min_mean.value = batt
                    self._option_list[:] = self._current_list[:]
                    self.check_plan_change()
            elif self._criteria == 2:
                self._add_to_message_queue(mesg.PRINT,"Calculated simu_time: "+str(simu_time))
                if self._min_mean.value < 0 or simu_time < self._min_mean.value:
                    self._min_mean.value = simu_time
                    self._option_list[:] = self._current_list[:]
                    self.check_plan_change()
            elif self._criteria == 3:
                self._add_to_message_queue(mesg.PRINT,"Average simu_time: "+str(total_simu_time/cant_simu))
                if self._min_mean.value < 0 or total_simu_time < self._min_mean.value:
                    self._min_mean.value = total_simu_time
                    self._option_list[:] = self._current_list[:]
                    self.check_plan_change()
                    self._add_to_message_queue(mesg.PRINT,"Best time: "+str(self._min_mean.value))

        if simu_time > model_time+1:
            os.system("cp /home/debian/powerdevs-master/examples/patrullaje-x3_PD30/params/real_flight_semivalidation_initial_conditions_params.ini /home/debian/powerdevs-master/examples/patrullaje-x3_PD30/params/temp.ini")
            os.system("cp /home/debian/powerdevs-master/examples/patrullaje-x3_PD30/plan-patrullaje/plan.txt /home/debian/powerdevs-master/examples/patrullaje-x3_PD30/plan-patrullaje/plan_temp.txt")
            #import sys
            #sys.exit(0)
            print("Paso")
            time.sleep(10000)
            #return

        self._last_report.value = time.time()
        
        if self._stop.value == 1:
            #Stop evaluating
            self._stop.value = 0
            self._evaluating.value = 0
        #Evaluate next one random
        else:
            random_list,ctrl_list = self.random_list()
            self._current_list[:] = random_list
            #Me aseguro que tome la misma accion controlable que se tomo
            self._current_list[self.state.value] = self._option_list[self.state.value]

            time.sleep(0.1)#To give time for other threads to catch up
            self._add_to_command_queue("simulate_plan")

    def add_locations(self,pos_locations):
        if self._sharedmem.get_no_disc():
            pos_loc = []
            for i in range(len(pos_locations)):
                if i%2 == 1:
                    continue
                pos_loc.append(pos_locations[i])
            pos_locations = np.array(pos_loc)
        
        pos_utm = []
        home_utm = self._sharedmem.get_home_utm()
        for pos in pos_locations:
            (ZoneNumber, UTMEasting, UTMNorthing) = llutm.LLtoUTM(pos[1],pos[0])
            pos_utm.append([UTMEasting-home_utm[0],UTMNorthing-home_utm[1]])
        create_location_file(pos_utm)

    def set_plan(self,states):
        self._states = states

        random_list,ctrl_list = self.random_list()
        if self._mode == 0:
            option_list = retreive_option_list(self._dir_plan,states,ctrl_list)
        else:
            option_list = random_list
        
        self._option_list = Array('i',option_list)
        self._current_list = Array('i',option_list)
        
        if self._mode == 1 or self._mode == 2:
            self.print_to_file(self._dir_plan,ctrl_list=self._option_list[:])
	
    def select_controllable(self,list_contr,state):
        if self._mission.startswith("SIM_"):
            # Commented out the waiting since it should work ok without waiting
            #if self._mode == 2:
                #self._stop.value = 1
                #while self._evaluating.value != 0:
                    #self._add_to_message_queue(mesg.PRINT,"Waiting for sims to stop")
                    #time.sleep(0.5)

            if self._mode == 1:
                index = randint(0,len(list_contr)-1)
            else:
                index = self._option_list[state]
                
            self.state.value = state
            self._add_to_message_queue(mesg.PRINT,"Selected "+list_contr[index]+" controllable")

            if self._mode == 2:
                self._add_to_command_queue("evaluate_current_plan")

                timeout = 50
                if time.time() - self._last_report.value > timeout:
                    os.system("cp /home/debian/powerdevs-master/examples/patrullaje-x3_PD30/params/real_flight_semivalidation_initial_conditions_params.ini /home/debian/powerdevs-master/examples/patrullaje-x3_PD30/params/temp.ini")
                    ferror = open("/home/debian/Documents/ardupilot/ArduPlane/error.txt",'w')
                    ferror.write("stopping simu because of error in timeout")
                    ferror.close()
                    self._add_to_event_queue('stop_simu') #Provoke automata error and RTL engage
                    
        else:
            self._add_to_message_queue(mesg.PRINT,"Select from "+str(len(list_contr))+" controllables")
            index = 0
        return index

    def get_control_list(self):
        ctrl_list = []
        for state in self._states:
            num_ctrl = 0
            for elem in state:
                if elem[1] == "C":
                    num_ctrl += 1

            ctrl_list.append(num_ctrl)
        return ctrl_list

    def random_list(self):
        if self._control_list == None:
            self._control_list = self.get_control_list()
            
        ctrl_list = self._control_list
        random_list = []
        for elem in ctrl_list:
            if elem > 0:
                random_list.append(randint(0,elem-1))
            else:
                random_list.append(0)
        return random_list,ctrl_list

    def print_to_file(self,filename,ctrl_list=[],init_state=0):
        states = self._states
        f = open(filename,'w')

        f.write(str(len(states)) + "\n")
        f.write(str(init_state) + "\n")
        ind = 0
        for state in states:
            cant = 0
            count_ctrl = -1
            txt = ""
            has_ctrl = False
            for event in state:
                if event[1] == "C":
                    count_ctrl += 1
                    if (len(ctrl_list) > 0):
                        if (count_ctrl != ctrl_list[ind]):
                            continue
                    cant += 1
                    has_ctrl = True
                    event_type = "C"
                else:
                    event_type = "U"
                    cant += 1
                    
                txt += " " + event[0] + " " + event_type + " " + str(event[2])

            if has_ctrl:
                begin = "C"
            else:
                begin = "U"

            txt = begin + " " + str(cant) + txt
            f.write(txt + "\n")
            ind += 1
        
        f.close()

def retreive_option_list(filename,states,ctrl_list):
    plan = open(filename)
    lines = plan.readlines()[2:]
    plan.close()

    orig_list = [0]*len(ctrl_list)

    for i in range(len(lines)):
        line = lines[i][:-1]
        values = line.split(' ')
        if values[0] == "U":
            orig_list[i] = ctrl_list[i]
            continue

        event = values[2]
        ev_type = values[3] #C or U
        next_state = values[4]

        options = states[i]
        for j in range(len(options)):
            if options[j][0] == event:
                break

        orig_list[i] = j

    return orig_list

def create_location_file(pos_utm):
    txt = ""
    for i in range(len(pos_utm)):
        pos = pos_utm[i]
        txt += str(pos[0])+","+str(pos[1])+","+str(pos[0])+","+str(pos[1])+",20,"+str(i)+"\n"

    txtfile = open("/home/debian/powerdevs-master/examples/patrullaje-x3_PD30/data/locations-patrullaje-x3.csv","w")
    txtfile.write(txt)
    txtfile.close()

def get_summary_follow(cant = 5):
    distances = []
    txtfile = open("/home/debian/powerdevs-master/examples/patrullaje-x3_PD30/summary.log","r")
    lines = txtfile.readlines()
    txtfile.close()
    for i in range(len(lines)):
        line = lines[i]

        if i>=1:
            values = line.split(",")
            values = (values[2]).split(':')
            name = values[0][1:]
            val = values[1][1:-1]
            if name == "distance":
                distances.append(float(val))

    dist = np.sum(distances[-cant:]) #last cant
            
    return dist

def get_summary_log_patrol():
    mean = -1
    rounds = -1
    batt = -1
    simu_time = -1
    txtfile = open("/home/debian/powerdevs-master/examples/patrullaje-x3_PD30/summary.log","r")
    lines = txtfile.readlines()
    txtfile.close()
    for i in range(len(lines)):
        line = lines[i]
        
        values = line.split(",")
        if i==0:
            if len(values) > 1:
                vals = (values[1][:-1]).split(":")
                mean = float(vals[1])

                vals = (values[0][:-1]).split(":")
                rounds = float(vals[1])
        elif i==1:
            vals = (values[0][:-1]).split(":")
            vals2 = (vals[1]).split('s')
            simu_time = float(vals2[0])
        elif i==2:
            vals = (values[0][:-1]).split(":")
            batt = float(vals[1])
        else:
            pass
    return mean, rounds, simu_time, batt

def get_summary_target():
    txtfile = open("/home/debian/powerdevs-master/examples/patrullaje-x3_PD30/summary_target.log","r")
    lines = txtfile.readlines()
    txtfile.close()
    for i in range(len(lines)):
        line = lines[i]
        
        values = line.split(":")
        if i==1:
            x0target = float(values[1][1:-1])
        elif i==2:
            y0target = float(values[1][1:-1])
        elif i==3:
            yaw0target = float(values[1][1:-1])
            
    return x0target, y0target, yaw0target


def create_param_file(model_time,yaw,x0,y0,budget,active_alarm,batt_consumed_event,batt_consumed,
                      wind_spd,wind_ang,turn_radius,batt_threshold,prob_c1,perc_hi,perc_med,high_dist_threshold,
                      low_dist_threshold):
    txt = "hdisp=0.001\n"
    txt += "battery_budget="+str(int(budget))+"\n"
    txt += "active_alarm="+str(active_alarm)+"\n"
    txt += "batt_consumed="+str(batt_consumed)+"\n"
    txt += "batt_consumed_event="+str(batt_consumed_event)+"\n"
    txt += "horizon="+str(model_time)+"\n"
    txt += "turn_radius="+str(turn_radius)+"\n"
    txt += "init_bat="+str(batt_consumed)+"\n"
    txt += "battery_duration="+str(int(budget))+"\n"
    txt += "batt_threshold="+str(batt_threshold)+"\n"
    txt += "prob_c1="+str(prob_c1)+"\n"
    txt += "perc_hi="+str(perc_hi)+"\n"
    txt += "perc_med="+str(perc_med)+"\n"
    txt += "high_dist_threshold="+str(high_dist_threshold)+"\n"
    txt += "low_dist_threshold="+str(low_dist_threshold)+"\n"
    txt += '''g=9.81
Ix=0.05632
Iy=0.09294
Iz=0.14441
Ixy=0
Ixz=0.00233
Iyz=0
Jxz=-0.00233
S=0.32
qinf=138
m=1.4
Weight=13.73399999999999998579
bb=1.6
cc=0.2
P=13.73399999999999998579
Ie=0
fwe_cte=0.000678
we=100.63954781332633103830
dotwe=0
gravity=9.81
SCHED_DEFAULT_LOOP_RATE=50
SCHED_DEFAULT_LOOP_RATE_OVER_10=5
SAMP_TIME=0.02000000000000000042
SAMP_TIME_INV=50
SAMP_TIME_INV_NEG=-50
RLL2SRV_P=1.701627
RLL2SRV_I=0.1350498
RLL2SRV_D=0.127622
RLL2SRV_TCONST=0.45
RLL2SRV_TCONST_INV=2.22222222222222232091
RLL2SRV_P_GAIN=0.61076256549999996537
RLL2SRV_RMAX=1.30899693899574720568
RLL2SRV_RMIN=-1.30899693899574720568
YAW2SRV_SLIP=0
YAW2SRV_RLL=1
YAW2SRV_INT=0
YAW2SRV_DAMP=0
HI_PASS_FILTER_K1=0.98039215686274505668
HI_PASS_FILTER_K2=-0.98039215686274505668
PTCH2SRV_P=0.8040572
PTCH2SRV_I=0.06700478
PTCH2SRV_D=0.0603043
PTCH2SRV_RLL=0
PTCH2SRV_TCONST=0.45
PTCH2SRV_TCONST_INV=2.22222222222222232091
PTCH2SRV_P_GAIN=0.28795297205000003338
PTCH2SRV_I_GAIN=0.03015215100000000203
PTCH2SRV_RMAX_DN=-1.30899693899574720568
PTCH2SRV_RMAX_UP=1.30899693899574720568
LIM_ROLL_CD=1.13446401379631423012
SCALING_SPEED=15
u_lim=0.90630778703664993667
l_lim=-0.90630778703664993667
KuP=5
KuI=0.5
bias_u=88
U_SP=17
KhP=0.05
KhI=0.01
H_SP=1055
TECS_CLIMB_MAX=5
TECS_CLIMB_MIN=-5
pitch0=0.08239812841106279595
roll0=-0.08310444634599448155
'''
    txt += "yaw0="+str(np.deg2rad(yaw))+"\n"
    txt += "x0="+str(x0)+"\n"
    txt += "y0="+str(y0)+"\n"
    txt += '''h0=1058.9
airspeed0=21.0
u0=21.0
v0=0
w0=0.0
p0=0.03205190593233794155
q0=0.21337141602869919010
r0=-0.04855541196281282917
Cx=0
Cy=0
Cz=0
Cl=0
Cm=0
Cn=0
ANTIWDUP_K1=0.00497512437810945351
ANTIWDUP_K2=0.99502487562189068093
lataccliminf=-0.90630778703664993667
latacclimsup=0.90630778703664993667
roll_filter_k1=0.03846153846153846367
roll_filter_k2=0.96153846153846145306
NAVL1_P=16.0
NAVL1_D=0.75
summaryfolder="../examples/patrullaje-x3_PD30/"
summaryfile="summmary"
'''
    txt += "windspeed="+str(wind_spd)+"\n"
    txt += "gamma="+str(wind_ang+np.pi)+"\n"
    txt += "tau=0\n"
    txtfile = open("/home/debian/powerdevs-master/examples/patrullaje-x3_PD30/params/real_flight_semivalidation_initial_conditions_params.ini",'w')
    txtfile.write(txt)
    txtfile.close()
    

