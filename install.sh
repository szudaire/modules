#!/bin/bash
rm ~/.cache/mavproxy/*.txt
rm ~/.cache/mavproxy/lib/*.txt
rm ~/.cache/mavproxy/logs/*.txt
rm ~/.cache/mavproxy/automatas/*.txt
mkdir ~/.cache/mavproxy
cp ./config_file.py ~/.cache/mavproxy/config_file.py
mkdir ~/.cache/mavproxy/cache
mkdir ~/.cache/mavproxy/captures/
mkdir ~/.cache/mavproxy/lib
mkdir ~/.cache/mavproxy/logs
mkdir ~/.cache/mavproxy/automatas
mkdir ~/.cache/mavproxy/code
cp -r ./Automatas/* ~/.cache/mavproxy/automatas/
gcc -fpic -shared -o planner_clib_trajectory.so planner_clib_trajectory.c
gcc -fpic -shared -o planner_clib_poly.so planner_clib_poly.c
g++ -fpic -shared -o planner_clib_rrtsolver.so planner_clib_rrtsolver.cpp
cp ./planner_clib_rrtsolver.so ~/.cache/mavproxy/lib/planner_clib_rrtsolver.so
cp ./planner_clib_trajectory.so ~/.cache/mavproxy/lib/planner_clib_trajectory.so
cp ./planner_gpu_trajectory.cu ~/.cache/mavproxy/lib/planner_gpu_trajectory.cu
cp ./planner_clib_poly.so ~/.cache/mavproxy/lib/planner_clib_poly.so

cd ../../
python setup.py build install --user
