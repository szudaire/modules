from MAVProxy.modules.planner_module import Module
from MAVProxy.modules import planner_util_message as mesg
from multiprocessing import Array,Value
import cv2 as cv
import time
import numpy as np
from os.path import expanduser
import numpy as np

class CSensor(Module):
	def init(self):
		self._process_type = 0
		self._list = [360]
		self._start_time = -1.0
		self._started = False
		self._ended = False
		self._counter = 0

		self._radius = 0.003
		return

	def has_C_next(self):
                next_pos = self._sharedmem.get_next_location()
                loc_pos = self._sharedmem.get_discretizer().get_position(next_pos)

                for elem in self._list:
                        dist = self._sharedmem.get_discretizer().get_position(elem) - loc_pos
                        dist = dist * self._sharedmem.get_scale()
                        if (np.linalg.norm(dist,2) < self._radius):
                                return 'yes_C_next'

                return 'no_C_next'
