#!/usr/bin/env python
'''
Example Module
Peter barker, September 2016

This module simply serves as a starting point for your own MAVProxy module.

1. copy this module sidewise (e.g. "cp mavproxy_example.py mavproxy_coolfeature.py"
2. replace all instances of "example" with whatever your module should be called
(e.g. "coolfeature")

3. trim (or comment) out any functionality you do not need
'''

import os
import os.path
import numpy as np
import time
import socket
import math
from MAVProxy.modules.lib import mp_util
if mp_util.has_wxpython:
    from MAVProxy.modules.lib.mp_menu import *

from MAVProxy.modules.lib.mp_module import MPModule
#from MAVProxy.modules.lib import mp_util
#from MAVProxy.modules.lib import mp_settings
from MAVProxy.modules.lib.textconsole import SimpleConsole
from MAVProxy.modules import planner_util_message as mesg
from MAVProxy.modules.planner_colors import pcolors
from MAVProxy.modules.mavproxy_map import mp_slipmap
from MAVProxy.modules import planner_util_sharedmem
from MAVProxy.modules import planner_util_codegen
from MAVProxy.modules.lib.ANUGA import lat_long_UTM_conversion as llutm
from random import random

class planner_viewer(MPModule):
    def __init__(self, mpstate):
        """Initialise module"""
        super(planner_viewer, self).__init__(mpstate, "planner_viewer", "")
        self.add_command('pstart', self.cmd_start, "start mission")
        self.add_command('pping', self.cmd_ping, "Ping onboard computer")
        self.add_command('pheight', self.cmd_height, "Set flight height for the planner")
        self.add_command('pcoverrad', self.cmd_cover_radius, "Set cover radius for the planner")
        self.add_command('psort', self.cmd_sort, "Set sorting algorithm for the planner")
        self.add_command('pmotion', self.cmd_motion, "Set motion planner for the planner")
        self.add_command('pgrid', self.cmd_grid, "Set grid size for the planner")
        self.add_command('pradius', self.cmd_radius, "Set turn radius for the planner")
        self.add_command('pangle', self.cmd_angle, "Set angle for the planner")
        self.add_command('pexit', self.cmd_exit, "Exit planner")
        self.add_command('pmission', self.cmd_load_mission, "Load mission from folder")
        self.add_command('pvideo', self.cmd_video, "start video")
        self.add_command('psleep', self.cmd_sleep, "sleep command consumer")
        self.add_command('pquit', self.cmd_quit, "Exit planner_viewer")
        self.add_command('pinit', self.cmd_init, "Init planner")
        self.add_command('pmodel', self.cmd_model, "Get discretization model")
        self.add_command('pupmodel', self.cmd_update_model, "Get update automata model")
        self.add_command('pregion', self.cmd_region, "Add selected region to model")
        self.add_command('pnofly', self.cmd_nofly, "Add regions as a no-fly regions")
        self.add_command('pplan', self.cmd_load_plan, "Load automata of the plan into the UAV")
        self.add_command('pupplan', self.cmd_update_plan, "Load update automata into the UAV")
        self.add_command('photswap', self.cmd_hotswap, "Hot swap update automata")
        self.add_command('pfence',self.cmd_fence, "Draw and save fence")
        self.add_command('psetfire',self.cmd_set_fire, "Set current fence as fire")
        self.add_command('plreg',self.cmd_region_load, "Load fence from file")
        self.add_command('psensor',self.cmd_region_sensor, "Create region sensor")
        self.add_command('ploadcode',self.cmd_load_code,"Load custom code to planner")
        self.add_command('punload',self.cmd_unload_code,"Unload running module from the planner")
        self.add_command('prreg',self.cmd_random_region,"Create random region")
        self.add_command('ptakeoff',self.cmd_takeoff,"Takeoff Plane UAV")

        self.menu_added_map = False
        if mp_util.has_wxpython:
            self.menu = MPMenuSubMenu('PFence',
                                  items=[MPMenuItem('Draw', 'Draw', '# pfence')])
    
        self.mpstate.functions.process_stdin("set moddebug 3", immediate=True)
        self._txt_console = SimpleConsole()
        self._started = False
        self._plan_started = False
        
        self.wp_file = "wp.txt"
        self.grid_file = "grid"
        
        self._grid = False
        self.prev = time.time()

        self._init_time = time.time()
        self._config_time = 5
        self._configured = False

        self._grid_num = 0
        self._no_grid = False

        self._cant_grid = -1
        self._disable_colors = False

        self._beeped_time = time.time()
        self._beeped_period = 1.0
        self._beeped_max = 1
        self._beeped_cant = 0

        self._prev_time = time.time()
        self._sleep_time = 0
        self._init_cmd_file()

        self._quit_time = time.time()
        self._quit_delay = 5
        self._quit_flag = False
        self._wait_for_init = False
        self._setup_done = False
        
        self._set_sharedmem()
        self._set_locations = False
        self._set_automata = False
        self._color_ind = 0
        self._region_ind = 0

        self._expected_queue = []
        self._automata_queue = []
        self._hotswap_ready = False
        self._utm = False
        self.identifier = 'PLANNER_VIEWER'

        self._border = []
        self._border_max = -1

        self._show_label = True
        self._no_disc = False

    def _set_sharedmem(self):
        self._sharedmem = planner_util_sharedmem.SharedMem()

    def cmd_takeoff(self,args):
        mesg.send_message(self,mesg.TAKEOFF,"")

    def cmd_set_fire(self,args):
        fence_points = self._read_fence(label="FIRE")
        self._save_fence(fence_points,fence_file="FIRE")
        elements = self._sharedmem.get_discretizer().get_locations_from_area(fence_points)
        self._fire_elems = elements
        mesg.send_message(self,mesg.FIRE_ELEMS,"")
        self._expected_queue.append("fire_elems")

    def _init_cmd_file(self):
        home = os.path.expanduser("~")
        cmd_file = home + '/.cache/mavproxy/cmd_file.txt'
        if not os.path.exists(cmd_file):
            self._exists_cmd_list = False
            return
        
        file = open(cmd_file,'r')
        self._cmd_list = file.readlines()
        file.close()
        
        self._exists_cmd_list = True
    
    def cmd_fence(self,args):
        if not 'draw_lines' in self.mpstate.map_functions:
            self._print_ln(pcolors.color("FAIL: No map drawing available",'red'))
            return
        self._print_ln(pcolors.color("Please draw the shape of the fence",'green'))
        self.mpstate.map_functions['draw_lines'](self.fence_draw_callback)
        
    def fence_draw_callback(self,points):
        if (len(points) < 3):
            self._print_ln(pcolors.color("FAIL: Too few points",'red'))
            return
        points.append(points[0])
        self._show_tmp_fence(points)
        self._save_fence(points)
        self._print_ln(pcolors.color("Fenced saved to fence.txt",'green'))
        return 

    def cmd_load_code(self,args):
        if len(args) > 0:
            filename = str(args[0])
            
            if not os.path.exists(filename):
                self._print_ln(pcolors.color("FAIL: File " + filename + " doesn't exist",'red'))
                return
            file_obj = open(filename,'r')
            data = file_obj.read()
            file_obj.close()
            self._code = data
            mesg.send_message(self,mesg.CODE,filename)
            self._expected_queue.append("code")
        else:
            self._print_ln(pcolors.color("FAIL: No code with that name",'red'))

    def cmd_nofly(self,args):
        if len(args) == 0:
            self._print_ln(pcolors.color("FAIL: Specify regions to add as nofly",'red'))
        else:
            points = np.array([],np.int)
            polygons = []
            for arg in args:
                region_name = str(arg)
                regions = self._sharedmem.get_discretizer().regions
                if region_name in regions.keys():
                    points = np.append(points,regions[region_name])
                    polygons.append(self._sharedmem.get_discretizer().regions_pol[region_name])
                else:
                    self._print_ln(pcolors.color("FAIL: No region with name " + arg,'red'))
                    return
            
            filename = "planner_SafeSensor.py"
            file_obj = open(filename,'w')
            file_obj.write(planner_util_codegen.create_safe_sensor_from_points(points,polygons))
            file_obj.close()
            self._print_ln(pcolors.color("SUCCESS: Safe Sensor region generated to "+filename,'yellow'))

            self.cmd_load_code(["planner_SafeSensor.py"])

    def calc_pos_moved_meters(self,position,x,y):
        (ZoneNumber, UTMEasting, UTMNorthing) = llutm.LLtoUTM(position[1],position[0])
        (Lat,Long) = llutm.UTMtoLL(UTMNorthing+y,UTMEasting+x,ZoneNumber,isSouthernHemisphere=(position[1] < 0))
        return np.array([Long,Lat],np.float)

    def cmd_random_region(self,args):
        side = 400 #meters for half the side length of square centered in home
        valid_dist = 200 #minimum distance between points 

        num_locs = 1
        if len(args) > 0:
            num_locs = int(args[0])

        valid_points = False
        while (not valid_points):
            loc_points = []
            loc_center = []
            for i in range(num_locs):
                x = random()*2*side - side
                y = random()*2*side - side
                
                position = self._sharedmem.get_position()
                center = self.calc_pos_moved_meters(position,x,y)

                df = 25 #meters
                pos1 = self.calc_pos_moved_meters(center,df,df)
                pos2 = self.calc_pos_moved_meters(center,df,-df)
                pos3 = self.calc_pos_moved_meters(center,-df,-df)
                pos4 = self.calc_pos_moved_meters(center,-df,df)
                points = [pos1,pos2,pos3,pos4]

                loc_points.append(points)
                loc_center.append([x,y])

            min_dist = -1
            for i in range(len(loc_center)):
                x1 = loc_center[i][0]
                y1 = loc_center[i][1]
                for j in range(len(loc_center)):
                    if j == i:
                        continue
                    x2 = loc_center[j][0]
                    y2 = loc_center[j][1]
                    dist = np.sqrt((x1-x2)**2 + (y1-y2)**2)
                    #print(dist)

                    if dist < min_dist or min_dist < 0:
                        min_dist = dist

            if min_dist > valid_dist:
                valid_points = True
            else:
                print("Iterating")
            
        for i in range(num_locs):
            points = loc_points[i]
            filename = "r"+str(i)+".txt"
            fence = open(filename,"w")
            for point in points:
                fence.write(str(point[0])+"\t"+str(point[1])+"\n")
            fence.write(str(points[0][0])+"\t"+str(points[0][1])+"\n")
            fence.close()

            self._print_ln(pcolors.color("Written to file " + filename,'yellow'))
            self.cmd_region_load(["r"+str(i)])
        
    def cmd_region_load(self,args):
        if len(args) > 0:
            filename = str(args[0])
            
            if not os.path.exists(filename+'.txt'):
                self._print_ln(pcolors.color("FAIL: File " + filename + ".txt doesn't exist",'red'))
                return
            
            points = self._read_fence(filename,name=filename,show=False)
            self._show_tmp_fence(points)
            self._save_fence(points)

            if(len(args)> 1 and args[1] == 'show'):
                self._show_label = False

            if filename == "REGION":
                if (len(args) > 1 and args[1] == 'utm'):
                    self.cmd_start(['utm'])
                else:
                    self.cmd_start([])
            elif filename == "FIRE":
                self.cmd_set_fire([])
            else:
                self.cmd_region([filename])
        else:
            self._print_ln(pcolors.color("FAIL: Specify a filename",'red'))

    def cmd_region_sensor(self,args):
        if len(args) > 0:
            region_name = str(args[0])
            regions = self._sharedmem.get_discretizer().regions
            if region_name in regions.keys():
                points = regions[region_name]
                filename = "planner_"+region_name+'Sensor.py'
                file_obj = open(filename,'w')
                file_obj.write(planner_util_codegen.create_sensor_from_points(points,region_name))
                file_obj.close()
                self._print_ln(pcolors.color("SUCCESS: Sensor region generated to "+filename,'yellow'))
                self.cmd_load_code([filename])
            else:
                self._print_ln(pcolors.color("FAIL: No region with that name",'red'))
        else:
            self._print_ln(pcolors.color("FAIL: Specify a region name",'red'))

    def cmd_unload_code(self,args):
        if len(args) > 0:
            modulename = str(args[0])
        else:
            self._print_ln(pcolors.color("FAIL: Please specify a module to unload",'red'))
            return

        mesg.send_message(self,mesg.UNLOAD,modulename)
        
    def cmd_load_plan(self,args,update=False):
        if len(args) > 0:
            filename = str(args[0])
        else:
            filename = "automata_data.txt"
            
        if not os.path.exists(filename):
            self._print_ln(pcolors.color("FAIL: File " + filename + " doesn't exist",'red'))
            return
        
        file = open(filename,'r')
        automata_data = file.read()
        file.close()
        self._tmp_automata_data = automata_data
        self._automata_queue.append(automata_data)

        if update:
            mesg.send_message(self,mesg.AUTOMATA,"update")
        else:
            mesg.send_message(self,mesg.AUTOMATA,"load")
        self._expected_queue.append("automata")

    def cmd_hotswap(self,args):
        if self._hotswap_ready:
            mesg.send_message(self,mesg.HOTSWAP,"")
        else:
            self._print_ln(pcolors.color("FAIL: Not waiting for a hotswap",'red'))

    def cmd_update_plan(self,args):
        if not self._plan_started:
            self._print_ln(pcolors.color("FAIL: Plan not started yet, upload plan via pplan",'red'))
        else:
            self.cmd_load_plan(args,update=True)

    def cmd_region(self,args):
        if (not self._set_locations) and (not self._no_disc):
            self._print_ln(pcolors.color("Warning: Locations not generated",'yellow'))
            return
            
        region_vec = ["A","B","C","D","E","F","G","H","I","J","K"]
        if (len(args) >= 1):
            label_name = str(args[0])
        else:
            label_name = region_vec[self._region_ind]
            self._region_ind += 1
            if (self._region_ind >= len(region_vec)):
                self._region_ind = 0

        discretizer = self._sharedmem.get_discretizer()
        if (label_name in discretizer.regions.keys()):
            self._print_ln(pcolors.color("FAIL: Name " + label_name + " already used",'red'))
            return

        fence_points = self._read_fence(label=label_name)
        self._save_fence(fence_points,fence_file=label_name)
        elements = self._sharedmem.get_discretizer().add_region(fence_points,label_name,self._no_disc)
        if len(elements) == 0:
            self._print_ln(pcolors.color("WARNING: No elements in region " + label_name,'yellow'))
        else:
            self._print_ln(pcolors.color("SUCCESS: Added " + str(len(elements)) +  " elements to region " + label_name,'yellow'))
        
    def cmd_model(self,args):
        if self._set_locations:
            model_file = "a1_model.lts"
            self.remove_file(model_file)
            position = self._sharedmem.get_discretizer().get_start_position()
            start_loc = self._sharedmem.get_discretizer().get_closest_element(position)
            model_string = self._sharedmem.get_discretizer().generate_model(start_loc)
            write_to_file(model_file,model_string)
            self._print_ln(pcolors.color("SUCCESS: Generated model in " + model_file,'yellow'))
        else:
            self._print_ln(pcolors.color("FAIL: Locations not generated",'red'))

    def cmd_update_model(self,args):
        if self._set_automata:
            if self._set_locations:
                model_file = "a1_update_model.lts"
                self.remove_file(model_file)
                position = self._sharedmem.get_discretizer().get_start_position()
                start_loc = self._sharedmem.get_discretizer().get_closest_element(position)
                model_string = self._sharedmem.get_discretizer().generate_model(start_loc,update=True,oldcontroller=self._automata_data)
                write_to_file(model_file,model_string)
                self._print_ln(pcolors.color("SUCCESS: Generated update model in " + model_file,'yellow'))
            else:
                self._print_ln(pcolors.color("FAIL: Locations not generated",'red'))
        else:
            self._print_ln(pcolors.color("FAIL: Missing a plan to update",'red'))

    def cmd_init(self,args):
        if (self._plan_started):
            self._print_ln("FAIL: Plan already started")
            return
        
        if (self._wait_for_init == True):
            mesg.send_message(self,mesg.INIT,"")
        else:
            self._print_ln("FAIL: Waiting for discretization")
            
    def cmd_motion(self,args):
        motion = str(args[0])
        if (motion == "uni"):
            mesg.send_message(self,mesg.MOTION,"0")
        elif (motion == "multi"):
            mesg.send_message(self,mesg.MOTION,"1")
        else:
            self._print_ln("FAIL: Motion Planner parameter incorrect")

    def cmd_sleep(self,args):
        try:
            self._sleep_time = float(args[0])
        except:
            self._print_ln("FAIL: Time parameter incorrect")
    
    def cmd_video(self,args):
        mesg.send_message(self,mesg.VIDEO,"")
    
    def cmd_start(self,args):
        if (self._setup_done == False):
            self._print_ln(pcolors.color("FAIL: Waiting for Initial configurations",'red'))
            return
        if (self._started == False):
            self._started = True
            
            mesg.send_message(self,mesg.GRID_QUERY,"")
            self._print_ln(pcolors.color("Waiting for GRID SIZE",'yellow'))
            #mesg.send_message(self,mesg.START,"")
            try:
                self._cant_grid = int(args[-1])
            except:
                self._print_ln("WARNING: Cant grid param not set")
                
            if (len(args) > 0):
                if args[0] == 'utm':
                    self._utm = True
        else:
            self._print_ln(pcolors.color("FAIL: Planner already started",'red'))

    def _generate_locations(self):
        if self._no_disc == False:
            label_name = "REGION"
            fence_points = self._read_fence(label=label_name)
            self._save_fence(fence_points,fence_file=label_name)
            self._sharedmem.set_scale()

        discretizer = self._sharedmem.get_discretizer()
        if (self._utm):
            utm_file = "utm_discrete.txt"
            #utm_fence_points = self._read_fence(utm=True)
            #discretizer.discretize(utm_fence_points,self._utm_grid_size,utm=True)
            #discretizer.save_discretization_to_file(utm_file)
            discretizer.load_discretization_from_file(utm_file)
            self._locations = discretizer.get_grid(fence_points,None,discretize=False)
        elif (self._no_disc == False):
            self._locations = discretizer.get_grid(fence_points,self._grid_size)
        else:
            self._locations = self._sharedmem.get_discretizer().generate_locations_nodisc()
            #print(self._locations)
        self._set_locations = True
        #START SOFTWARE IN PLANNER
        mesg.send_message(self,mesg.START,"")
        self._expected_queue.append("locations")

    def cmd_ping(self,args):
        self._ping_time = time.time()
        mesg.send_message(self,mesg.PING,"")

    def cmd_exit(self,args):
        mesg.send_message(self,mesg.EXIT,"")
    
    def _print_ln(self,print_str):
        self._txt_console.writeln(print_str)

    def _load_grid(self,grid_file):
        wp_list = self._get_waypoints(grid_file)
        self.cargar_icon_grilla(wp_list)
        
    def _get_waypoints(self,wp_file):
        file = open(wp_file,'r')
        lines = file.readlines()
        file.close()
        wp_list = []
        for i in range(len(lines)):
            if (i == 0):
                continue
            line = lines[i]
            values = line.split('\t')
            if (len(values) > 2):
                wp_x = values[8]
                wp_y = values[9]
            else:
                wp_x = values[0]
                wp_y = values[1]
            if (wp_x != 0.0):
                wp_list.append([wp_x,wp_y])
        wp_list = np.array(wp_list)
        
        return wp_list
    
    def remove_file(self,file):
        if os.path.exists(file):
            os.remove(file)

    def _process_command(self,command):
        self.mpstate.functions.process_stdin(command, immediate=True)
    
    def idle_task(self):
        if self.module('map') is not None and not self.menu_added_map:
            self.menu_added_map = True
            self.module('map').add_menu(self.menu)
            
        if (self.mpstate.status.flightmode == 'RTL'):
            if (self._beeped_cant < self._beeped_max and time.time() > self._beeped_time + self._beeped_period):
                self._beeped_time = time.time()
                self._beeped_cant += 1
                os.system("beep")
        
        if (not self._configured):
            if (time.time() > self._init_time + self._config_time):
                self.mpstate.functions.process_stdin("map follow 0", immediate=True)
                #self.mpstate.functions.process_stdin("param set SIM_SPEEDUP 1", immediate=True)
                self._configured = True

        if (self._exists_cmd_list and len(self._cmd_list) > 0 and time.time() >= self._prev_time + self._sleep_time):
            self._sleep_time = 0
            self._prev_time = time.time()

            cmd = self._cmd_list.pop(0)
            if (cmd[-1] == '\n'):
                cmd = cmd[:-1]

            self._print_ln(pcolors.color(cmd,'blue'))
            self._process_command(cmd)

        if (self._quit_flag):
            if (time.time() >= self._quit_time + self._quit_delay):
                os.system("killall -9 mavproxy.py xterm")
            
    def cargar_icon_grilla(self,posiciones):
        #posiciones = np.flip(posiciones,0)
        i = 0
        for elem in posiciones:
            i += 1
            self._process_command("map icon " + str(elem[1]) + " " + str(elem[0]) + " tinyflag")
            if self._cant_grid > 0 and i > self._cant_grid:
                break

    def _save_fence(self,points,fence_file="fence"):
        fence_file += '.txt'
        self.remove_file(fence_file)
        file = open(fence_file,'w')
        for point in points:
            file.write(str(point[0]) + '\t' + str(point[1]) + '\n')
        file.close()
    
    def _read_fence(self,label="",color=None,utm=False,name="",show=True):
        if not utm:
            if name == "":
                fence_file = "fence.txt"
            else:
                fence_file = name + ".txt"
        else:
            fence_file = "fence_utm.txt"
            
        file = open(fence_file,'r')
        lines = file.readlines()
        file.close()
        
        points = [[]]*len(lines)
        i=0
        for line in lines:
            aux_str = ''
            aux_x = 0.0
            for char in line:
                if(char == '\t'):
                    aux_x = float(aux_str)
                    aux_str = ''
                    continue
                aux_str += char
            
            points[i] = [float(aux_str),aux_x]
            i += 1

        points = np.array(points)
        if not utm:
            if show:
                self._show_fence(np.flip(points,1),label,color)
            
        return points

    def _show_fence(self,points,label,color):
        colors = {"white":[255,255,255],"red":[180,0,0],"green":[0,180,0],"blue":[0,0,180],"cian":[0,180,180],
                  "yellow":[180,180,0],"purple":[180,0,180],"black":[0,0,0],"grey":[180,180,180],"orange":[255,180,0],"darkgrey":[40,40,40],
                  "darkyellow":[130,130,0],"darkgreen":[0,90,0],"darkred":[90,0,0],"darkblue":[0,0,120]}
        #color_vector = ["green","red","cian","blue","purple","black","yellow","grey","orange"]
        color_vector = ["white","black","black","green","green","green","blue","blue","red"]
        
        #patrol-patrol
        #color_vector = ["darkgreen","darkblue","darkblue","darkblue","darkblue","black","darkgrey","darkgrey","black"]
        #cover-cover
        #color_vector = ["darkgreen","darkred","darkblue","darkblue","black","darkgrey","darkgrey","black"]
        #patrol-cover
        #color_vector = ["darkgreen","darkred","darkred","darkblue","black","darkgrey","darkgrey","black"]
        #color_vector = ["darkgreen","darkblue","black","darkblue","black","darkgrey","darkgrey","black"]
        #color_vector = ["darkgreen","darkblue","darkblue","darkblue","black","black","orange","darkgrey","black"]
        
        if color == None:
            color = color_vector[self._color_ind]
            self._color_ind += 1
            if (self._color_ind >= len(color_vector)):
                self._color_ind = 0
        
        map_module = self.mpstate.module("map")
        map = map_module.map
        map.add_object(mp_slipmap.SlipPolygon('polygon - %s [%u]' % (str(1),map_module.icon_counter),
                    points, 2, colors[color], 2, arrow = False, popup_menu=None))
        map_module.icon_counter += 1

        if (self._show_label):
            right_most = points[:,1].argmax()
            map.add_object(mp_slipmap.SlipLabel('label - %s [%u]' % (str(1),map_module.icon_counter),
                    [float(points[right_most][0]),float(points[right_most][1])], ' ' + label, 3, [0,0,0]))#[255,255,255]))
            map_module.icon_counter += 1

    def _show_tmp_fence(self,points):
        map_module = self.mpstate.module("map")
        map = map_module.map
        try:
            map.remove_object("fence_tmp")
        except:
            pass
        
        map.add_object(mp_slipmap.SlipPolygon("fence_tmp",
                    points, 2, [0,180,0], 2, arrow = False, popup_menu=None))

    def _clear_border(self):
        map_module = self.mpstate.module("map")
        map_obj = map_module.map
        for i in range(self._border_max+1):
            map_obj.remove_object("border"+str(i))

        self._border_max = -1
        self._border_num = 0
        self._border = []
        
    def _draw_border(self,points):
        map_module = self.mpstate.module("map")
        map_obj = map_module.map
        if (self._border_num > self._border_max):
            self._border_max = self._border_num
        map_obj.add_object(mp_slipmap.SlipPolygon("border"+str(self._border_num),
                    points, 2, [0,0,220], 2, arrow = False, popup_menu=None))
        
        self._border_num += 1
        self._border = []
    
    def _send_locations_to_host(self,host_ip):
        self._send_to_host(self._locations.tostring(),host_ip)
        self.cargar_icon_grilla(self._locations)
        return
    
    def _send_automata_to_host(self,host_ip):
        self._send_to_host(self._automata_queue.pop(),host_ip)
        return
        
    def _send_code_to_host(self,host_ip):
        self._send_to_host(self._code,host_ip)
        return
    
    def _send_fire_to_host(self,host_ip):
        self._send_to_host(self._fire_elems,host_ip)
        return

    def _send_to_host(self,message,host_ip):
        self._print_ln(pcolors.color("Host IP: " + host_ip,'green'))
        
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host_ip, 50000))
        s.sendall(message)
        time.sleep(0.05)        
        s.close()
    
    def _position_update(self, GLOBAL_POSITION_INT):
        '''update gps position'''
        lat = GLOBAL_POSITION_INT.lat*0.0000001
        lon = GLOBAL_POSITION_INT.lon*0.0000001
        relative_alt = GLOBAL_POSITION_INT.relative_alt*0.001 # mm2meter conversion
        alt = GLOBAL_POSITION_INT.alt*0.001
        
        self._sharedmem.set_position(np.array([lon,lat]))
        self._sharedmem.set_altitude(alt)
        self._sharedmem.set_relative_altitude(relative_alt)
        
        '''update ground velocity'''
        vx = GLOBAL_POSITION_INT.vx*0.01 # cm2metre
        vy = GLOBAL_POSITION_INT.vy*0.01 # cm2metre
        vz = GLOBAL_POSITION_INT.vz*0.01 # cm2metre
        
        '''update heading angle'''
        hdg = GLOBAL_POSITION_INT.hdg*0.01 # cdeg2deg
        
        direction = np.array([vy,vx])
        mod = np.linalg.norm(direction,axis=0)
        
        if(mod > 2): # m/s
            self._sharedmem.set_direction(direction/mod)
        else:
            direction = np.array([math.sin(math.radians(hdg)),math.cos(math.radians(hdg))])
            self._sharedmem.set_direction(direction)
        
    def _attitude_update(self, ATTITUDE):
        roll = ATTITUDE.roll
        self._sharedmem.set_roll(np.rad2deg(roll))

    def update_target(self,position):
        map_module = self.mpstate.module("map")
        map = map_module.map
        try:
            map.remove_object("target")
        except:
            pass

        values = position.split(' ')
        latlon = [float(values[0]),float(values[1])]
        map.add_object(mp_slipmap.SlipCircle('target', 2,
                    latlon, 50, [180,0,0], 5, arrow = False, popup_menu=None))
    
    def mavlink_packet(self,m):
        mtype = m.get_type()
        
        if mtype == "GLOBAL_POSITION_INT":
            self._position_update(m)

        if mtype == "ATTITUDE":
            self._attitude_update(m)
        
        if (mtype == "STATUSTEXT"):
            if (mesg.analyze(self,m.text[0])):
                code = mesg.code(m.text)
                if (code == mesg.PRINT):
                    self._print_ln(pcolors.color(mesg.message(m.text),'yellow'))
                elif (code == mesg.NODISC):
                    self._sharedmem.set_scale()
                    self._print_ln(pcolors.color("Not using grid discretization",'yellow'))
                    self._no_disc = True
                elif (code == mesg.SETUP):
                    self._print_ln(pcolors.color(mesg.message(m.text),'yellow'))
                    self._setup_done = True
                elif (code == mesg.INIT_CONFIG):
                    self._print_ln(pcolors.color(mesg.message(m.text),'yellow'))
                    self._wait_for_init = True
                elif (code == mesg.INIT):
                    if (mesg.message(m.text)[0] == 'S'): #SUCCESS
                        self._wait_for_init = False
                        self._plan_started = True
                    self._print_ln(pcolors.color(mesg.message(m.text),'yellow'))
                elif (code == mesg.MARK_WP):
                    locs = mesg.message(m.text)
                    self._process_command("map icon " + locs + " " + "tinyrallypoint")
                elif (code == mesg.FOUND):
                    locs = mesg.message(m.text)
                    self._process_command("map icon " + locs + " " + "barrell")
                    self._print_ln(pcolors.color("FOUND TARGET",'yellow'))
                elif (code == mesg.TARGET):
                    locs = mesg.message(m.text)
                    self.update_target(locs)
                elif (code == mesg.BORDER_CLEAR):
                    self._clear_border()
                elif (code == mesg.BORDER_DRAW):
                    self._border.append(self._border[0])
                    self._draw_border(self._border)
                elif (code == mesg.BORDER):
                    locs = mesg.message(m.text)
                    values = locs.split(" ")
                    self._border.append([float(values[0]),float(values[1])])
                    #self._process_command("map icon " + locs + " " + "smallbluebarrell")
                elif (code == mesg.NOFLY):
                    locs = mesg.message(m.text)
                    self._process_command("map icon " + locs + " " + "smallbarrell")
                elif (code == mesg.UPDATE_WP):
                    self._process_command("wp save " + self.wp_file)
                elif (code == mesg.HOSTIP):
                    expected = self._expected_queue.pop()
                    if (expected == "locations"):
                        self._send_locations_to_host(mesg.message(m.text))
                    elif (expected == "automata"):
                        self._send_automata_to_host(mesg.message(m.text))
                    elif (expected == "code"):
                        self._send_code_to_host(mesg.message(m.text))
                    elif (expected == "fire_elems"):
                        self._send_fire_to_host(mesg.message(m.text))
                elif (code == mesg.PING):
                    delta_time = time.time() - self._ping_time
                    self._print_ln("Received PING. Elapsed time: " + str(delta_time))
                elif (code == mesg.HOTSWAP):
                    message = mesg.message(m.text)
                    if (message == 'ready'):
                        self._hotswap_ready = True
                elif (code == mesg.ERROR):
                    self._print_ln("Received ERROR message, engaging RTL mode")
                elif (code == mesg.AUTOMATA):
                    self._set_automata = True
                    self._automata_data = self._tmp_automata_data
                elif (code == mesg.GRID_QUERY):
                    values = mesg.message(m.text).split(',')
                    grid_size = float(values[0])
                    grid_angle = float(values[1])
                    self._print_ln("Received GRID SIZE of " + str(grid_size) + " meters")
                    self._print_ln("Received GRID ANGLE of " + str(np.rad2deg(grid_angle)) + " degrees")
                    self._grid_size = self._sharedmem.meters2latlon(grid_size)[1]
                    self._utm_grid_size = grid_size
                    self._sharedmem.get_discretizer().set_grid_angle(grid_angle)
                    self._generate_locations()
                elif (code == mesg.EXIT_SUCCESS):
                    self._print_ln("SUCCESS: Exit planner completed")
                elif (code == mesg.VIDEO_REPLY):
                    if (mesg.message(m.text) == "Fail"):
                        self._print_ln("FAIL: Video already started")
                    else:
                        self._print_ln("SUCCESS: Video started")
                elif (code == mesg.QUIT):
                    self._process_command("pquit")

        return

    def _parse_double(self,args,text):
        if (len(args) > 0):
            try:
                value = float(args[0])
                if (value > 0):
                    return value
                else:
                    self._print_ln("FAIL: " + text + " must be greater than zero")
            except ValueError:
                self._print_ln("FAIL: " + text + " parameter not a float")
        else:
            self._print_ln("FAIL: No " + text + " parameter")
        return None
    
    def cmd_height(self,args):
        value = self._parse_double(args,'height')
        if (value != None):
            mesg.send_message(self,mesg.HEIGHT,str(value))
            
    def cmd_cover_radius(self,args):
        value = self._parse_double(args,'radius')
        if (value != None):
            mesg.send_message(self,mesg.COVER_RADIUS,str(value))

    def cmd_grid(self,args):
        value = self._parse_double(args,'grid size')
        if (value != None):
            mesg.send_message(self,mesg.GRID_SIZE,str(value))

    def cmd_radius(self,args):
        value = self._parse_double(args,'radius')
        if (value != None):
            mesg.send_message(self,mesg.RADIUS,str(value))
            
    def cmd_angle(self,args):
        value = self._parse_double(args,'angle')
        if (value != None):
            mesg.send_message(self,mesg.ANGLE,str(value))

    def cmd_sort(self,args):
        sort = str(args[0])
        mesg.send_message(self,mesg.SORT,sort)
    
    def cmd_quit(self,args):
        self._process_command("pexit")
        self._quit_time = time.time()
        self._quit_flag = True

    def cmd_load_mission(self,args):
        path = str(args[0])
        cant_grid = -1
        
        try:
            cant_grid = int(args[1])
        except:
            self._print_ln("FAIL: Cant grid param not set")
            
        if (os.path.exists(path)):
            path = "./" + path + "/"
            map_module = self.mpstate.module("map")
            map = map_module.map

            self._process_command("fence load " + path + "fence.txt")
            
            #Grid
            self._grid_num = 1
            while (os.path.exists(path + "grid" + str(self._grid_num) + ".txt")):
                wp_list = self._get_waypoints(path + "grid" + str(self._grid_num) + ".txt")
                self._grid_num += 1
                for i in range(len(wp_list)):
                    #if (i == 0):
                        #continue
                    if (cant_grid >= 0 and i >= cant_grid):
                        break
                    wp = wp_list[i]
                    self._process_command("map icon " + str(wp[0]) + " " + str(wp[1]) + " " + "tinyflag")
            
            #Positions
            file = open(path + "pos_log.txt")
            lines = file.readlines()
            file.close()
            points = []
            count = 0
            for i in range(len(lines)):
                line = lines[i]
                if (count < 2 and i != len(lines)-1):
                    count += 1
                    continue
                else:
                    count = 0
                values = line.split(',')
                if (len(values) > 0):
                    #flag = "smallplane.png"
                    #icon = map.icon(flag)
                    #map.add_object(mp_slipmap.SlipIcon('icon - %s [%u]' % (str(flag),map_module.icon_counter), 
                               #(float(values[2]),float(values[1])),
                           #icon, layer=3, rotation=-np.rad2deg(float(values[3])), follow=False))
                    points.append([float(values[2]),float(values[1])])

            #Loop points
            #points.append(points[0])

            #patrol-patrol
            #ind1 = 117
            #ind2 = 152
            #cover-cover
            #ind1 = 254
            #ind2 = 255
            #patrol-cover

            #Monitor2
            #ind1 = 73
            #ind2 = 110
            #ind3 = 138
            #ind4 = 157
            #Monitor3
            #ind1 = 36
            #ind2 = 62
            #ind3 = 82
            #ind4 = 85

            #Data-collect
            ind1 = 118
            ind2 = 235
            ind3 = 698
            ind4 = 717
            
            points_color1 = points[ind1:ind2+1] #points[:ind1+1]
            points_color2 = []#points[ind2:ind3+1] #points[ind1:ind2+1]
            points_color3 = []#points[ind3:ind4+1] #points[ind2:]
            points_color4 = []#points[ind4:] #points[ind2:]

            if len(points_color1) > 0:
                map.add_object(mp_slipmap.SlipPolygon('polygon - %s [%u]' % (str(1),map_module.icon_counter),
                        points_color1, 2, [0,0,0], 2, arrow = False, popup_menu=None))
                map_module.icon_counter += 1
            if len(points_color2) > 0:
                map.add_object(mp_slipmap.SlipPolygon('polygon - %s [%u]' % (str(1),map_module.icon_counter),
                        points_color2, 2, [180,255,180], 3, arrow = False, popup_menu=None))
                map_module.icon_counter += 1
            if len(points_color3) > 0:
                map.add_object(mp_slipmap.SlipPolygon('polygon - %s [%u]' % (str(1),map_module.icon_counter),
                        points_color3, 2, [255,180,0], 2, arrow = False, popup_menu=None))
                map_module.icon_counter += 1
            if len(points_color4) > 0:
                #map.add_object(mp_slipmap.SlipPolygon('polygon - %s [%u]' % (str(1),map_module.icon_counter),
                        #points_color4, 2, [40,40,40], 2, arrow = False, popup_menu=None))
                map_module.icon_counter += 1
            
            #Visited locations
            if (cant_grid > 0):
                file = open(path + "markwp.txt")
                lines = file.readlines()
                file.close()
                count = 0
                repeat = False
                for line in lines:
                    if (not repeat):
                        repeat = True
                        continue
                    repeat = False
                    count += 1
                    self._process_command("map icon " + line + " " + "tinyrallypoint")

                    #Show numbers
                    #values = line.split(' ')
                    #map.add_object(mp_slipmap.SlipLabel('label - %s [%u]' % (str(1),map_module.icon_counter),
                                #[float(values[0]),float(values[1])], ' ' + str(count), 3, [255,255,255]))
                    #map_module.icon_counter += 1
            else:
                self._print_ln("Not showing visited locations")

            #Found locations
            try:
                file = open(path + "found.txt")
                lines = file.readlines()
                file.close()
                for line in lines:
                    self._process_command("map icon " + line + " " + "barrell")
            except:
                self._print_ln("FAIL: found.txt not found")
            
            
            #NoFly locations
            try:
                file = open(path + "nofly.txt")
                lines = file.readlines()
                file.close()
                for line in lines:
                    self._process_command("map icon " + line + " " + "smallbluebarrell")
            except:
                self._print_ln("FAIL: nofly.txt not found")
            

def init(mpstate):
    '''initialise module'''
    return planner_viewer(mpstate)


def write_to_file(filename,text):
    log = open(filename,"a+")
    log.write(text)
    log.close()
