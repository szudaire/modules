from MAVProxy.modules.planner_module import Module
from MAVProxy.modules import planner_util_message as mesg
import numpy as np
import time
from multiprocessing import Value
import random

class Battery(Module):
    def init(self):
        self._set_suscription_packets(["GLOBAL_POSITION_INT","WIND"])

        self._orig_budget = 3780.0/21.0 #need to be higher than 1500/21.0
        
        self._sharedmem.set_budget(self._orig_budget) #Meters

        self.set_random_bat()
        
        self._sharedmem.set_battery_warning(0)
        self._sharedmem.set_total_dist(0)
        self.controllables = ['charge','visit_all']

        self._time_set = False
        self._time2_set = False

        self._visit_array = []
        self._visit_time = Value('d',0.0)

        self._process_type = 1

        self._wind_timer = 0
        self._sustain = 0
        return

    def set_random_bat(self):
        range_val = 0.33
        val = 1 + (random.random()*range_val)
        
        self._modifier = 1#val
        print("Modifier",self._modifier)

    def _started(self):
        self._visit_time.value = time.time()

    def visit_all(self):
        t2 = time.time()
        t1 = self._visit_time.value
        self._visit_array.append(t2-t1)

        self._visit_time.value = t2

        if np.sum(self._visit_array) > 1800: #time limit
            #ftest = open("/home/debian/powerdevs-master/examples/patrullaje-x3_PD30/time_log.txt",'w')
            ftest = open("/root/.cache/mavproxy/logs/time_log.txt",'w')
            ftest.write("plan changes: "+str(self._sharedmem.num_change_choices.value)+"\n")
            ftest.write("rounds: "+str(len(self._visit_array))+"\n")
            ftest.write("mean: "+str(np.mean(self._visit_array))+"\n")
            ftest.write("min: "+str(np.min(self._visit_array))+"\n")
            ftest.write("max: "+str(np.max(self._visit_array))+"\n")
            ftest.write(str(self._visit_array)+"\n")
            ftest.close()

            self._add_to_mavproxy_command_queue("rtl") #Generate RTL to quit mission and save info
            #self._add_to_event_queue('stop_simu') #Provoke automata error and RTL engage

        return
    
    def charge(self):
        self._add_to_message_queue(mesg.PRINT,"Uploading with "+str(self._sharedmem.get_total_dist())+" out of "+str(self._sharedmem.get_budget()))
        self._sharedmem.set_total_dist(0)
        self._sharedmem.set_battery_warning(0)
        
        self.set_random_bat()
        return 'charged'
	
    def mavlink_packet(self,m):
        mtype = m.get_type()
        if (mtype == "WIND"):
            ang = m.direction*np.pi/180
            spd = m.speed
            self._sharedmem.set_wind_data(spd,ang)
        elif mtype == "GLOBAL_POSITION_INT":
            vx = m.vx*0.01
            vy = m.vy*0.01
            vel = np.sqrt(vx**2 + vy**2)

            if vel < 10: #Don't start counting until takeoff
                return

            
            if not self._time_set:
                #self._add_to_mavproxy_command_queue("param set SIM_WIND_SPD 6")
                self._wind_timer = time.time()
                self._time = time.time()
                self._time_set = True
                if not self._time2_set:
                    self._time2 = time.time()
                    self._time2_set = True
            else:
                
                if time.time() - self._time < 0.1:
                    return

                dt = time.time() - self._time
                self._time = time.time()
                aux = self._sharedmem.get_total_dist() #Now reinterpreted as time
                
                #aux += dt*vel*self._modifier
                aux += dt
                self._sharedmem.set_total_dist(aux)

                if time.time() - self._wind_timer > self._sustain:
                    self._wind_timer = time.time()
                    #self._add_to_mavproxy_command_queue("param set SIM_WIND_SPD "+str(random.randint(2,12)))
                    self._sustain = random.randint(30,90)
                    
                if time.time() - self._time2 > 5:
                    self._time2 = time.time()
                    if self._sharedmem.get_total_dist() > 10 and self._sharedmem.get_next_location() != 0:
                        loc_current_pos = self._sharedmem.get_position()
                        loc_next_pos = self._sharedmem.get_discretizer().get_position(0)
                        self._sharedmem.set_trajectory_parameters(loc_current_pos,auxwp=True,overwp=True,check_ang=True,pond=False,curr_dir=True)
                        wp_list,distance,dir = self._sharedmem.calculate_trajectory(loc_next_pos,update_dir=False)
                        dist_scale = self._sharedmem.meters2latlon(1.0)[1]

                        distance = distance/dist_scale

                        #Distance modified for time
                        distance = distance/21.0
                        
                        #print(self._total_dist.value,distance)
                        if self._sharedmem.get_total_dist() + distance > self._sharedmem.get_budget():
                            if not self._sharedmem.get_battery_warning():
                                self._add_to_message_queue(mesg.PRINT,"Time warning with "+str(self._sharedmem.get_total_dist())+" out of "+str(self._sharedmem.get_budget()))
                                self._sharedmem.set_battery_warning(1)
                                self._sharedmem.set_current_location(-1)
                                self._sharedmem.set_arrived_dir_flag(False)
                                self._add_to_event_queue('low_bat')
                    
